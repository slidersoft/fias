﻿var express = require('express');
var path = require('path'); // модуль для парсинга пути
var app = express();

var mysql = require('mysql');

var connection = mysql.createConnection({
	database: 'fias',
	host: '127.0.0.1',
	user: 'fias',
	password: 'puKA1aFo'
});


app.get('/address', function (req, res) {    
	var sql = 'SELECT * FROM `address_objects` AS ao WHERE 1=1'
	if (req.query.formalname)
		sql += " AND ao.formalname = '" + req.query.formalname + "'";
	if (req.query.parentguid)
		sql += " AND ao.parentguid = '" + req.query.parentguid + "'";
	if (!req.query.formalname && !req.query.parentguid)
		sql += " AND ao.parentguid = ''";
	console.log(sql)
	connection.query(sql, function (err, rows, fields) {
		if (err) res.send(err);
		res.send(rows);
	});    
});

app.get('/address/:parent', function (req, res) {    
	var parent = req.param('parent');
	connection.query('SELECT * FROM `address_objects` AS ao WHERE ao.parentguid = ?', [parent], function (err, rows, fields) {
		if (err) res.send(err);
		res.send(rows);
	});    
});
app.get('/address/:parent/:address', function (req, res) {
	var parent = req.param('parent');
	var address = req.param('address');
	connection.query('SELECT * FROM `address_objects` AS ao WHERE ao.parentguid = ? AND ao.formalname like ? AND ao.livestatus=1 AND ao.enddate > NOW()', [parent, '%' + address + '%'], function (err, rows, fields) {
		if (err) res.send(err);
		res.send(rows);
	});
});
app.get('/house/:parent', function (req, res) {
	var parent = req.param('parent');
	connection.query('SELECT * FROM `house` AS h WHERE h.aoguid = ? AND h.enddate > NOW()', [parent], function (err, rows, fields) {
		if (err) res.send(err);
		res.send(rows);
	}); 
});
app.get('/rooms/:parent', function (req, res) {
	var parent = req.param('parent');
	connection.query('SELECT * FROM `rooms` AS r WHERE r.houseguid = ? AND r.livestatus = 1 AND r.enddate > NOW()', [parent], function (err, rows, fields) {
		if (err) res.send(err);
		res.send(rows);
	});
});

app.listen(1338, function () {
	console.log('Express server listening on port 1338');
});