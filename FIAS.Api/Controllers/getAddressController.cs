﻿using FIAS.Api.Models;
using FIAS.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace FIAS.Api.Controllers
{
    public class getAddressController : ApiController
    {
        UnitOfWork uow;
        public getAddressController()
        {
            uow = new UnitOfWork();
        }

        public IEnumerable<AddressObjectRequest> Get()
        {
            var ao = uow.AddressObjects.GetAll().Select(a => new AddressObjectRequest(a));
            return ao;
        }

        public IEnumerable<AddressObjectRequest> Get(string name, string parent_guid)
        {
            var ao = uow.AddressObjects.GetObject(name,parent_guid).Select(a => new AddressObjectRequest(a));
            return ao;
        }

        public IEnumerable<AddressObjectRequest> Get(string name)
        {
            var ao = uow.AddressObjects.GetObject(name).Select(a => new AddressObjectRequest(a));
            return ao;
        }
    }
}
