﻿using FIAS.Api.Models;
using FIAS.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace FIAS.Api.Controllers
{
    public class getHousesController : ApiController
    {
        UnitOfWork uow;
        public getHousesController()
        {
            uow = new UnitOfWork();
        }

        public IEnumerable<HouseRequest> Get(string name)
        {
            var h = uow.House.GetHousesSteet(name).Select(ho => new HouseRequest(ho));
            return h;
        }
    }
}
