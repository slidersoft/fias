﻿using FIAS.Domain.Entity;
namespace FIAS.Api.Models
{
    public class HouseRequest
    {
        public string housenum { get; set; }
        public string buildnum { get; set; }
        public string houseid { get; set; }
        public string houseguid { get; set; }
        public string aoguid { get; set; }

        public HouseRequest() { }

        public HouseRequest(house h)
        {
            this.housenum = h.housenum;
            this.buildnum = h.buildnum;
            this.houseid = h.houseid;
            this.houseguid = h.houseguid;
            this.aoguid = h.aoguid;
        }

        public house ToHouse()
        {
            return new house
            {
                housenum = this.housenum,
                buildnum = this.buildnum,
                houseid = this.houseid,
                houseguid = this.houseguid,
                aoguid = this.aoguid
            };
        }
    }
}