﻿using FIAS.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FIAS.Api.Models
{
    public class AddressObjectRequest
    {
        public string aoguid { get; set; }
        public string formalname { get; set; }
        public string offname { get; set; }
        public string shortname { get; set; }
        public string parentguid { get; set; }

        public AddressObjectRequest() { }

        public AddressObjectRequest(address_objects ao)
        {
            this.aoguid = ao.aoguid;
            this.formalname = ao.formalname;
            this.offname = ao.offname;
            this.parentguid = ao.parentguid;
            this.shortname = ao.shortname;            
        }

        public address_objects ToAddressObject()
        {
            return new address_objects
            {
                aoguid = this.aoguid,
                formalname = this.formalname,
                offname = this.offname,
                parentguid = this.parentguid,
                shortname = this.shortname
            };
        }
    }
}