CREATE TABLE `structure_status` (
	`strstatid` INT(10) NOT NULL COMMENT 'Признак строения',
	`name` VARCHAR(20) NOT NULL COMMENT 'Наименование',
	`shortname` VARCHAR(20) NULL DEFAULT NULL COMMENT 'Краткое наименование'
)
COMMENT='Признак строения'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
