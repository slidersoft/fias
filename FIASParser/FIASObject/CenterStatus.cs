﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIASParser
{
    public class CenterStatus
    {
        public int CENTERSTID { get; set; }
        public string NAME { get; set; }
    }
}
