﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIASParser
{
    /// <summary>
    /// Тип адресного объекта
    /// </summary>
    public class AddressObjectType
    {
        /// <summary>
        /// Уровень адресного объекта
        /// </summary>
        public int LEVEL { get; set; }
        /// <summary>
        /// Краткое наименование типа объекта
        /// </summary>
        public string SCNAME { get; set; }
        /// <summary>
        /// Полное наименование типа объекта
        /// </summary>
        public string SOCRNAME { get; set; }
        /// <summary>
        /// Ключевое поле
        /// </summary>
        public string KOD_T_ST { get; set; }
    }
}
