﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIASParser
{
    public class House
    {
        [Display(Name = "Почтовый индекс")]
        [StringLength(6)]
        public string POSTALCODE { get; set; }

        [Display(Name = "Код ИФНС ФЛ")]
        [StringLength(4)]
        public string IFNSFL { get; set; }

        [Display(Name = "Код территориального участка ИФНС ФЛ")]
        [StringLength(4)]
        public string TERRIFNSFL { get; set; }

        [Display(Name = "Код ИФНС ЮЛ")]
        [StringLength(4)]
        public string IFNSUL { get; set; }

        [Display(Name = "Код территориального участка ИФНС ЮЛ")]
        [StringLength(4)]
        public string TERRIFNSUL { get; set; }

        [Display(Name = "OKATO")]
        [StringLength(11)]
        public string OKATO { get; set; }

        [Display(Name = "OKTMO")]
        [StringLength(11,MinimumLength = 8)]
        public string OKTMO { get; set; }

        [Display(Name = "Дата время внесения записи")]
        [Required]
        public DateTime UPDATEDATE { get; set; }

        [Display(Name = "Номер дома")]
        [StringLength(20, MinimumLength = 1)]
        public string HOUSENUM { get; set; }

        [Display(Name = "Номер дома")]
        [Range(0,9)]
        [Required]
        public int ESTSTATUS { get; set; }

        [Display(Name = "Номер корпуса")]
        [StringLength(10, MinimumLength = 0)]
        public string BUILDNUM { get; set; }

        [Display(Name = "Номер строения")]
        [StringLength(10, MinimumLength = 0)]
        public string STRUCNUM { get; set; }

        [Display(Name = "Признак строения")]
        [Range(0, 9999999999)]
        public int STRSTATUS { get; set; }

        [Display(Name = "Уникальный идентификатор записи дома")]
        [StringLength(36)]
        public string HOUSEID { get; set; }

        [Display(Name = "Глобальный уникальный идентификатор дома")]
        [StringLength(36)]
        public string HOUSEGUID { get; set; }

        [Display(Name = "Guid записи родительского объекта (улицы, города, населенного пункта и т.п.)")]
        [StringLength(36)]
        public string AOGUID { get; set; }

        [Display(Name = "Начало действия записи")]        
        [Required]
        public DateTime STARTDATE { get; set; }

        [Display(Name = "Окончание действия записи")]
        [Required]
        public DateTime ENDDATE { get; set; }

        [Display(Name = "Состояние дома")]
        [Range(0, 9999999999)]
        [Required]
        public int STATSTATUS { get; set; }

        [Display(Name = "Внешний ключ на нормативный документ")]        
        [StringLength(36)]
        public string NORMDOC { get; set; }

        [Display(Name = "Внешний ключ на нормативный документ")]
        [Range(0, 9999999999)]
        [Required]
        public int COUNTER { get; set; }
    }
}
