﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace FIASParser
{
    public static class Extensions
    {

        public static string EmptyIfNull(this object value)
        {
            if (value == null)
                return "";
            return value.ToString();
        }
    }

    class Program
    {
        //const string CONNECTION_STRING = "server=192.168.1.41;user id=root;password=12345678;persistsecurityinfo=True;database=fias;charset=utf8";
        //const string CONNECTION_STRING = "server=192.168.0.105;user id=root;password=12345678;persistsecurityinfo=True;database=fias;charset=utf8";
        const string CONNECTION_STRING = "server=localhost;user id=root;password=mypassword;persistsecurityinfo=True;database=fias;charset=utf8";

        static void Main(string[] args)
        {

            //string file_name_ACTSTAT = "E:\\work\\Фиас\\XLM\\AS_ACTSTAT_20160416_89eb3924-2c4c-4004-9170-2d4c0bac7595.XML";
            //string file_name_ADDROBJ = "E:\\work\\Фиас\\XLM\\AS_ADDROBJ_20160416_652e54b3-d97f-4699-9424-1a5ee26380ce.XML";
            //string file_name_CENTERST = "E:\\work\\Фиас\\XLM\\AS_CENTERST_20160416_72f3a7b3-134e-4ff7-b08c-cd5a7575ed85.XML";


            var file_name_list = new List<string>
            {
                "D:\\fias_xml\\AS_ADDROBJ_20210124_481337f1-ad4e-4963-9a8b-b84a37cb375e.XML"
            };

            foreach (string file_name in file_name_list)
            {
                Parser parser = new Parser(CONNECTION_STRING, file_name);
                parser.Parsing();
            }
                      
            Console.ReadKey();
        }

        
    }
}
