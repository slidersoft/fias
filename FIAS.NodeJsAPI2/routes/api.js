﻿var express = require('express');
var router = express.Router();
var url = require('url');
var sql = require('sql-query');
var sqlQuery = sql.Query();    

var mysql = require('mysql');
//var connection = mysql.createConnection({
//    host: 'localhost',
//    user: 'mysql',
//    password: 'mysql',
//    database: 'fias'
//});

var connection = mysql.createConnection({
    database: 'fias',
    host: '127.0.0.1',
    user: 'fias',
    password: 'puKA1aFo'
});

router.get('/', function (req, res) {
    var query = sqlSelect
        .from('address_objects')
        .where({ parentguid: "" })
        .build();

    console.log(query);

    connection.query(query, function (error, results, fields) {
        if (error) res.send(error);        
        res.send(results);        
    });
});

router.get('/address', function (req, res) {
   
    var sqlSelect = sqlQuery.select();
    var r = url.parse(req.url, true);
    var offset = 0;

    sqlSelect
        .from('address_objects');

    if (r.query.parentguid)
        sqlSelect.where({ parentguid: r.query.parentguid });
    else
        sqlSelect.where({ parentguid: "" });
        
    if (r.query.formalname)
        sqlSelect.where({ formalname: sql.like(r.query.formalname) });

    if (r.query.shortname)
        sqlSelect.where({ shortname: r.query.shortname });

    if (r.query.livestatus)
        sqlSelect.where({ livestatus: r.query.livestatus });

    if (r.query.limit) {
        sqlSelect.limit(r.query.limit);
        if (r.query.offset)
            var offset = r.query.offset;
    }

    var query = sqlSelect.build();

    if (offset > 0)
        query += ", " + offset;    

    console.log(r);
    console.log(query);
    
    connection.query(query, function (error, results, fields) {
        if (error) res.send("Ошибка:" + error);
        res.send(results);
    });
});

router.get('/house', function (req, res) {

    var sqlSelect = sqlQuery.select();
    var r = url.parse(req.url, true);
    var offset = 0;

    sqlSelect
        .from('house');

    if (r.query.aoguid)
        sqlSelect.where({ aoguid: r.query.aoguid });
    else
        sqlSelect.where({ aoguid: "" });

    if (r.query.housenum)
        sqlSelect.where({ housenum: r.query.housenum });

    if (r.query.buildnum)
        sqlSelect.where({ buildnum: r.query.buildnum });

    if (r.query.limit) {
        sqlSelect.limit(r.query.limit);
        if (r.query.offset)
            var offset = r.query.offset;
    }

    var query = sqlSelect.build();

    if (offset > 0)
        query += ", " + offset;

    console.log(r);
    console.log(query);

    connection.query(query, function (error, results, fields) {
        if (error) res.send("Ошибка:" + error);
        res.send(results);
    });
});

router.get('/room', function (req, res) {

    var sqlSelect = sqlQuery.select();
    var r = url.parse(req.url, true);
    var offset = 0;

    sqlSelect
        .from('rooms');

    if (r.query.houseguid)
        sqlSelect.where({ houseguid: r.query.houseguid });
    else
        sqlSelect.where({ houseguid: "" });

    if (r.query.roomnumber)
        sqlSelect.where({ roomnumber: r.query.roomnumber });

    if (r.query.livestatus)
        sqlSelect.where({ livestatus: r.query.livestatus });

    if (r.query.limit) {
        sqlSelect.limit(r.query.limit);
        if (r.query.offset)
            var offset = r.query.offset;
    }

    var query = sqlSelect.build();

    if (offset > 0)
        query += ", " + offset;

    console.log(r);
    console.log(query);

    connection.query(query, function (error, results, fields) {
        if (error) res.send("Ошибка:" + error);
        res.send(results);
    });
});

module.exports = router;