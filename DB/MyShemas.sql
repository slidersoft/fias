-- --------------------------------------------------------
-- Хост:                         192.168.1.38
-- Версия сервера:               5.5.49-0ubuntu0.14.04.1-log - (Ubuntu)
-- ОС Сервера:                   debian-linux-gnu
-- HeidiSQL Версия:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица contingent.academic_year
DROP TABLE IF EXISTS `academic_year`;
CREATE TABLE IF NOT EXISTS `academic_year` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `number_days` int(11) DEFAULT '0' COMMENT 'Количество дней',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Учебный год';

-- Дамп данных таблицы contingent.academic_year: ~0 rows (приблизительно)
DELETE FROM `academic_year`;
/*!40000 ALTER TABLE `academic_year` DISABLE KEYS */;
/*!40000 ALTER TABLE `academic_year` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.accreditation
DROP TABLE IF EXISTS `accreditation`;
CREATE TABLE IF NOT EXISTS `accreditation` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Аккредитация',
  `educational_institutions_id` bigint(20) NOT NULL,
  `series` varchar(4) NOT NULL COMMENT 'Серия бланка',
  `number` varchar(6) NOT NULL COMMENT 'Номер бланка',
  `register_number` bigint(20) NOT NULL COMMENT 'Реестровый номер свидетельства о государственной аккредитации образовательной деятельности для организаций, которые должны проходить аккредитацию',
  `date_issue` date NOT NULL COMMENT 'Дата выдачи',
  `expiration_date` date NOT NULL COMMENT 'Срок действия (значение: дата или «бессрочный»)',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `delete` tinyint(4) NOT NULL COMMENT 'Удален?',
  PRIMARY KEY (`id`),
  KEY `FK_accreditation_educational_institutions` (`educational_institutions_id`),
  CONSTRAINT `FK_accreditation_educational_institutions` FOREIGN KEY (`educational_institutions_id`) REFERENCES `educational_institutions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Аккредитация';

-- Дамп данных таблицы contingent.accreditation: ~0 rows (приблизительно)
DELETE FROM `accreditation`;
/*!40000 ALTER TABLE `accreditation` DISABLE KEYS */;
/*!40000 ALTER TABLE `accreditation` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.actions_people_participated
DROP TABLE IF EXISTS `actions_people_participated`;
CREATE TABLE IF NOT EXISTS `actions_people_participated` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `guid` varchar(36) NOT NULL DEFAULT '0',
  `people_id` bigint(20) NOT NULL DEFAULT '0',
  `name_action` text NOT NULL,
  `status_action` varchar(255) NOT NULL DEFAULT '0',
  `date_participation` date NOT NULL,
  `results_participation` text NOT NULL,
  `given_ranks` text NOT NULL COMMENT 'Присвоеные звания / разряды',
  PRIMARY KEY (`id`),
  KEY `guid` (`guid`),
  KEY `FK_actions_people_participated_people` (`people_id`),
  CONSTRAINT `FK_actions_people_participated_people` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Участие в мероприятиях';

-- Дамп данных таблицы contingent.actions_people_participated: ~0 rows (приблизительно)
DELETE FROM `actions_people_participated`;
/*!40000 ALTER TABLE `actions_people_participated` DISABLE KEYS */;
/*!40000 ALTER TABLE `actions_people_participated` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.addresses_people
DROP TABLE IF EXISTS `addresses_people`;
CREATE TABLE IF NOT EXISTS `addresses_people` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `people_id` bigint(20) NOT NULL,
  `people_guid` varchar(36) NOT NULL,
  `address_type_id` int(11) NOT NULL,
  `address_text` text NOT NULL,
  `address_fias` varchar(36) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Удален?',
  PRIMARY KEY (`id`),
  KEY `people_guid` (`people_guid`),
  KEY `FK_addresses_people_people` (`people_id`),
  KEY `FK_addresses_people_address_type` (`address_type_id`),
  CONSTRAINT `FK_addresses_people_address_type` FOREIGN KEY (`address_type_id`) REFERENCES `address_type` (`id`),
  CONSTRAINT `FK_addresses_people_people` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COMMENT='Адреса людей';

-- Дамп данных таблицы contingent.addresses_people: ~45 rows (приблизительно)
DELETE FROM `addresses_people`;
/*!40000 ALTER TABLE `addresses_people` DISABLE KEYS */;
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(2, 1, '1781ce97-1d7f-11e6-8749-bcaec598f084', 1, 'Уфа', '', '2016-05-24 15:03:59', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(3, 56, '202f41a6-53c4-471a-a21b-e434b7fd8092', 1, 'From Russia', '8e401e7f-7f30-4f5d-81a6-e504c435a9c7', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(4, 57, 'bdb1683e-3067-4b6f-ba47-abe0362d1277', 1, 'From Russia', 'e24bc46d-8f04-4acc-b374-faee9a6ae7be', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(5, 58, '72fbc338-2c28-4f10-9ad5-4022127e8c44', 1, 'From Russia', 'bd67184b-f53d-4977-8fe8-1bd36a5ac9b5', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(6, 59, '2e948a67-85fd-49e6-85d0-b6a5da20246c', 1, 'From Russia', '8d5888f1-2e65-42e6-bc74-b865f9c1fa71', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(7, 60, '17338eef-b3fc-4664-a1d4-a23ad7115152', 1, 'From Russia', '49b5e6df-14f7-41d5-8c1a-1050688b0c74', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(8, 61, '9b898537-3adf-458d-a6b2-149da4e63ead', 1, 'From Russia', '562c0963-7867-4e8e-a6f9-f09957ddf3b0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(9, 62, '1fb6a22d-76c0-4a41-9b7c-57a9f4e47537', 1, 'From Russia', '18aff90b-3ca5-4a35-b7ce-b4f01512509b', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(10, 63, 'b1897fc0-770b-4605-8ce5-54156683fcbb', 1, 'Уфа', '9b72bbf8-4392-4327-94df-5ea5b74f075d', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(11, 64, '30e2a114-a640-4f15-944c-a17458f3c78f', 1, 'Уфа', '07efc9ea-8f3e-420e-9646-635385099b69', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(12, 65, '775902cb-bf63-473a-82e5-895e51c1a213', 1, 'Уфа', '63619e67-9fcc-4280-a207-044ee9aea0f2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(13, 66, 'dad7d7a0-a1d2-4ce2-bfba-2fd7bd0223e6', 1, 'Уфа', 'fe3fd72d-ca44-4bc2-b856-041b7d24b5fc', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(14, 67, '8cc80e02-4bfd-4199-88de-7dc4a7f44778', 1, 'Уфа', '538b2e39-f4cf-4e26-828d-f8fde65043fd', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(15, 68, '8db09685-372a-45a7-81e2-7034f5a7f456', 1, 'Уфа', '47b3799b-724a-4f05-9de1-3793b37a044a', '2016-06-01 16:55:47', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(16, 69, '98d49dca-bba2-4697-ac7a-e327a376d40f', 1, 'Уфа', '45e5add2-9030-49e4-bad1-13c1035f72e8', '2016-06-03 16:55:29', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(17, 70, 'e8754580-804f-4272-a0ac-cc915bd409b8', 1, 'Уфа', '2cb41484-25f5-4b5e-ab6c-10812d71e5ad', '2016-06-03 16:57:03', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(18, 71, '6f4f5c93-aa85-4711-9b13-1df3ae65c967', 1, 'Уфа', '5db414c9-ad51-4bed-b736-81579012b8fd', '2016-06-03 16:59:59', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(19, 72, 'acd2c736-f7f8-43b2-9ccb-54322eddbb4b', 1, 'Уфа', '0f19fff8-2ba0-43c0-87a7-de73c4a853d7', '2016-06-03 17:03:39', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(20, 73, '86e6c9a7-dd48-493e-a3a1-70ff2fd1e3b8', 1, 'Уфа', '539830cf-3f0f-4082-9ad0-db36f685810c', '2016-06-06 09:34:54', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(21, 74, '9639614e-5ecd-46fb-8877-b7bee81f95da', 1, 'Уфа', 'b1ad559c-7364-4bdd-94e8-0d5f01c04bcf', '2016-06-06 11:20:45', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(22, 75, '6952cd3f-42f9-4988-b69f-4cec7741adcf', 1, 'Уфа', 'a84fbf08-4a70-4b31-b4cd-9d7087e745c8', '2016-06-06 11:21:34', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(23, 76, '79e4fd09-c5b1-41c2-ac59-1bbad7c22b1e', 1, 'Уфа', '7c5bbade-dfeb-4ac3-a5d9-66e25434497a', '2016-06-06 17:16:23', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(25, 78, 'b09e2b02-39fe-4381-8cb3-2835d0c1521b', 1, 'Уфа', '0078fcf6-92d3-47ee-97fc-5b046fd283df', '2016-06-06 17:24:44', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(26, 79, 'b5364686-1d5a-47a0-b7f9-44c29851f44e', 1, 'Уфа', 'd470151c-56cf-4a16-ae22-cc78ef990ee2', '2016-06-06 17:37:06', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(31, 84, 'b99ae78a-20fd-4a6f-8231-0616bf5d1b91', 1, 'Уфа', 'd1eb88f0-cdb2-44c6-9d36-aea65e6f3f52', '2016-06-06 17:48:16', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(32, 85, 'f988e067-91f4-4934-93a3-6fb5c40a1f27', 1, 'Уфа', '69323391-7e5f-48bf-9a7f-059abcdf04b3', '2016-06-07 14:36:48', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(33, 86, '3c8794a4-4d17-49b5-8128-ed25a1021af2', 1, 'Уфа', '7361d438-96bf-4804-bdc0-d4a29a9bc489', '2016-06-07 14:38:27', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(34, 87, 'ab1da5c4-a9ad-4c27-a284-84a12e6e94d9', 1, 'Уфа', 'c18e1ee7-dde8-4f15-8cad-7cf553a88212', '2016-06-07 14:39:15', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(35, 88, '98ee866f-1b7c-413a-a879-b3509a9aa0da', 1, 'Уфа', 'bb980c05-0164-490d-9785-c5c49d6e68a9', '2016-06-07 14:39:26', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(36, 89, '9af139a4-2d64-45b8-ad0a-ca16b0f9f127', 1, 'Уфа', 'c9b982b5-985b-47f8-95b3-eb131fd95fb1', '2016-06-07 14:40:18', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(37, 90, 'ce0889c7-96c7-4d48-aeab-f2bbe39e3694', 1, 'Уфа', '8beae625-8dea-4299-b4c6-711020ef95ae', '2016-06-07 14:42:54', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(38, 91, '79f11c9d-8e47-4f39-b96a-1c8e357a684e', 1, 'Уфа', 'b1313b18-cee0-405d-824e-d2eb9ef9f588', '2016-06-07 14:45:02', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(39, 92, 'c73618ad-2df7-45ad-8736-791ae31fb816', 1, 'Уфа', '3c2b46d3-8b8e-4db7-89e3-27fd84a4c0d7', '2016-06-07 14:46:40', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(40, 93, '389ccaed-4ffe-44f6-aa2d-9fc391dc3fd5', 1, 'Уфа', 'dc3801ac-661e-4245-b943-1c2bd57417ac', '2016-06-07 14:50:58', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(41, 94, '1d93a967-8942-4915-acd2-76c9b1dcec88', 1, 'Уфа', '21c169e5-997a-4903-8b44-713b3210465c', '2016-06-07 14:51:44', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(42, 95, 'db7210b2-6ce8-471f-b80e-43cd584bc097', 1, 'Уфа', 'fd22550e-ad1e-4c0a-a387-f614365a5c43', '2016-06-07 14:52:15', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(43, 96, 'bbc162c0-6a83-4893-9247-50297c956cdd', 1, 'Уфа', 'd6a5c01b-ba83-415e-9969-27cbcce46205', '2016-06-07 15:02:37', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(44, 97, '8f45cc55-7a10-440f-85da-b316fa10b168', 1, 'Уфа', '55520d8b-01d4-4f51-82dc-3beaed83457d', '2016-06-07 15:03:00', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(45, 98, 'c4aabc8e-5dfe-44d1-b5b5-46ac9aae2bc1', 1, 'Уфа', '0aac772d-f17d-4152-9e12-ff392f58cced', '2016-06-07 15:08:15', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(46, 99, 'c0d36ae9-2139-4abd-884c-4f85eeee2ac7', 1, 'Уфа', '6a900228-3862-47b3-bf9a-8a94bc163f22', '2016-06-07 15:10:22', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(47, 100, '94802577-7fd4-4ca7-b684-64f8361be502', 1, 'Уфа', '4e253b21-6a42-483c-87ee-d7666dcdd95d', '2016-06-07 15:11:57', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(48, 101, '2644e580-61f1-4575-b58d-c5e006429672', 1, 'Уфа', '8bc2933e-71d8-40bd-986e-72f960536a2a', '2016-06-07 15:14:49', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(49, 102, '2bc79d48-9f2c-4ecc-8520-78f4e68d0cb9', 1, 'Уфа', '30aec2b5-f9a3-4ba0-a54a-6cf97b57ae05', '2016-06-07 15:19:26', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(50, 103, 'c2084211-dfd4-4992-bf39-161add006083', 1, 'Уфа', '293146d0-d28f-4c3e-8ea9-7aac594c8132', '2016-06-07 15:20:33', '0000-00-00 00:00:00', 0);
INSERT INTO `addresses_people` (`id`, `people_id`, `people_guid`, `address_type_id`, `address_text`, `address_fias`, `create_date`, `update_date`, `deleted`) VALUES
	(51, 104, '8225c697-33bd-48cd-b7b1-fbbcc19e500d', 1, 'Уфа', 'c3da2de7-d598-4b12-ad38-c023187bf085', '2016-06-07 15:21:59', '0000-00-00 00:00:00', 0);
/*!40000 ALTER TABLE `addresses_people` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.address_type
DROP TABLE IF EXISTS `address_type`;
CREATE TABLE IF NOT EXISTS `address_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Типы адресов';

-- Дамп данных таблицы contingent.address_type: ~3 rows (приблизительно)
DELETE FROM `address_type`;
/*!40000 ALTER TABLE `address_type` DISABLE KEYS */;
INSERT INTO `address_type` (`id`, `name`) VALUES
	(1, 'Адрес регистрации по месту жительства');
INSERT INTO `address_type` (`id`, `name`) VALUES
	(2, 'Адрес регистрации по месту пребывания');
INSERT INTO `address_type` (`id`, `name`) VALUES
	(3, 'Адрес фактического места жительства');
/*!40000 ALTER TABLE `address_type` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.applications_admission_school
DROP TABLE IF EXISTS `applications_admission_school`;
CREATE TABLE IF NOT EXISTS `applications_admission_school` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_educational_institutions` bigint(20) NOT NULL DEFAULT '0',
  `id_people` bigint(20) NOT NULL,
  `educational_class` int(11) NOT NULL DEFAULT '0' COMMENT 'Учебный класс',
  `create_date` date NOT NULL COMMENT 'Дата регистрации заявления о приеме',
  PRIMARY KEY (`id`),
  KEY `FK_applications_admission_school_educational_institutions` (`id_educational_institutions`),
  KEY `FK_applications_admission_school_people` (`id_people`),
  CONSTRAINT `FK_applications_admission_school_educational_institutions` FOREIGN KEY (`id_educational_institutions`) REFERENCES `educational_institutions` (`id`),
  CONSTRAINT `FK_applications_admission_school_people` FOREIGN KEY (`id_people`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Заявления о приеме в школу';

-- Дамп данных таблицы contingent.applications_admission_school: ~0 rows (приблизительно)
DELETE FROM `applications_admission_school`;
/*!40000 ALTER TABLE `applications_admission_school` DISABLE KEYS */;
/*!40000 ALTER TABLE `applications_admission_school` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.birth_certificates
DROP TABLE IF EXISTS `birth_certificates`;
CREATE TABLE IF NOT EXISTS `birth_certificates` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `people_id` bigint(20) NOT NULL,
  `people_guid` varchar(36) NOT NULL,
  `series` varchar(5) NOT NULL COMMENT 'Серия свидетельства о рождении',
  `number` varchar(10) NOT NULL COMMENT 'Номер свидетельства о рождении',
  `issuing_authority` text COMMENT 'Орган выдавший документ',
  `date_issue` date NOT NULL COMMENT 'Дата выдачи свидетельства о рождении',
  `number_act_record` bigint(20) DEFAULT NULL COMMENT 'Номер актовой записи',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Удалить?',
  PRIMARY KEY (`id`),
  KEY `people_id` (`people_id`),
  KEY `people_guid` (`people_guid`),
  CONSTRAINT `FK_birth_certificates_people` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Реквизиты свидетельства о рождении';

-- Дамп данных таблицы contingent.birth_certificates: ~1 rows (приблизительно)
DELETE FROM `birth_certificates`;
/*!40000 ALTER TABLE `birth_certificates` DISABLE KEYS */;
INSERT INTO `birth_certificates` (`id`, `people_id`, `people_guid`, `series`, `number`, `issuing_authority`, `date_issue`, `number_act_record`, `create_date`, `update_date`, `deleted`) VALUES
	(1, 1, '1781ce97-1d7f-11e6-8749-bcaec598f084', 'IIАР', '789026', NULL, '2009-05-06', NULL, '2016-05-19 10:21:26', '0000-00-00 00:00:00', 0);
/*!40000 ALTER TABLE `birth_certificates` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.categories_children_difficult_life_situation
DROP TABLE IF EXISTS `categories_children_difficult_life_situation`;
CREATE TABLE IF NOT EXISTS `categories_children_difficult_life_situation` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник категорий детей, находящихся в трудной жизненной ситуации';

-- Дамп данных таблицы contingent.categories_children_difficult_life_situation: ~30 rows (приблизительно)
DELETE FROM `categories_children_difficult_life_situation`;
/*!40000 ALTER TABLE `categories_children_difficult_life_situation` DISABLE KEYS */;
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(1, 'Дети, оставшиеся без попечения родителей.');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(2, 'Дети-инвалиды ');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(3, 'Дети с ограниченными возможностями здоровья ');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(4, 'Дети - жертвы вооруженных и межнациональных конфликтов, экологических  и техногенных катастроф, стихийных бедствий');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(5, 'Дети - жертвы экологических и техногенных катастроф, стихийных бедствий');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(6, 'Дети из семей беженцев и вынужденных переселенцев');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(7, 'Дети, оказавшиеся в экстремальных условиях');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(8, 'Дети - жертвы насилия');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(9, 'Дети, отбывающие наказание в виде лишения свободы в воспитательных колониях');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(10, 'Дети, с девиантным (общественно опасным) поведением ');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(11, 'Дети, проживающие в малоимущих семьях');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(12, 'Дети с отклонениями в поведении');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(13, 'Дети, жизнедеятельность которых объективно нарушена в результате сложившихся обстоятельств');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(101, 'Смерть родителей;');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(102, 'Лишение родителей родительских прав;');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(103, 'Ограничение родителей в родительских правах;');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(104, 'Признание родителей недееспособными;');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(105, 'Болезнь родителей;');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(106, 'Длительное отсутствие родителей;');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(301, 'Дети с нарушениями слуха ');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(302, 'Дети с нарушениями зрения ');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(303, 'Дети с тяжелыми нарушениями речи ');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(304, 'Дети с нарушением опорно-двигательного аппарата ');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(305, 'Дети с детским церебральным параличом ');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(306, 'Дети с интеллектуальными нарушениями ');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(307, 'Дети с расстройством аутистического спектра ');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(308, 'Дети с задержкой психического развития ');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(309, 'Дети с психическими заболеваниями ');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(310, 'Дети с заболеваниями нервной системы ');
INSERT INTO `categories_children_difficult_life_situation` (`id`, `name`) VALUES
	(311, 'Дети со сложной структурой дефекта ');
/*!40000 ALTER TABLE `categories_children_difficult_life_situation` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.children_difficult_life_situations
DROP TABLE IF EXISTS `children_difficult_life_situations`;
CREATE TABLE IF NOT EXISTS `children_difficult_life_situations` (
  `people_id` bigint(20) NOT NULL,
  `difficult_life_situation` int(11) NOT NULL,
  PRIMARY KEY (`people_id`,`difficult_life_situation`),
  KEY `FK__categories_children_difficult_life_situation` (`difficult_life_situation`),
  CONSTRAINT `FK__categories_children_difficult_life_situation` FOREIGN KEY (`difficult_life_situation`) REFERENCES `categories_children_difficult_life_situation` (`id`),
  CONSTRAINT `FK__people` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Дети находящиеся в трудной жизненной ситуации ';

-- Дамп данных таблицы contingent.children_difficult_life_situations: ~0 rows (приблизительно)
DELETE FROM `children_difficult_life_situations`;
/*!40000 ALTER TABLE `children_difficult_life_situations` DISABLE KEYS */;
/*!40000 ALTER TABLE `children_difficult_life_situations` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.countries
DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `full_name_eng` varchar(255) NOT NULL,
  `name_eng` varchar(255) NOT NULL,
  `iso_num` varchar(3) NOT NULL,
  `iso2` varchar(2) NOT NULL,
  `iso3` varchar(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `iso_num` (`iso_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица со списком названий стран и их кодами в соответствии с ISO 3166. Дополнен буквенными кодами, используемыми в IPTC.';

-- Дамп данных таблицы contingent.countries: ~250 rows (приблизительно)
DELETE FROM `countries`;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(1, 'Албания', 'Албания', 'Albania', 'Albania', '8', 'AL', 'ALB');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(2, 'Алжир', 'Алжир', 'Algeria', 'Algeria', '12', 'DZ', 'DZA');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(3, 'Американский Самоа', 'Американский Самоа', 'American Samoa', 'American Samoa', '16', 'AS', 'ASM');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(4, 'Андорра', 'Андорра', 'Andorra', 'Andorra', '20', 'AD', 'AND');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(5, 'Ангола', 'Ангола', 'Angola', 'Angola', '24', 'AO', 'AGO');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(6, 'Ангилья', 'Ангилья', 'Anguilla', 'Anguilla', '660', 'AI', 'AIA');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(7, 'Антарктика', 'Антарктика', 'Antarctica', 'Antarctica', '10', 'AQ', 'ATA');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(8, 'Антигуа и Барбуда', 'Антигуа и Барбуда', 'Antigua and Barbuda', 'Antigua and Barbuda', '28', 'AG', 'ATG');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(9, 'Аргентина', 'Аргентина', 'Argentina', 'Argentina', '32', 'AR', 'ARG');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(10, 'Армения', 'Армения', 'Armenia', 'Armenia', '51', 'AM', 'ARM');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(11, 'Аруба', 'Аруба', 'Aruba', 'Aruba', '533', 'AW', 'ABW');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(12, 'на море', 'на море', 'at Sea', 'at Sea', '', '', 'XSE');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(13, 'Австралия', 'Австралия', 'Australia', 'Australia', '36', 'AU', 'AUS');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(14, 'Австрия', 'Австрия', 'Austria', 'Austria', '40', 'AT', 'AUT');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(15, 'Азербайджан', 'Азербайджан', 'Azerbaijan', 'Azerbaijan', '31', 'AZ', 'AZE');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(16, 'Багамские острова', 'Багамские острова', 'Bahamas', 'Bahamas', '44', 'BS', 'BHS');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(17, 'Бахрейн', 'Бахрейн', 'Bahrain', 'Bahrain', '48', 'BH', 'BHR');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(18, 'Бангладеш', 'Бангладеш', 'Bangladesh', 'Bangladesh', '50', 'BD', 'BGD');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(19, 'Барбадос', 'Барбадос', 'Barbados', 'Barbados', '52', 'BB', 'BRB');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(20, 'Беларусь', 'Беларусь', 'Belarus', 'Belarus', '112', 'BY', 'BLR');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(21, 'Бельгия', 'Бельгия', 'Belgium', 'Belgium', '56', 'BE', 'BEL');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(22, 'Белиз', 'Белиз', 'Belize', 'Belize', '84', 'BZ', 'BLZ');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(23, 'Бенин', 'Бенин', 'Benin', 'Benin', '204', 'BJ', 'BEN');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(24, 'Бермуды', 'Бермуды', 'Bermuda', 'Bermuda', '60', 'BM', 'BMU');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(25, 'Бутан', 'Бутан', 'Bhutan', 'Bhutan', '64', 'BT', 'BTN');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(26, 'Боливия', 'Боливия', 'Bolivia', 'Bolivia', '68', 'BO', 'BOL');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(27, 'Босния и Герцеговина', 'Босния и Герцеговина', 'Bosnia and Herzegovina', 'Bosnia and Herzegovina', '70', 'BA', 'BIH');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(28, 'Ботсвана', 'Ботсвана', 'Botswana', 'Botswana', '72', 'BW', 'BWA');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(29, 'остров Буве', 'остров Буве', 'Bouvet Island', 'Bouvet Island', '74', 'BV', 'BVT');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(30, 'Бразилия', 'Бразилия', 'Brazil', 'Brazil', '76', 'BR', 'BRA');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(31, 'Британские территории Индийского океана', 'Британские территории Индийского океана', 'British Indian Ocean Territory', 'British Indian Ocean Territory', '86', 'IO', 'IOT');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(32, 'Бруней', 'Бруней', 'Brunei Darussalam', 'Brunei', '96', 'BN', 'BRN');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(33, 'Болгария', 'Болгария', 'Bulgaria', 'Bulgaria', '100', 'BG', 'BGR');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(34, 'Буркина Фасо', 'Буркина Фасо', 'Burkina Faso', 'Burkina Faso', '854', 'BF', 'BFA');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(35, 'Бурунди', 'Бурунди', 'Burundi', 'Burundi', '108', 'BI', 'BDI');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(36, 'Камбоджа', 'Камбоджа', 'Cambodia', 'Cambodia', '116', 'KH', 'KHM');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(37, 'Камерун', 'Камерун', 'Cameroon', 'Cameroon', '120', 'CM', 'CMR');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(38, 'Канада', 'Канада', 'Canada', 'Canada', '124', 'CA', 'CAN');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(39, 'острова Зеленого Мыса', 'о-ва Зеленого Мыса', 'Cape Verde', 'Cape Verde', '132', 'CV', 'CPV');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(40, 'Каймановы острова', 'Каймановы острова', 'Cayman Islands', 'Cayman Islands', '136', 'KY', 'CYM');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(41, 'Центральная Африканская Республика', 'Центральная Африканская Республика', 'Central African Republic', 'Central African Republic', '140', 'CF', 'CAF');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(42, 'Чад', 'Чад', 'Chad', 'Chad', '148', 'TD', 'TCD');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(43, 'Нормандские острова', 'Нормандские острова', 'Channel Islands', 'Channel Islands', '830', '', '');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(44, 'Чили', 'Чили', 'Chile', 'Chile', '152', 'CL', 'CHL');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(45, 'Китайская Народная Республика', 'Китай', 'China', 'China', '156', 'CN', 'CHN');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(46, 'остров Рождества', 'остров Рождества', 'Christmas Island', 'Christmas Island', '162', 'CX', 'CXR');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(47, 'Кокосовые острова', 'Кокосовые острова', 'Cocos (Keeling) Islands', 'Cocos (Keeling) Islands', '166', 'CC', 'CCK');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(48, 'Колумбия', 'Колумбия', 'Colombia', 'Colombia', '170', 'CO', 'COL');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(49, 'Коморские острова', 'Коморские острова', 'Comoros', 'Comoros', '174', 'KM', 'COM');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(50, 'Конго', 'Конго', 'Congo', 'Congo', '178', 'CG', 'COG');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(51, 'Демократическая республика Конго', 'Демократическая республика Конго', 'Congo (Democratic Republic of the)', 'Congo, Democractic Republic', '180', 'CD', 'COD');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(52, 'острова Кука', 'острова Кука', 'Cook Islands', 'Cook Islands', '184', 'CK', 'COK');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(53, 'Коста Рика', 'Коста Рика', 'Costa Rica', 'Costa Rica', '188', 'CR', 'CRI');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(54, 'Кот-д\'Ивуар', 'Кот-д\'Ивуар', 'Cote d\'Ivoire', 'Cote D\'Ivoire', '384', 'CI', 'CIV');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(55, 'Хорватия', 'Хорватия', 'Croatia', 'Croatia', '191', 'HR', 'HRV');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(56, 'Куба', 'Куба', 'Cuba', 'Cuba', '192', 'CU', 'CUB');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(57, 'Кипр', 'Кипр', 'Cyprus', 'Cyprus', '196', 'CY', 'CYP');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(58, 'Чехия', 'Чехия', 'Czech Republic', 'Czech Republic', '203', 'CZ', 'CZE');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(59, 'Дания', 'Дания', 'Denmark', 'Denmark', '208', 'DK', 'DNK');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(60, 'Джибути', 'Джибути', 'Djibouti', 'Djibouti', '262', 'DJ', 'DJI');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(61, 'Доминика', 'Доминика', 'Dominica', 'Dominica', '212', 'DM', 'DMA');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(62, 'Доминиканская республика', 'Доминиканская республика', 'Dominican Republic', 'Dominican Republic', '214', 'DO', 'DOM');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(63, 'Эквадор', 'Эквадор', 'Ecuador', 'Ecuador', '218', 'EC', 'ECU');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(64, 'Египет', 'Египет', 'Egypt', 'Egypt', '818', 'EG', 'EGY');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(65, 'Сальвадор', 'Сальвадор', 'El Salvador', 'El Salvador', '222', 'SV', 'SLV');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(66, 'Англия', 'Англия', 'England', 'England', '', '', 'XEN');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(67, 'Экваториальная Гвинея', 'Экваториальная Гвинея', 'Equatorial Guinea', 'Equatorial Guinea', '226', 'GQ', 'GNQ');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(68, 'Эритрея', 'Эритрея', 'Eritrea', 'Eritrea', '232', 'ER', 'ERI');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(69, 'Эстония', 'Эстония', 'Estonia', 'Estonia', '233', 'EE', 'EST');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(70, 'Эфиопия', 'Эфиопия', 'Ethiopia', 'Ethiopia', '231', 'ET', 'ETH');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(71, 'ЕС', 'ЕС', 'European Union', 'EU', '', '', 'XEU');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(72, 'Фарерские острова', 'Фарерские острова', 'Faroe Islands', 'Faroe Islands', '234', 'FO', 'FRO');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(73, 'Фолклендские (Мальвинские) острова', 'Фолклендские (Мальвинские) острова', 'Falkland Islands (Malvinas)', 'Falkland Islands (Malvinas)', '238', 'FK', 'FLK');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(74, 'Фиджи', 'Фиджи', 'Fiji', 'Fiji', '242', 'FJ', 'FJI');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(75, 'Финляндия', 'Финляндия', 'Finland', 'Finland', '246', 'FI', 'FIN');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(76, 'Франция', 'Франция', 'France', 'France', '250', 'FR', 'FRA');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(77, 'Французская Гвиана', 'Французская Гвиана', 'French Guiana', 'French Guiana', '254', 'GF', 'GUF');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(78, 'Французская Полинезия', 'Французская Полинезия', 'French Polynesia', 'French Polynesia', '258', 'PF', 'PYF');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(79, 'Французские Южные Территории', 'Французские Южные Территории', 'French Southern Territories', 'French Southern Territories', '260', 'TF', 'ATF');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(80, 'Габон', 'Габон', 'Gabon', 'Gabon', '266', 'GA', 'GAB');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(81, 'Гамбия', 'Гамбия', 'Gambia', 'Gambia', '270', 'GM', 'GMB');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(82, 'Грузия', 'Грузия', 'Georgia', 'Georgia', '268', 'GE', 'GEO');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(83, 'Германия', 'Германия', 'Germany', 'Germany', '276', 'DE', 'DEU');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(84, 'Гана', 'Гана', 'Ghana', 'Ghana', '288', 'GH', 'GHA');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(85, 'Гибралтар', 'Гибралтар', 'Gibraltar', 'Gibraltar', '292', 'GI', 'GIB');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(86, 'Греция', 'Греция', 'Greece', 'Greece', '300', 'GR', 'GRC');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(87, 'Гренландия', 'Гренландия', 'Greenland', 'Greenland', '304', 'GL', 'GRL');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(88, 'Гренада', 'Гренада', 'Grenada', 'Grenada', '308', 'GD', 'GRD');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(89, 'Гваделупа', 'Гваделупа', 'Guadeloupe', 'Guadeloupe', '312', 'GP', 'GLP');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(90, 'Гуам', 'Гуам', 'Guam', 'Guam', '316', 'GU', 'GUM');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(91, 'Гватемала', 'Гватемала', 'Guatemala', 'Guatemala', '320', 'GT', 'GTM');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(92, 'Гвинея', 'Гвинея', 'Guinea', 'Guinea', '324', 'GN', 'GIN');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(93, 'Гвинея-Бисау', 'Гвинея-Бисау', 'Guinea-Bissau', 'Guinea-Bissau', '624', 'GW', 'GNB');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(94, 'Гайана', 'Гайана', 'Guyana', 'Guyana', '328', 'GY', 'GUY');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(95, 'Гаити', 'Гаити', 'Haiti', 'Haiti', '332', 'HT', 'HTI');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(96, 'острова Герда и МакДональда', 'о-ва Герда и МакДональда', 'Heard Island and McDonald Islands', 'Heard Island and McDonald Islands', '334', 'HM', 'HMD');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(97, 'Гондурас', 'Гондурас', 'Honduras', 'Honduras', '340', 'HN', 'HND');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(98, 'Гонконг (Китай)', 'Гонконг (Китай)', 'Hong Kong (Special Administrative Region of China)', 'Hong Kong (China)', '344', 'HK', 'HKG');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(99, 'Венгрия', 'Венгрия', 'Hungary', 'Hungary', '348', 'HU', 'HUN');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(100, 'Исландия', 'Исландия', 'Iceland', 'Iceland', '352', 'IS', 'ISL');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(101, 'в полете', 'в полете', 'In Flight', 'In Flight', '', '', 'XIF');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(102, 'Индия', 'Индия', 'India', 'India', '356', 'IN', 'IND');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(103, 'Индонезия', 'Индонезия', 'Indonesia', 'Indonesia', '360', 'ID', 'IDN');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(104, 'Иран', 'Иран', 'Iran (Islamic Republic of)', 'Iran', '364', 'IR', 'IRN');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(105, 'Ирак', 'Ирак', 'Iraq', 'Iraq', '368', 'IQ', 'IRQ');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(106, 'Ирландия', 'Ирландия', 'Ireland', 'Ireland', '372', 'IE', 'IRL');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(107, 'остров Мэн', 'остров Мэн', 'Isle of Man', 'Isle of Man', '833', '', '');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(108, 'Израиль', 'Израиль', 'Israel', 'Israel', '376', 'IL', 'ISR');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(109, 'Италия', 'Италия', 'Italy', 'Italy', '380', 'IT', 'ITA');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(110, 'Ямайка', 'Ямайка', 'Jamaica', 'Jamaica', '388', 'JM', 'JAM');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(111, 'Япония', 'Япония', 'Japan', 'Japan', '392', 'JP', 'JPN');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(112, 'Иордания', 'Иордания', 'Jordan', 'Jordan', '400', 'JO', 'JOR');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(113, 'Казахстан', 'Казахстан', 'Kazakhstan', 'Kazakhstan', '398', 'KZ', 'KAZ');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(114, 'Кения', 'Кения', 'Kenya', 'Kenya', '404', 'KE', 'KEN');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(115, 'Кирибати', 'Кирибати', 'Kiribati', 'Kiribati', '296', 'KI', 'KIR');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(116, 'Корея', 'Корея', 'Korea (Republic of)', 'Korea', '410', 'KR', 'KOR');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(117, 'Корейская Народная Демократическая республика', 'КНДР', 'Korea (Democratic People\'s Republic of)', 'Korea, North', '408', 'KP', 'PRK');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(118, 'Кувейт', 'Кувейт', 'Kuwait', 'Kuwait', '414', 'KW', 'KWT');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(119, 'Кыргызстан', 'Киргизия', 'Kyrgyzstan', 'Kyrgyzstan', '417', 'KG', 'KGZ');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(120, 'Лаос', 'Лаос', 'Lao People\'s Democratic Republic', 'Laos', '418', 'LA', 'LAO');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(121, 'Латвия', 'Латвия', 'Latvia', 'Latvia', '428', 'LV', 'LVA');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(122, 'Ливан', 'Ливан', 'Lebanon', 'Lebanon', '422', 'LB', 'LBN');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(123, 'Лесото', 'Лесото', 'Lesotho', 'Lesotho', '426', 'LS', 'LSO');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(124, 'Либерия', 'Либерия', 'Liberia', 'Liberia', '430', 'LR', 'LBR');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(125, 'Ливия', 'Ливия', 'Libyan Arab Jamahiriya', 'Libya', '434', 'LY', 'LBY');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(126, 'Лихтенштейн', 'Лихтенштейн', 'Liechtenstein', 'Liechtenstein', '438', 'LI', 'LIE');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(127, 'Литва', 'Литва', 'Lithuania', 'Lithuania', '440', 'LT', 'LTU');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(128, 'Люксембург', 'Люксембург', 'Luxembourg', 'Luxembourg', '442', 'LU', 'LUX');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(129, 'Макао (Китай)', 'Макао (Китай)', 'Macau (Special Administrative Region of China)', 'Macau (China)', '446', 'MO', 'MAC');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(130, 'Македония', 'Македония', 'Macedonia (The former Yugoslav Republic of)', 'Macedonia', '807', 'MK', 'MKD');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(131, 'Мадагаскар', 'Мадагаскар', 'Madagascar', 'Madagascar', '450', 'MG', 'MDG');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(132, 'Малави', 'Малави', 'Malawi', 'Malawi', '454', 'MW', 'MWI');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(133, 'Малайзия', 'Малайзия', 'Malaysia', 'Malaysia', '458', 'MY', 'MYS');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(134, 'Мальдивские острова', 'Мальдивские острова', 'Maldives', 'Maldives', '462', 'MV', 'MDV');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(135, 'Мали', 'Мали', 'Mali', 'Mali', '466', 'ML', 'MLI');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(136, 'Мальта', 'Мальта', 'Malta', 'Malta', '470', 'MT', 'MLT');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(137, 'Маршалловы острова', 'Маршалловы острова', 'Marshall Islands', 'Marshall Islands', '584', 'MH', 'MHL');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(138, 'Мартиника', 'Мартиника', 'Martinique', 'Martinique', '474', 'MQ', 'MTQ');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(139, 'Мавритания', 'Мавритания', 'Mauritania', 'Mauritania', '478', 'MR', 'MRT');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(140, 'Маврикий', 'Маврикий', 'Mauritius', 'Mauritius', '480', 'MU', 'MUS');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(141, 'Майотта', 'Майотта', 'Mayotte', 'Mayotte', '175', 'YT', 'MYT');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(142, 'Мексика', 'Мексика', 'Mexico', 'Mexico', '484', 'MX', 'MEX');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(143, 'Микронезия', 'Микронезия', 'Micronesia (Federated States of)', 'Micronesia', '583', 'FM', 'FSM');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(144, 'Молдова', 'Молдова', 'Moldova (Republic of)', 'Moldova', '498', 'MD', 'MDA');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(145, 'Монако', 'Монако', 'Monaco', 'Monaco', '492', 'MC', 'MCO');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(146, 'Монголия', 'Монголия', 'Mongolia', 'Mongolia', '496', 'MN', 'MNG');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(147, 'Монтсеррат', 'Монтсеррат', 'Montserrat', 'Montserrat', '500', 'MS', 'MSR');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(148, 'Марокко', 'Марокко', 'Morocco', 'Morocco', '504', 'MA', 'MAR');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(149, 'Мозамбик', 'Мозамбик', 'Mozambique', 'Mozambique', '508', 'MZ', 'MOZ');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(150, 'Мьянма', 'Мьянма', 'Myanmar', 'Myanmar', '104', 'MM', 'MMR');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(151, 'Намибия', 'Намибия', 'Namibia', 'Namibia', '516', 'NA', 'NAM');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(152, 'Науру', 'Науру', 'Nauru', 'Nauru', '520', 'NR', 'NRU');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(153, 'Непал', 'Непал', 'Nepal', 'Nepal', '524', 'NP', 'NPL');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(154, 'Нидерланды', 'Нидерланды', 'Netherlands', 'Netherlands', '528', 'NL', 'NLD');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(155, 'Антильские острова', 'Антильские острова', 'Netherlands Antilles', 'Netherlands Antilles', '530', 'AN', 'ANT');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(156, 'Новая Каледония', 'Новая Каледония', 'New Caledonia', 'New Caledonia', '540', 'NC', 'NCL');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(157, 'Новая Зеландия', 'Новая Зеландия', 'New Zealand', 'New Zealand', '554', 'NZ', 'NZL');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(158, 'Никарагуа', 'Никарагуа', 'Nicaragua', 'Nicaragua', '558', 'NI', 'NIC');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(159, 'Нигер', 'Нигер', 'Niger', 'Niger', '562', 'NE', 'NER');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(160, 'Нигерия', 'Нигерия', 'Nigeria', 'Nigeria', '566', 'NG', 'NGA');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(161, 'Ниуэ', 'Ниуэ', 'Niue', 'Niue', '570', 'NU', 'NIU');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(162, 'остров Норфолк', 'остров Норфолк', 'Norfolk Island', 'Norfolk Island', '574', 'NF', 'NFK');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(163, 'Марианские острова', 'Марианские острова', 'Northern Mariana Islands', 'Northern Mariana Islands', '580', 'MP', 'MNP');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(164, 'Норвегия', 'Норвегия', 'Norway', 'Norway', '578', 'NO', 'NOR');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(165, 'Северная Ирландия', 'Северная Ирландия', 'Nothern Ireland', 'Nothern Ireland', '', '', 'XNI');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(166, 'Оман', 'Оман', 'Oman', 'Oman', '512', 'OM', 'OMN');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(167, 'Пакистан', 'Пакистан', 'Pakistan', 'Pakistan', '586', 'PK', 'PAK');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(168, 'Палау', 'Палау', 'Palau', 'Palau', '585', 'PW', 'PLW');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(169, 'Палестина', 'Палестина', 'Palestinian Territory (Occupied)', 'Palestine', '275', 'PS', 'PSE');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(170, 'Панама', 'Панама', 'Panama', 'Panama', '591', 'PA', 'PAN');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(171, 'Папуа - Новая Гвинея', 'Папуа - Новая Гвинея', 'Papua New Guinea', 'Papua new Guinea', '598', 'PG', 'PNG');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(172, 'Парагвай', 'Парагвай', 'Paraguay', 'Paraguay', '600', 'PY', 'PRY');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(173, 'Перу', 'Перу', 'Peru', 'Peru', '604', 'PE', 'PER');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(174, 'Филиппины', 'Филиппины', 'Philippines', 'Philippines', '608', 'PH', 'PHL');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(175, 'остров Питкэрн', 'остров Питкэрн', 'Pitcairn', 'Pitcairn', '612', 'PN', 'PCN');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(176, 'Польша', 'Польша', 'Poland', 'Poland', '616', 'PL', 'POL');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(177, 'Португалия', 'Португалия', 'Portugal', 'Portugal', '620', 'PT', 'PRT');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(178, 'Пуэрто-Рико', 'Пуэрто-Рико', 'Puerto Rico', 'Puerto Rico', '630', 'PR', 'PRI');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(179, 'Катар', 'Катар', 'Qatar', 'Qatar', '634', 'QA', 'QAT');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(180, 'Реюньон', 'Реюньон', 'Reunion', 'Reunion', '638', 'RE', 'REU');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(181, 'Румыния', 'Румыния', 'Romania', 'Romania', '642', 'RO', 'ROU');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(182, 'Россия', 'Россия', 'Russian Federation', 'Russia', '643', 'RU', 'RUS');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(183, 'Руанда', 'Руанда', 'Rwanda', 'Rwanda', '646', 'RW', 'RWA');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(184, 'остров Святой Елены', 'остров Святой Елены', 'Saint Helena', 'Saint Helena', '654', 'SH', 'SHN');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(185, 'Сент-Китс и Невис', 'Сент-Китс и Невис', 'Saint Kitts and Nevis', 'Saint Kitts and Nevis', '659', 'KN', 'KNA');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(186, 'Сент-Люсия', 'Сент-Люсия', 'Saint Lucia', 'Saint Lucia', '662', 'LC', 'LCA');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(187, 'Сен-Пьер и Микелон', 'Сен-Пьер и Микелон', 'Saint Pierre and Miquelon', 'Saint Pierre and Miquelon', '666', 'PM', 'SPM');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(188, 'Сент-Винсент и Гренадины', 'Сент-Винсент и Гренадины', 'Saint Vincent and the Grenadines', 'Saint Vincent and The Grenadines', '670', 'VC', 'VCT');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(189, 'острова Самоа', 'о-ва Самоа', 'Samoa', 'Samoa', '882', 'WS', 'WSM');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(190, 'Сан-Марино', 'Сан-Марино', 'San Marino', 'San Marino', '674', 'SM', 'SMR');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(191, 'Сан-Томе и Принсипи', 'Сан-Томе и Принсипи', 'Sao Tome and Principe', 'Sao Tome and Principe', '678', 'ST', 'STP');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(192, 'Саудовская Аравия', 'Саудовская Аравия', 'Saudi Arabia', 'Saudi Arabia', '682', 'SA', 'SAU');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(193, 'Шотландия', 'Шотландия', 'Scotland', 'Scotland', '', '', 'XSC');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(194, 'Сенегал', 'Сенегал', 'Senegal', 'Senegal', '686', 'SN', 'SEN');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(195, 'Сейшельские острова', 'Сейшельские острова', 'Seychelles', 'Seychelles', '690', 'SC', 'SYC');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(196, 'Сьерра-Леоне', 'Сьерра-Леоне', 'Sierra Leone', 'Sierra Leone', '694', 'SL', 'SLE');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(197, 'Сингапур', 'Сингапур', 'Singapore', 'Singapore', '702', 'SG', 'SGP');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(198, 'Словакия', 'Словакия', 'Slovakia', 'Slovakia', '703', 'SK', 'SVK');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(199, 'Словения', 'Словения', 'Slovenia', 'Slovenia', '705', 'SI', 'SVN');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(200, 'Соломоновы острова', 'Соломоновы острова', 'Solomon Islands', 'Solomon Islands', '90', 'SB', 'SLB');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(201, 'Сомали', 'Сомали', 'Somalia', 'Somalia', '706', 'SO', 'SOM');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(202, 'ЮАР', 'ЮАР', 'South Africa', 'South Africa', '710', 'ZA', 'ZAF');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(203, 'Южная Георгия и Южные Сандвичевы острова', 'Южная Георгия и Южные Сандвичевы о-ва', 'South Georgia and The South Sandwich Islands', 'South Georgia and The South Sandwich Islands', '239', 'GS', 'SGS');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(204, 'в космосе', 'в космосе', 'Space', 'Space', '', '', 'XSP');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(205, 'Испания', 'Испания', 'Spain', 'Spain', '724', 'ES', 'ESP');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(206, 'Шри Ланка', 'Шри Ланка', 'Sri Lanka', 'Sri Lanka', '144', 'LK', 'LKA');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(207, 'Судан', 'Судан', 'Sudan', 'Sudan', '736', 'SD', 'SDN');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(208, 'Суринам', 'Суринам', 'Suriname', 'Suriname', '740', 'SR', 'SUR');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(209, 'острова Свалбард и Ян Майен', 'о-ва Свалбард и Ян Майен', 'Svalbard and Jan Mayen Islands', 'Svalbard and Jan Mayen Islands', '744', 'SJ', 'SJM');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(210, 'Свазиленд', 'Свазиленд', 'Swaziland', 'Swaziland', '748', 'SZ', 'SWZ');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(211, 'Швеция', 'Швеция', 'Sweden', 'Sweden', '752', 'SE', 'SWE');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(212, 'Швейцария', 'Швейцария', 'Switzerland', 'Switzerland', '756', 'CH', 'CHE');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(213, 'Сирия', 'Сирия', 'Syrian Arab Republic', 'Syria', '760', 'SY', 'SYR');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(214, 'Тайвань (Республика Китай)', 'Тайвань (Китай)', 'Taiwan (Province of China)', 'Taiwan (China)', '158', 'TW', 'TWN');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(215, 'Таджикистан', 'Таджикистан', 'Tajikistan', 'Tajikistan', '762', 'TJ', 'TJK');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(216, 'Танзания', 'Танзания', 'Tanzania (United Republic of)', 'Tanzania', '834', 'TZ', 'TZA');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(217, 'Тайланд', 'Тайланд', 'Thailand', 'Thailand', '764', 'TH', 'THA');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(218, 'Восточный Тимор', 'Восточный Тимор', 'Timor-Leste (East Timor)', 'Timor-Leste', '626', 'TL', 'TLS');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(219, 'Того', 'Того', 'Togo', 'Togo', '768', 'TG', 'TGO');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(220, 'Токелау', 'Токелау', 'Tokelau', 'Tokelau', '772', 'TK', 'TKL');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(221, 'Тонга', 'Тонга', 'Tonga', 'Tonga', '776', 'TO', 'TON');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(222, 'Тринидад и Тобаго', 'Тринидад и Тобаго', 'Trinidad and Tobago', 'Trinidad And Tobago', '780', 'TT', 'TTO');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(223, 'Тунис', 'Тунис', 'Tunisia', 'Tunisia', '788', 'TN', 'TUN');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(224, 'Турция', 'Турция', 'Turkey', 'Turkey', '792', 'TR', 'TUR');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(225, 'Туркменистан', 'Туркменистан', 'Turkmenistan', 'Turkmenistan', '795', 'TM', 'TKM');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(226, 'острова Туркс и Кайкос', 'о-ва Туркс и Кайкос', 'Turks and Caicos Islands', 'Turks and Caicos Islands', '796', 'TC', 'TCA');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(227, 'Тувалу', 'Тувалу', 'Tuvalu', 'Tuvalu', '798', 'TV', 'TUV');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(228, 'Уганда', 'Уганда', 'Uganda', 'Uganda', '800', 'UG', 'UGA');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(229, 'Украина', 'Украина', 'Ukraine', 'Ukraine', '804', 'UA', 'UKR');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(230, 'ООН', 'ООН', 'United Nations', 'UN', '', '', 'XUN');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(231, 'Арабские Эмираты', 'Арабские Эмираты', 'United Arab Emirates', 'United Arab Emirates', '784', 'AE', 'ARE');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(232, 'Великобритания', 'Великобритания', 'United Kingdom', 'UK', '826', 'GB', 'GBR');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(233, 'Соединенные Штаты Америки', 'США', 'United States', 'USA', '840', 'US', 'USA');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(234, 'Отдаленные Острова США', 'Отдаленные Острова США', 'United States Minor Outlying Islands', 'United States Minor Outlying Islands', '581', 'UM', 'UMI');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(235, 'Уругвай', 'Уругвай', 'Uruguay', 'Uruguay', '858', 'UY', 'URY');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(236, 'Узбекистан', 'Узбекистан', 'Uzbekistan', 'Uzbekistan', '860', 'UZ', 'UZB');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(237, 'Вануату', 'Вануату', 'Vanuatu', 'Vanuatu', '548', 'VU', 'VUT');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(238, 'Ватикан', 'Ватикан', 'Holy See (Vatican City State)', 'Holy See (Vatican)', '336', 'VA', 'VAT');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(239, 'Венесуэла', 'Венесуэла', 'Venezuela', 'Venezuela', '862', 'VE', 'VEN');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(240, 'Вьетнам', 'Вьетнам', 'Viet Nam', 'Vietnam', '704', 'VN', 'VNM');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(241, 'Виргинские острова (Британские)', 'Виргинские острова (Британские)', 'Virgin Islands (British)', 'Virgin Islands (British)', '92', 'VG', 'VGB');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(242, 'Виргинские острова (США)', 'Виргинские острова (США)', 'Virgin Islands (US)', 'Virgin Islands (US)', '850', 'VI', 'VIR');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(243, 'Уэльс', 'Уэльс', 'Wales', 'Wales', '', '', 'XWA');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(244, 'острова Валлис и Футуна', 'о-ва Валлис и Футуна', 'Wallis and Futuna Islands', 'Wallis and Futuna Islands', '876', 'WF', 'WLF');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(245, 'Западная Сахара', 'Западная Сахара', 'Western Sahara', 'Western Sahara', '732', 'EH', 'ESH');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(246, 'Йемен', 'Йемен', 'Yemen', 'Yemen', '887', 'YE', 'YEM');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(247, 'Югославия', 'Югославия', 'Yugoslavia', 'Yugoslavia', '891', 'YU', 'YUG');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(248, 'Замбия', 'Замбия', 'Zambia', 'Zambia', '894', 'ZM', 'ZMB');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(249, 'Зимбабве', 'Зимбабве', 'Zimbabwe', 'Zimbabwe', '716', 'ZW', 'ZWE');
INSERT INTO `countries` (`id`, `full_name`, `name`, `full_name_eng`, `name_eng`, `iso_num`, `iso2`, `iso3`) VALUES
	(250, 'Афганистан', 'Афганистан', 'Afghanistan', 'Afghanistan', '4', 'AF', 'AFG');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.data_exchange
DROP TABLE IF EXISTS `data_exchange`;
CREATE TABLE IF NOT EXISTS `data_exchange` (
  `guid` varchar(36) NOT NULL,
  `method` text NOT NULL,
  `message` text,
  `status` int(11) DEFAULT NULL,
  `request` longtext,
  `response` longtext,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Запрос \\ Ответ';

-- Дамп данных таблицы contingent.data_exchange: ~17 rows (приблизительно)
DELETE FROM `data_exchange`;
/*!40000 ALTER TABLE `data_exchange` DISABLE KEYS */;
INSERT INTO `data_exchange` (`guid`, `method`, `message`, `status`, `request`, `response`, `create_date`, `update_date`) VALUES
	('1791d51c-1d7f-11e6-8749-bcaec598f084', 'get_people', NULL, 1, '1781d51c-1d7f-11e6-8749-bcaec598f084', '[{"guid":"1781d51c-1d7f-11e6-8749-bcaec598f084","full_name":"Родион Суфияров Даниэлевич","date_birth":"\\/Date(1413828000000)\\/","birthplace":"Уфа","birthplace_fias":"21606","sex":"М","snils":"","group_health":1,"sports_group":2,"citizenship_iso":null,"categories_children_difficult_life_situation":[]}]', '2016-07-14 12:48:44', '2016-07-15 14:28:26');
INSERT INTO `data_exchange` (`guid`, `method`, `message`, `status`, `request`, `response`, `create_date`, `update_date`) VALUES
	('4253a66f-2570-4b40-92f1-d6022d6111e8', 'any_command', NULL, -1, '[{"guid":"3a55bb64-2a95-4dc2-a598-a54199c0e69c","full_name":"??? ?? ?????","date_birth":"/Date(1467787916606)/","birthplace":"???","birthplace_fias":"7cd0b7b0-d265-4654-8e46-3e4c071db7f9","sex":"?","snils":"46546546565","group_health":null,"sports_group":null,"citizenship_iso":[10,15,25],"categories_children_difficult_life_situation":null}]', 'Запрощенный метод не существует', '2016-07-12 10:04:51', '2016-07-14 12:11:46');
INSERT INTO `data_exchange` (`guid`, `method`, `message`, `status`, `request`, `response`, `create_date`, `update_date`) VALUES
	('4253a76f-2570-4b40-92f1-d6022d6111e8', 'add_people', NULL, -1, '[{"guid":"21988a23-e505-49d8-b13e-1e08387b6142","full_name":"Газизовв Рамильв Ралифовичв","date_birth":"2015-09-01T00:00:00","birthplace":"","birthplace_fiase":"","sex":"Мужской","snils":"","group_health":1,"sport_group":1,"citizenship_iso":[12345],"categories_children_difficult_life_situation":[67890]}]', 'Ошибка при сериализации переданнго объекта: Ссылка на объект не указывает на экземпляр объекта.', '2016-07-15 12:48:10', '2016-07-15 14:40:30');
INSERT INTO `data_exchange` (`guid`, `method`, `message`, `status`, `request`, `response`, `create_date`, `update_date`) VALUES
	('4253g66f-2570-4b40-92f1-d6022d6111e8', 'add_people', NULL, -1, '[{"guid":"21988a13-e505-49d8-b13e-1e08387b6142","full_name":"Газизовв Рамильв Ралифовичв","date_birth":"2015-09-01T00:00:00","birthplace":"","birthplace_fiase":"","sex":"Male","snils":"","group_health":1,"sport_group":1,"citizenship_iso":[12345],"categories_children_difficult_life_situation":[67890]}]', 'Ошибка при сериализации переданнго объекта: Ссылка на объект не указывает на экземпляр объекта.', '2016-07-15 12:39:11', '2016-07-15 12:42:29');
INSERT INTO `data_exchange` (`guid`, `method`, `message`, `status`, `request`, `response`, `create_date`, `update_date`) VALUES
	('81988a13-e505-49d7-b13e-2e08387b6112', 'add_people', NULL, -1, '[{"guid":"81988a13-e505-49d7-b13e-2e08387b6182","full_name":"???????? ??????? ??????????","date_birth":"2015-09-01T00:00:00","birthplace":"","birthplace_fiase":"","sex":"Male","snils":"","group_health":1,"sport_group":1,"citizenship_iso":12345,"categories_children_difficult_life_situation":67890}]', 'Ошибка при сериализации переданнго объекта: Не удается преобразовать объект типа "System.Int32" в тип "System.Int32[]"', '2016-07-13 11:14:26', '2016-07-14 12:11:46');
INSERT INTO `data_exchange` (`guid`, `method`, `message`, `status`, `request`, `response`, `create_date`, `update_date`) VALUES
	('81988a13-e505-49d7-b13e-2e08387b6122', 'get_people', '', -1, '[object Object]', 'не верный guid запроса', '2016-07-13 11:08:37', '2016-07-14 12:11:46');
INSERT INTO `data_exchange` (`guid`, `method`, `message`, `status`, `request`, `response`, `create_date`, `update_date`) VALUES
	('81988a13-e505-49d7-b13e-2e08387b6132', 'get_people', '', -1, '[object Object]', 'не верный guid запроса', '2016-07-13 11:08:24', '2016-07-14 12:11:47');
INSERT INTO `data_exchange` (`guid`, `method`, `message`, `status`, `request`, `response`, `create_date`, `update_date`) VALUES
	('81988a13-e505-49d7-b13e-2e08387b6142', 'add_people', '', -1, '[{"guid":"81988a13-e505-49d7-b13e-2e08387b6142","full_name":"Газизовв Рамильв Ралифовичв","date_birth":"2015-09-01T00:00:00","birthplace":"","birthplace_fiase":"","sex":"Male","snils":"","group_health":1,"sport_group":1,"citizenship_iso":12345,"categories_children_difficult_life_situation":67890}]', 'Ошибка при сериализации переданнго объекта: Не удается преобразовать объект типа "System.Int32" в тип "System.Int32[]"', '2016-07-13 11:59:19', '2016-07-14 12:14:49');
INSERT INTO `data_exchange` (`guid`, `method`, `message`, `status`, `request`, `response`, `create_date`, `update_date`) VALUES
	('81988a13-e505-49d7-b13e-2e08387b6182', 'add_people', '', -1, 'undefined', 'Ошибка при сериализации переданнго объекта: Недопустимый примитив JSON: undefined.', '2016-07-13 09:20:00', '2016-07-14 12:11:47');
INSERT INTO `data_exchange` (`guid`, `method`, `message`, `status`, `request`, `response`, `create_date`, `update_date`) VALUES
	('81988a13-e505-49d7-b13e-2e08387b6184', 'get_people', '', -1, 'undefined', 'не верный guid запроса', '2016-07-13 09:22:26', '2016-07-14 12:11:47');
INSERT INTO `data_exchange` (`guid`, `method`, `message`, `status`, `request`, `response`, `create_date`, `update_date`) VALUES
	('81988a13-e505-49d7-b13e-2e08387b6185', 'get_people', '', -1, '81988a13-e505-49d7-b13e-2e08387b6182', 'Человек с guid 81988a13-e505-49d7-b13e-2e08387b6182не найден', '2016-07-13 09:39:02', '2016-07-14 12:11:48');
INSERT INTO `data_exchange` (`guid`, `method`, `message`, `status`, `request`, `response`, `create_date`, `update_date`) VALUES
	('81988a13-e505-49d7-b13e-2e08387b6186', 'get_people', '', -1, 'undefined', 'не верный guid запроса', '2016-07-13 09:24:03', '2016-07-14 12:11:48');
INSERT INTO `data_exchange` (`guid`, `method`, `message`, `status`, `request`, `response`, `create_date`, `update_date`) VALUES
	('81988a13-e505-49d7-b13e-2e08387b6188', 'get_people', '', -1, '81988a13-e505-49d7-b13e-2e08387b6182', 'Человек с guid 81988a13-e505-49d7-b13e-2e08387b6182не найден', '2016-07-13 09:43:45', '2016-07-14 12:11:48');
INSERT INTO `data_exchange` (`guid`, `method`, `message`, `status`, `request`, `response`, `create_date`, `update_date`) VALUES
	('81988a13-e505-49d7-b13e-2e08387b6189', 'get_people', '', -1, '81988a13-e505-49d7-b13e-2e08387b6182', 'Человек с guid 81988a13-e505-49d7-b13e-2e08387b6182не найден', '2016-07-13 09:44:33', '2016-07-14 12:11:48');
INSERT INTO `data_exchange` (`guid`, `method`, `message`, `status`, `request`, `response`, `create_date`, `update_date`) VALUES
	('81988a13-e505-49d7-b13e-2e08387b6222', 'add_people', '', -1, '[object Object]', 'Ошибка при сериализации переданнго объекта: Недопустимый примитив JSON: object.', '2016-07-13 11:04:46', '2016-07-14 12:11:48');
INSERT INTO `data_exchange` (`guid`, `method`, `message`, `status`, `request`, `response`, `create_date`, `update_date`) VALUES
	('81988a13-e505-49d8-b13e-2e08387b6112', 'get_people', NULL, 2, '"21988a23-e705-49d1-b13e-1e08387b6142"', '[{"guid":"21988a23-e705-49d1-b13e-1e08387b6142","full_name":"Газизовв Рамильв Ралифовичв","date_birth":"\\/Date(1441047600000)\\/","birthplace":"","birthplace_fias":null,"sex":"Мужской","snils":"","group_health":1,"sports_group":null,"citizenship_iso":[643],"categories_children_difficult_life_situation":[]}]', '2016-07-15 15:34:54', '2016-07-18 12:07:18');
INSERT INTO `data_exchange` (`guid`, `method`, `message`, `status`, `request`, `response`, `create_date`, `update_date`) VALUES
	('81989a13-e505-49d7-b13e-2e08387b6189', 'add_people', NULL, 2, '[{"guid":"21988a23-e705-49d1-b13e-1e08387b6142","full_name":"Газизовв Рамильв Ралифовичв","date_birth":"2015-09-01T00:00:00","birthplace":"","birthplace_fiase":"","sex":"Мужской","snils":"","group_health":1,"sport_group":1,"citizenship_iso":[643],"categories_children_difficult_life_situation":[67890]}]', 'Обект успено сохранен в базу', '2016-07-15 14:50:26', '2016-07-15 15:12:03');
/*!40000 ALTER TABLE `data_exchange` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.disability_people
DROP TABLE IF EXISTS `disability_people`;
CREATE TABLE IF NOT EXISTS `disability_people` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `guid` varchar(36) NOT NULL,
  `people_id` bigint(20) NOT NULL,
  `type_disability` int(11) NOT NULL,
  `category_disability` int(11) DEFAULT NULL,
  `validity_period` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_disability_people_types_categories_disability` (`type_disability`),
  KEY `FK_disability_people_types_categories_disability_2` (`category_disability`),
  KEY `FK_disability_people_people` (`people_id`),
  KEY `guid` (`guid`),
  CONSTRAINT `FK_disability_people_people` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`),
  CONSTRAINT `FK_disability_people_types_categories_disability` FOREIGN KEY (`type_disability`) REFERENCES `types_categories_disability` (`id`),
  CONSTRAINT `FK_disability_people_types_categories_disability_2` FOREIGN KEY (`category_disability`) REFERENCES `types_categories_disability` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COMMENT='Инвалидности людей';

-- Дамп данных таблицы contingent.disability_people: ~31 rows (приблизительно)
DELETE FROM `disability_people`;
/*!40000 ALTER TABLE `disability_people` DISABLE KEYS */;
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(1, 'f38f66ea-37fd-474e-9605-92a933d7e2fc', 1, 22, 11, '2027-05-23');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(2, '562bdbd2-9c26-4fb4-baa1-a715680a5623', 2, 22, 11, '2026-12-16');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(3, '3c11bc98-233d-4c4f-9374-9fdf60013f62', 3, 22, 11, '2038-06-03');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(4, '2d2944ae-6169-4702-a686-a497af7ecf27', 4, 22, 11, '2022-02-02');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(5, 'ff3a4f18-2025-447a-beae-6a619d47cd66', 5, 22, 11, '2032-02-16');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(6, '20b7334e-33f9-4237-95d5-d09c128538af', 6, 22, 11, '2040-03-18');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(7, '9df18665-5efd-4955-9483-58f2caead3f7', 7, 22, 11, '2023-02-19');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(8, '45668f3a-ba4b-4cd9-9963-cf7384a38bb4', 8, 22, 11, '2021-03-24');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(9, '0b8abe70-7b4c-4c2c-8c82-3b53fe3eb084', 9, 22, 11, '2033-04-05');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(10, '2bd2968a-efc0-47ab-8092-8f5c7847e901', 10, 22, 11, '2030-09-02');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(11, 'ca585048-05b3-4bc5-b0a8-317b67f2c009', 84, 22, 11, '2043-06-23');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(12, 'f272c180-4e50-41d0-9d44-c5716cd3f9bc', 85, 22, 11, '2038-06-11');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(13, 'd40b2b5d-cf4b-40ce-9330-49ac5dfc1644', 86, 22, 11, '2036-01-03');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(14, '93384526-83d9-46e2-af52-3e7a26cd978e', 87, 22, 11, '2036-08-04');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(15, 'd6565d2d-9f79-4776-add2-0b9a1f2d0735', 88, 22, 11, '2035-08-12');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(16, '9d0b9a64-f554-4ec7-a31c-a74ac7417409', 89, 22, 11, '2031-12-07');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(17, '35bc3291-6eae-4228-b08d-6936725e1457', 90, 22, 11, '2037-06-25');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(18, 'c5daf726-fc9c-4b43-9855-b9f4b3d6d591', 91, 22, 11, '2029-01-11');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(19, 'b21cdef1-d728-4f48-9138-7ccafae11cd8', 92, 22, 11, '2025-09-23');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(20, '9d921b8a-6c0e-4830-a757-37059f371d67', 93, 22, 11, '2040-11-11');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(21, '186e4b8d-5b61-492f-a6e3-0fda272b68d3', 94, 22, 11, '2029-10-25');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(22, '9939c876-bef5-4209-906b-8c809346a9ee', 95, 22, 11, '2032-02-04');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(23, '2e21684b-71f8-4308-8d4f-ba117e8620c4', 96, 22, 11, '2038-09-21');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(24, '6d281dfe-fb7c-45e3-bec0-32294224be7e', 97, 22, 11, '2042-05-25');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(25, '1c8cb765-438a-4c26-b314-8a3d918e7fd5', 98, 22, 11, '2025-09-02');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(26, 'ca340140-21c8-4ee1-a50b-a263e090f918', 99, 22, 11, '2025-07-05');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(27, '6a176d38-bc76-4a52-b7b9-d59207344ba6', 100, 22, 11, '2039-07-15');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(28, '4b458ebc-ef15-48ad-b5fb-f15b98266c76', 101, 22, 11, '2036-12-23');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(29, '9893ffaa-785c-4f28-aee4-0a8e47f03fc0', 102, 22, 11, '2018-08-06');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(30, 'b562b9fe-6924-48e5-8338-085dcac7f41f', 103, 22, 11, '2034-07-19');
INSERT INTO `disability_people` (`id`, `guid`, `people_id`, `type_disability`, `category_disability`, `validity_period`) VALUES
	(31, '8806033c-f888-4e24-a0da-5f78cba84120', 104, 22, 11, '2037-08-27');
/*!40000 ALTER TABLE `disability_people` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.documents_proving_identity
DROP TABLE IF EXISTS `documents_proving_identity`;
CREATE TABLE IF NOT EXISTS `documents_proving_identity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `guid` varchar(36) NOT NULL,
  `people_id` bigint(20) NOT NULL,
  `people_guid` varchar(36) NOT NULL,
  `document_type` int(11) NOT NULL COMMENT 'Вид документа, удостоверяющего личность',
  `series` varchar(10) NOT NULL COMMENT 'Серия документа, удостоверяющего личность',
  `number` varchar(10) NOT NULL COMMENT 'Номер документа, удостоверяющего личность',
  `date_issue_document` date DEFAULT NULL COMMENT 'Дата выдачи документа',
  `document_valid_to` date DEFAULT NULL COMMENT 'Документ действителен до',
  `place_issue_document` text COMMENT 'Место выдачи документа',
  `issuing_authority` text COMMENT 'Орган выдавший докумен',
  `creat_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Удален?',
  PRIMARY KEY (`id`),
  KEY `people_guid` (`people_guid`),
  KEY `FK_documents_proving_identity_people` (`people_id`),
  KEY `series_number` (`series`,`number`),
  KEY `FK_documents_proving_identity_types_identity_documents` (`document_type`),
  KEY `guid` (`guid`),
  CONSTRAINT `FK_documents_proving_identity_people` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`),
  CONSTRAINT `FK_documents_proving_identity_types_identity_documents` FOREIGN KEY (`document_type`) REFERENCES `types_identity_documents` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8 COMMENT='Документы удостоверяющие личность';

-- Дамп данных таблицы contingent.documents_proving_identity: ~22 rows (приблизительно)
DELETE FROM `documents_proving_identity`;
/*!40000 ALTER TABLE `documents_proving_identity` DISABLE KEYS */;
INSERT INTO `documents_proving_identity` (`id`, `guid`, `people_id`, `people_guid`, `document_type`, `series`, `number`, `date_issue_document`, `document_valid_to`, `place_issue_document`, `issuing_authority`, `creat_date`, `update_date`, `deleted`) VALUES
	(47, '', 1, '1781ce97-1d7f-11e6-8749-bcaec598f084', 21, '8006', '280995', '2007-08-15', '0000-00-00', '', '', '2016-05-24 10:34:01', '0000-00-00 00:00:00', 0);
INSERT INTO `documents_proving_identity` (`id`, `guid`, `people_id`, `people_guid`, `document_type`, `series`, `number`, `date_issue_document`, `document_valid_to`, `place_issue_document`, `issuing_authority`, `creat_date`, `update_date`, `deleted`) VALUES
	(48, '', 1, '1781ce97-1d7f-11e6-8749-bcaec598f084', 21, '8005', '907005', '2005-08-09', '0000-00-00', '', '', '2016-05-24 10:34:01', '0000-00-00 00:00:00', 0);
INSERT INTO `documents_proving_identity` (`id`, `guid`, `people_id`, `people_guid`, `document_type`, `series`, `number`, `date_issue_document`, `document_valid_to`, `place_issue_document`, `issuing_authority`, `creat_date`, `update_date`, `deleted`) VALUES
	(49, '', 1, '1781ce97-1d7f-11e6-8749-bcaec598f084', 21, '8006', '107505', '2006-09-17', '0000-00-00', '', '', '2016-05-24 10:34:01', '0000-00-00 00:00:00', 0);
INSERT INTO `documents_proving_identity` (`id`, `guid`, `people_id`, `people_guid`, `document_type`, `series`, `number`, `date_issue_document`, `document_valid_to`, `place_issue_document`, `issuing_authority`, `creat_date`, `update_date`, `deleted`) VALUES
	(50, '', 1, '1781ce97-1d7f-11e6-8749-bcaec598f084', 21, '8015', '211012', '2015-07-15', '0000-00-00', '', '', '2016-05-24 10:34:01', '0000-00-00 00:00:00', 0);
INSERT INTO `documents_proving_identity` (`id`, `guid`, `people_id`, `people_guid`, `document_type`, `series`, `number`, `date_issue_document`, `document_valid_to`, `place_issue_document`, `issuing_authority`, `creat_date`, `update_date`, `deleted`) VALUES
	(51, '', 1, '1781ce97-1d7f-11e6-8749-bcaec598f084', 21, '8013', '796016', '2016-02-01', '0000-00-00', '', '', '2016-05-24 10:34:01', '0000-00-00 00:00:00', 0);
INSERT INTO `documents_proving_identity` (`id`, `guid`, `people_id`, `people_guid`, `document_type`, `series`, `number`, `date_issue_document`, `document_valid_to`, `place_issue_document`, `issuing_authority`, `creat_date`, `update_date`, `deleted`) VALUES
	(52, '', 1, '1781ce97-1d7f-11e6-8749-bcaec598f084', 21, '8010', '008054', '2010-03-15', '0000-00-00', '', '', '2016-05-24 10:34:01', '0000-00-00 00:00:00', 0);
INSERT INTO `documents_proving_identity` (`id`, `guid`, `people_id`, `people_guid`, `document_type`, `series`, `number`, `date_issue_document`, `document_valid_to`, `place_issue_document`, `issuing_authority`, `creat_date`, `update_date`, `deleted`) VALUES
	(53, '', 1, '1781ce97-1d7f-11e6-8749-bcaec598f084', 21, '8003', '329728', '2002-08-19', '0000-00-00', '', '', '2016-05-24 10:34:01', '0000-00-00 00:00:00', 0);
INSERT INTO `documents_proving_identity` (`id`, `guid`, `people_id`, `people_guid`, `document_type`, `series`, `number`, `date_issue_document`, `document_valid_to`, `place_issue_document`, `issuing_authority`, `creat_date`, `update_date`, `deleted`) VALUES
	(54, '', 1, '1781ce97-1d7f-11e6-8749-bcaec598f084', 21, '8004', '981724', '2014-06-26', '0000-00-00', '', '', '2016-05-24 10:34:01', '0000-00-00 00:00:00', 0);
INSERT INTO `documents_proving_identity` (`id`, `guid`, `people_id`, `people_guid`, `document_type`, `series`, `number`, `date_issue_document`, `document_valid_to`, `place_issue_document`, `issuing_authority`, `creat_date`, `update_date`, `deleted`) VALUES
	(55, '', 1, '1781ce97-1d7f-11e6-8749-bcaec598f084', 21, '8014', '981724', '2014-06-26', '0000-00-00', '', '', '2016-05-24 10:34:01', '0000-00-00 00:00:00', 0);
INSERT INTO `documents_proving_identity` (`id`, `guid`, `people_id`, `people_guid`, `document_type`, `series`, `number`, `date_issue_document`, `document_valid_to`, `place_issue_document`, `issuing_authority`, `creat_date`, `update_date`, `deleted`) VALUES
	(56, '', 1, '1781ce97-1d7f-11e6-8749-bcaec598f084', 21, '8004', '792931', '2004-04-05', '0000-00-00', '', '', '2016-05-24 10:34:01', '0000-00-00 00:00:00', 0);
INSERT INTO `documents_proving_identity` (`id`, `guid`, `people_id`, `people_guid`, `document_type`, `series`, `number`, `date_issue_document`, `document_valid_to`, `place_issue_document`, `issuing_authority`, `creat_date`, `update_date`, `deleted`) VALUES
	(58, '', 85, 'f988e067-91f4-4934-93a3-6fb5c40a1f27', 12, '7153', '542124', '2016-01-09', '2016-07-28', 'Wardview', 'Wilkinson, Weissnat and Kirlin', '2016-06-08 16:23:47', '0000-00-00 00:00:00', 0);
INSERT INTO `documents_proving_identity` (`id`, `guid`, `people_id`, `people_guid`, `document_type`, `series`, `number`, `date_issue_document`, `document_valid_to`, `place_issue_document`, `issuing_authority`, `creat_date`, `update_date`, `deleted`) VALUES
	(59, '', 4, '1781d1e8-1d7f-11e6-8749-bcaec598f084', 12, '8677', '665850', '2013-09-25', '2018-07-29', 'Lake Helgaton', 'Littel LLC', '2016-06-08 16:47:34', '0000-00-00 00:00:00', 0);
INSERT INTO `documents_proving_identity` (`id`, `guid`, `people_id`, `people_guid`, `document_type`, `series`, `number`, `date_issue_document`, `document_valid_to`, `place_issue_document`, `issuing_authority`, `creat_date`, `update_date`, `deleted`) VALUES
	(60, '', 22, 'c72ed338-6f57-432d-b4a7-64bf71bf3a6c', 12, '9231', '462912', '2014-09-20', '2017-08-05', 'South Thomasshire', 'Pacocha Inc and Sons', '2016-06-08 17:07:59', '0000-00-00 00:00:00', 0);
INSERT INTO `documents_proving_identity` (`id`, `guid`, `people_id`, `people_guid`, `document_type`, `series`, `number`, `date_issue_document`, `document_valid_to`, `place_issue_document`, `issuing_authority`, `creat_date`, `update_date`, `deleted`) VALUES
	(61, '', 43, '429ef111-72c5-4782-871d-2e8f303cd339', 12, '3056', '133053', '2014-07-12', '2018-03-14', 'Dejahmouth', 'Marks, Rodriguez and Bradtke', '2016-06-09 09:28:08', '0000-00-00 00:00:00', 0);
INSERT INTO `documents_proving_identity` (`id`, `guid`, `people_id`, `people_guid`, `document_type`, `series`, `number`, `date_issue_document`, `document_valid_to`, `place_issue_document`, `issuing_authority`, `creat_date`, `update_date`, `deleted`) VALUES
	(62, '', 96, 'bbc162c0-6a83-4893-9247-50297c956cdd', 12, '9134', '506821', '2016-05-03', '2017-11-02', 'Wehnermouth', 'Nikolaus-Bosco', '2016-06-09 09:31:29', '0000-00-00 00:00:00', 0);
INSERT INTO `documents_proving_identity` (`id`, `guid`, `people_id`, `people_guid`, `document_type`, `series`, `number`, `date_issue_document`, `document_valid_to`, `place_issue_document`, `issuing_authority`, `creat_date`, `update_date`, `deleted`) VALUES
	(63, '', 61, '9b898537-3adf-458d-a6b2-149da4e63ead', 12, '2675', '369456', '2014-10-02', '2018-11-26', 'Gaylordland', 'Grady Inc and Sons', '2016-06-09 09:34:16', '0000-00-00 00:00:00', 0);
INSERT INTO `documents_proving_identity` (`id`, `guid`, `people_id`, `people_guid`, `document_type`, `series`, `number`, `date_issue_document`, `document_valid_to`, `place_issue_document`, `issuing_authority`, `creat_date`, `update_date`, `deleted`) VALUES
	(64, '', 97, '8f45cc55-7a10-440f-85da-b316fa10b168', 12, '4210', '405910', '2014-01-30', '2019-01-28', 'South Ednaport', 'Sanford Group', '2016-06-09 09:34:42', '0000-00-00 00:00:00', 0);
INSERT INTO `documents_proving_identity` (`id`, `guid`, `people_id`, `people_guid`, `document_type`, `series`, `number`, `date_issue_document`, `document_valid_to`, `place_issue_document`, `issuing_authority`, `creat_date`, `update_date`, `deleted`) VALUES
	(65, '', 19, 'fab2d83d-00c7-4970-af93-7991799953fa', 12, '1644', '508594', '2016-03-21', '2016-12-24', 'Lindgrentown', 'Denesik-Volkman', '2016-06-09 09:48:20', '0000-00-00 00:00:00', 0);
INSERT INTO `documents_proving_identity` (`id`, `guid`, `people_id`, `people_guid`, `document_type`, `series`, `number`, `date_issue_document`, `document_valid_to`, `place_issue_document`, `issuing_authority`, `creat_date`, `update_date`, `deleted`) VALUES
	(66, '', 8, '1781d413-1d7f-11e6-8749-bcaec598f084', 12, '2312', '385055', '2015-05-03', '2018-10-06', 'North Ashleighmouth', 'Ledner-Labadie', '2016-06-09 10:14:49', '0000-00-00 00:00:00', 0);
INSERT INTO `documents_proving_identity` (`id`, `guid`, `people_id`, `people_guid`, `document_type`, `series`, `number`, `date_issue_document`, `document_valid_to`, `place_issue_document`, `issuing_authority`, `creat_date`, `update_date`, `deleted`) VALUES
	(67, '', 1, '1781ce97-1d7f-11e6-8749-bcaec598f084', 12, '9988', '779305', '2015-12-30', '2018-08-23', 'Brooklynborough', 'Keeling, Pfannerstill and Wisozk', '2016-06-09 10:15:29', '0000-00-00 00:00:00', 0);
INSERT INTO `documents_proving_identity` (`id`, `guid`, `people_id`, `people_guid`, `document_type`, `series`, `number`, `date_issue_document`, `document_valid_to`, `place_issue_document`, `issuing_authority`, `creat_date`, `update_date`, `deleted`) VALUES
	(68, '', 10, '1781d51c-1d7f-11e6-8749-bcaec598f084', 12, '5094', '806353', '2014-06-07', '2017-04-16', 'Ardellaburgh', 'Emmerich Group', '2016-06-09 10:16:37', '0000-00-00 00:00:00', 0);
INSERT INTO `documents_proving_identity` (`id`, `guid`, `people_id`, `people_guid`, `document_type`, `series`, `number`, `date_issue_document`, `document_valid_to`, `place_issue_document`, `issuing_authority`, `creat_date`, `update_date`, `deleted`) VALUES
	(69, '', 23, '35393dcf-2ece-4cde-9a46-5be484b8a3be', 12, '5327', '746066', '2014-12-31', '2017-06-15', 'Brandynmouth', 'Bergstrom LLC', '2016-06-09 11:06:18', '0000-00-00 00:00:00', 0);
/*!40000 ALTER TABLE `documents_proving_identity` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.educational_institutions
DROP TABLE IF EXISTS `educational_institutions`;
CREATE TABLE IF NOT EXISTS `educational_institutions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `guid` varchar(36) NOT NULL COMMENT 'Уникальный идентификатор',
  `full_name` text NOT NULL COMMENT 'Полное наименование',
  `additional_name` text COMMENT 'Дополнительное наименование (на момент реорганизации)',
  `short_name` varchar(255) NOT NULL COMMENT 'Дополнительное наименование (на момент реорганизации)',
  `shifts_count` int(11) NOT NULL COMMENT 'Количество смен',
  `inn` varchar(10) NOT NULL COMMENT 'ИНН',
  `kpp` varchar(9) DEFAULT NULL COMMENT 'КПП',
  `ogrn` varchar(15) DEFAULT NULL COMMENT 'ОГРН',
  `is_filial` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Юридическое лицо или филиал',
  `parent_guid_id` varchar(36) DEFAULT NULL COMMENT 'Головная организация',
  `legal_residence` text COMMENT 'Юридический адрес',
  `legal_residence_fials` varchar(36) NOT NULL COMMENT 'Юридический адрес по ФИАС',
  `actual_address` text COMMENT 'Фактический адрес (почтовый адрес)',
  `actual_address_fias` varchar(36) NOT NULL COMMENT 'Фактический адрес (почтовый адрес) по ФИАС',
  `oktmo` varchar(11) DEFAULT NULL COMMENT 'Код ОКТМО',
  `head_full_name` text COMMENT 'ФИО руководителя',
  `web_site` varchar(50) DEFAULT NULL COMMENT 'Веб-сайт',
  `e-mail` varchar(50) DEFAULT NULL COMMENT 'Электронная почта',
  `phone_number` varchar(50) DEFAULT NULL COMMENT 'Номер телефона',
  `status` int(11) NOT NULL COMMENT 'Статус',
  `okfs` varchar(2) DEFAULT NULL COMMENT 'Общероссийский классификатор форм собственности',
  `okved` varchar(6) DEFAULT NULL COMMENT 'Отраслевая принадлежность по ОКВЭД',
  `okopf` varchar(5) DEFAULT NULL COMMENT 'Организационно-правовая форма (код ОКОПФ)',
  `organization_type` int(11) NOT NULL COMMENT 'Тип организации, осуществляющей образовательную деятельность',
  `limit_fullness` smallint(6) DEFAULT NULL COMMENT 'Предельная наполняемость (максимальное количество детей, которое может находиться в образовательной организации в одну смену с соблюдением норм СанПин);',
  `okogu` varchar(50) DEFAULT NULL COMMENT 'Принадлежность к государственным или муниципальным органам управления (код по ОКОГУ).',
  `cretate_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Удален?',
  PRIMARY KEY (`id`),
  KEY `guid` (`guid`),
  KEY `short_name` (`short_name`),
  KEY `inn` (`inn`),
  KEY `parent_guid_id` (`parent_guid_id`),
  KEY `FK_educational_institutions_type_educational_organizations` (`organization_type`),
  KEY `FK_educational_institutions_shifts_educational_institutions` (`shifts_count`),
  KEY `FK_educational_institutions_statuses_educational_organizations` (`status`),
  CONSTRAINT `FK_educational_institutions_shifts_educational_institutions` FOREIGN KEY (`shifts_count`) REFERENCES `shifts_educational_institutions` (`id`),
  CONSTRAINT `FK_educational_institutions_statuses_educational_organizations` FOREIGN KEY (`status`) REFERENCES `statuses_educational_organizations` (`id`),
  CONSTRAINT `FK_educational_institutions_type_educational_organizations` FOREIGN KEY (`organization_type`) REFERENCES `type_educational_organizations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Информация об организации, осуществляющей образовательную деятельность';

-- Дамп данных таблицы contingent.educational_institutions: ~0 rows (приблизительно)
DELETE FROM `educational_institutions`;
/*!40000 ALTER TABLE `educational_institutions` DISABLE KEYS */;
/*!40000 ALTER TABLE `educational_institutions` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.educational_licenses
DROP TABLE IF EXISTS `educational_licenses`;
CREATE TABLE IF NOT EXISTS `educational_licenses` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Аккредитация',
  `educational_institutions_id` bigint(20) NOT NULL,
  `series` varchar(4) NOT NULL COMMENT 'Серия бланка',
  `number` varchar(6) NOT NULL COMMENT 'Номер бланка',
  `register_number` bigint(20) NOT NULL COMMENT 'Реестровый номер свидетельства о государственной аккредитации образовательной деятельности для организаций, которые должны проходить аккредитацию',
  `date_issue` datetime NOT NULL COMMENT 'Дата выдачи',
  `expiration_date` datetime NOT NULL COMMENT 'Срок действия',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted` tinyint(4) NOT NULL COMMENT 'Удален?',
  PRIMARY KEY (`id`),
  KEY `FK_license_educational_institutions` (`educational_institutions_id`),
  CONSTRAINT `FK_license_educational_institutions` FOREIGN KEY (`educational_institutions_id`) REFERENCES `educational_institutions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Лицензия на осуществление образовательной деятельности';

-- Дамп данных таблицы contingent.educational_licenses: ~0 rows (приблизительно)
DELETE FROM `educational_licenses`;
/*!40000 ALTER TABLE `educational_licenses` DISABLE KEYS */;
/*!40000 ALTER TABLE `educational_licenses` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.founders_educational_institutions
DROP TABLE IF EXISTS `founders_educational_institutions`;
CREATE TABLE IF NOT EXISTS `founders_educational_institutions` (
  `people_id` bigint(20) NOT NULL,
  `educational_institution` bigint(20) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`people_id`,`educational_institution`),
  KEY `FK_founders_educational_institutions_educational_institutions` (`educational_institution`),
  CONSTRAINT `FK_founders_educational_institutions_educational_institutions` FOREIGN KEY (`educational_institution`) REFERENCES `educational_institutions` (`id`),
  CONSTRAINT `FK_founders_educational_institutions_people` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Учредители образовательных учреждений';

-- Дамп данных таблицы contingent.founders_educational_institutions: ~0 rows (приблизительно)
DELETE FROM `founders_educational_institutions`;
/*!40000 ALTER TABLE `founders_educational_institutions` DISABLE KEYS */;
/*!40000 ALTER TABLE `founders_educational_institutions` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.people
DROP TABLE IF EXISTS `people`;
CREATE TABLE IF NOT EXISTS `people` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `guid` varchar(36) NOT NULL,
  `full_name` text NOT NULL,
  `date_birth` date NOT NULL COMMENT 'Дата рождения',
  `birthplace` text NOT NULL COMMENT 'Место рождения',
  `birthplace_fias` varchar(36) DEFAULT NULL COMMENT 'Место рождения ФИАС',
  `sex` varchar(7) NOT NULL COMMENT 'Пол',
  `snils` varchar(11) DEFAULT NULL COMMENT 'СНИЛС',
  `group_health` int(11) DEFAULT NULL,
  `sports_group` int(11) DEFAULT NULL,
  `need_adapted_training_program` tinyint(4) DEFAULT NULL COMMENT 'Наличие потребности в адаптированной программе обучения  ',
  `need_long-term_treatment` tinyint(4) DEFAULT NULL COMMENT 'Наличие потребности в длительном лечении',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `guid` (`guid`),
  KEY `FK_people_types_medical_groups_health` (`group_health`),
  KEY `FK_people_types_medical_groups_health_2` (`sports_group`),
  CONSTRAINT `FK_people_types_medical_groups_health` FOREIGN KEY (`group_health`) REFERENCES `types_medical_groups_health` (`id`),
  CONSTRAINT `FK_people_types_medical_groups_health_2` FOREIGN KEY (`sports_group`) REFERENCES `types_medical_groups_health` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8 COMMENT='Общие сведения о контингенте обучающихся';

-- Дамп данных таблицы contingent.people: ~103 rows (приблизительно)
DELETE FROM `people`;
/*!40000 ALTER TABLE `people` DISABLE KEYS */;
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(1, '1781ce97-1d7f-11e6-8749-bcaec598f084', 'Булат Хакимов Динарович', '2009-05-02', 'р.п. Чишмы Чишминский район республика Башкортостан, Россия', '30879', 'М', '154-530-747', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(2, '1781d0b1-1d7f-11e6-8749-bcaec598f084', 'Камиль Шавалиев  Айдарович', '2009-10-22', 'г. Уфа', '32918', 'М', '', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(3, '1781d15c-1d7f-11e6-8749-bcaec598f084', 'Лев Скуин Анатольевич', '2009-04-22', 'Башкортостан, г.Уфа', '30266', 'М', '', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(4, '1781d1e8-1d7f-11e6-8749-bcaec598f084', 'Алексей Синдяков Дмитриевич', '2009-02-10', 'г.Уфа', '30470', 'М', '', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(5, '1781d27c-1d7f-11e6-8749-bcaec598f084', 'Эмиль Галлямов Флюрович', '2008-12-29', 'Москва', '16542', 'М', '', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(6, '1781d2fe-1d7f-11e6-8749-bcaec598f084', 'Матвей Антипин Сергеевич', '2009-04-22', 'г.Уфа Республика Башкортостан, Россия', '20970', 'М', '190-577-566', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(7, '1781d386-1d7f-11e6-8749-bcaec598f084', 'Марат Сафиуллин Фанилевич', '2009-04-30', 'г.Уфа', '25633', 'М', '175-247-103', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(8, '1781d413-1d7f-11e6-8749-bcaec598f084', 'Давид Суфияров Даниэлевич', '2005-06-23', 'Уфа', '21606', 'М', '143-781-961', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(9, '1781d4a0-1d7f-11e6-8749-bcaec598f084', 'Богдан Суфияров Даниэлевич', '2009-03-15', 'Уфа', '21606', 'М', '', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(10, '1781d51c-1d7f-11e6-8749-bcaec598f084', 'Родион Суфияров Даниэлевич', '2014-10-21', 'Уфа', '21606', 'М', '', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(11, '90ccb770-d148-49cf-9ede-568d743a5e0d', 'Иванов Иван Иванов', '2016-05-24', 'Уфа', NULL, 'М', NULL, 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(12, 'c1a8191f-feef-4c18-9da2-86827501c95e', 'Иванов Иван Иванов', '2016-05-24', 'Уфа', NULL, 'М', NULL, 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(13, '94485ca3-9bdf-4db8-8141-f8bb38a9b71f', 'Иванов ИВан Иванович', '2016-05-24', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(14, '8ed1629f-59ed-4847-be8c-eda2a558c149', 'Иванов ИВан Иванович', '2016-05-25', 'Уфа', NULL, 'М', '0', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(15, 'd531b546-e9a3-422f-9c9a-0040e1ba6a08', 'Иванов ИВан Иванович', '2016-05-26', 'Уфа', NULL, 'М', '0', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(16, '195e2b86-2492-4394-a5df-5b6c9adcd15e', 'Иванов ИВан Иванович', '2016-05-26', 'Уфа', NULL, 'М', '0', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(17, '96cfd7a4-c91a-4475-b6c4-8b04f5f038b5', 'Иванов Иван Иванович', '2016-05-26', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(18, '8cce4c8e-a38d-4096-adcd-f14c634bbd88', 'Иванов Иван Иванович', '2016-05-26', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(19, 'fab2d83d-00c7-4970-af93-7991799953fa', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(20, '220d2bde-5054-43f7-8d49-87660289db1a', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(21, '457894a7-a832-4e42-b740-622ac9794ef5', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(22, 'c72ed338-6f57-432d-b4a7-64bf71bf3a6c', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(23, '35393dcf-2ece-4cde-9a46-5be484b8a3be', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(24, 'b2e6d335-f1a4-4b02-b831-1d0d7e177821', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(25, '8cfeb159-282e-4281-b5bb-fad85d7bcb2b', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(26, '10601f94-e103-41e3-abc0-b8e82c1d21bf', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(27, '40578444-8d43-41e9-b14a-69c445a757a6', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(28, 'd3882197-7b5f-4c7e-ad8a-4aea3235a84b', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(29, 'c504e47c-2a4c-497c-bbf4-2a82ca9b39e2', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(30, 'e0299066-ae7d-4b05-9203-06a9f74a0f50', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(31, 'a9291117-7c81-4804-b48a-b42aaa2a401a', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(32, '07d7333f-fd46-4865-9e64-2de368518e13', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(33, 'ff8af618-b33b-4ba8-a25d-f71be26a46ee', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(34, '46d3e280-bd62-4b97-a6a0-8e3d563e4b8b', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(35, 'd0558a3d-b454-4da0-b680-5b7b5bc1378b', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(36, 'b1e5ce78-e23e-4c32-bdb3-234de9db0643', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(37, 'ecb333d6-395a-4282-8693-48627addc813', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(38, '4717926f-136e-4dc3-901f-df2c6974f5b2', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(39, 'f8b6bb01-61ed-4da5-a76f-d44a4f491c5c', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(40, '7c85ed36-3b88-428e-b4fe-e9bc60c69cb5', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(41, '24e03218-c4ff-4aba-8c36-07624b5a1c9e', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(42, 'a5cdfc1c-b216-4892-ab07-11244d5ee1d1', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(43, '429ef111-72c5-4782-871d-2e8f303cd339', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(44, '07ac75a6-aea6-4e6c-a054-246e48cd8e1b', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(45, '465dc142-fad8-45ea-96d4-8c303a5d9972', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(46, 'b4aa0b97-a7d2-431c-a542-4294100ea7e9', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(47, 'c31b14f8-9dbc-48c0-a6ff-c1f974aaeba6', 'Иванов Иван Иванович', '2016-05-27', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(48, '8f0f21b8-83d5-4c83-96c1-cc474e7dd6d5', 'Иванов Иван Иванович', '2016-05-30', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(49, 'a1e30cd6-df40-49c4-837f-f30f4df6f8ab', 'Иванов Иван Иванович', '2016-05-30', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(50, 'efd5219e-7d7d-406f-be02-261a9e2ca22e', 'Иванов Иван Иванович', '2016-05-30', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(51, '6d6a27ae-9ba9-4c0e-80e4-312887b65be7', 'Иванов Иван Иванович', '2016-05-30', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(52, 'aad2e24e-56d2-4683-a9b3-f4db0e8d7d7d', 'Иванов Иван Иванович', '2016-05-30', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(53, '3fe64e93-83b2-4f25-b7c8-063072e07f60', 'Иванов Иван Иванович', '2016-05-30', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(54, 'eece3278-b704-425b-be68-e0e274444979', 'Иванов Иван Иванович', '2016-05-30', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(55, '6ba2078f-38a8-4f6e-be27-96df7a79a60c', 'Иванов Иван Иванович', '2016-05-31', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(56, '202f41a6-53c4-471a-a21b-e434b7fd8092', 'Иванов Иван Иванович', '2016-05-31', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(57, 'bdb1683e-3067-4b6f-ba47-abe0362d1277', 'Иванов Иван Иванович', '2016-06-01', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(58, '72fbc338-2c28-4f10-9ad5-4022127e8c44', 'Иванов Иван Иванович', '2016-06-01', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(59, '2e948a67-85fd-49e6-85d0-b6a5da20246c', 'Иванов Иван Иванович', '2016-06-01', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(60, '17338eef-b3fc-4664-a1d4-a23ad7115152', 'Иванов Иван Иванович', '2016-06-01', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(61, '9b898537-3adf-458d-a6b2-149da4e63ead', 'Иванов Иван Иванович', '2016-06-01', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(62, '1fb6a22d-76c0-4a41-9b7c-57a9f4e47537', 'Иванов Иван Иванович', '2016-06-01', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(63, 'b1897fc0-770b-4605-8ce5-54156683fcbb', 'Иванов Иван Иванович', '2016-06-01', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(64, '30e2a114-a640-4f15-944c-a17458f3c78f', 'Иванов Иван Иванович', '2016-06-01', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(65, '775902cb-bf63-473a-82e5-895e51c1a213', 'Иванов Иван Иванович', '2016-06-01', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(66, 'dad7d7a0-a1d2-4ce2-bfba-2fd7bd0223e6', 'Иванов Иван Иванович', '2016-06-01', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(67, '8cc80e02-4bfd-4199-88de-7dc4a7f44778', 'Иванов Иван Иванович', '2016-06-01', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(68, '8db09685-372a-45a7-81e2-7034f5a7f456', 'Иванов Иван Иванович', '2016-06-01', 'Уфа', NULL, 'М', '0', 1, 2, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(69, '98d49dca-bba2-4697-ac7a-e327a376d40f', 'Johathan Brekke', '2016-06-03', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(70, 'e8754580-804f-4272-a0ac-cc915bd409b8', 'Cristina Reinger', '2016-06-03', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(71, '6f4f5c93-aa85-4711-9b13-1df3ae65c967', 'Cristal Ferry', '2016-06-03', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(72, 'acd2c736-f7f8-43b2-9ccb-54322eddbb4b', 'Mr. Issac Pacocha', '2016-06-03', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(73, '86e6c9a7-dd48-493e-a3a1-70ff2fd1e3b8', 'Gussie Veum V', '2016-06-06', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(74, '9639614e-5ecd-46fb-8877-b7bee81f95da', 'Una Satterfield', '2016-06-06', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(75, '6952cd3f-42f9-4988-b69f-4cec7741adcf', 'Maya Klocko', '2016-06-06', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(76, '79e4fd09-c5b1-41c2-ac59-1bbad7c22b1e', 'Leonardo Sipes', '2016-06-06', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(78, 'b09e2b02-39fe-4381-8cb3-2835d0c1521b', 'Verna Dooley', '2016-06-06', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(79, 'b5364686-1d5a-47a0-b7f9-44c29851f44e', 'Ms. Hilbert Renner', '2016-06-06', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(84, 'b99ae78a-20fd-4a6f-8231-0616bf5d1b91', 'Mrs. Eleonore Walsh', '2016-06-06', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(85, 'f988e067-91f4-4934-93a3-6fb5c40a1f27', 'Josefa Hansen', '2016-06-07', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(86, '3c8794a4-4d17-49b5-8128-ed25a1021af2', 'Magali Lynch', '2016-06-07', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(87, 'ab1da5c4-a9ad-4c27-a284-84a12e6e94d9', 'Dr. Chris Rosenbaum', '2016-06-07', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(88, '98ee866f-1b7c-413a-a879-b3509a9aa0da', 'Delaney Murphy', '2016-06-07', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(89, '9af139a4-2d64-45b8-ad0a-ca16b0f9f127', 'Brycen Hagenes', '2016-06-07', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(90, 'ce0889c7-96c7-4d48-aeab-f2bbe39e3694', 'Kole Ratke', '2016-06-07', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(91, '79f11c9d-8e47-4f39-b96a-1c8e357a684e', 'Jovan Rowe', '2016-06-07', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(92, 'c73618ad-2df7-45ad-8736-791ae31fb816', 'Dr. Claud Fritsch', '2016-06-07', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(93, '389ccaed-4ffe-44f6-aa2d-9fc391dc3fd5', 'Wyatt Wilkinson', '2016-06-07', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(94, '1d93a967-8942-4915-acd2-76c9b1dcec88', 'Kyra Cormier', '2016-06-07', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(95, 'db7210b2-6ce8-471f-b80e-43cd584bc097', 'Eve Murazik', '2016-06-07', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(96, 'bbc162c0-6a83-4893-9247-50297c956cdd', 'Jessica King', '2016-06-07', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(97, '8f45cc55-7a10-440f-85da-b316fa10b168', 'Kiley Ferry', '2016-06-07', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(98, 'c4aabc8e-5dfe-44d1-b5b5-46ac9aae2bc1', 'Pablo Veum', '2016-06-07', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(99, 'c0d36ae9-2139-4abd-884c-4f85eeee2ac7', 'Josiah O\'Keefe', '2016-06-07', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(100, '94802577-7fd4-4ca7-b684-64f8361be502', 'Arnulfo Schinner', '2016-06-07', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(101, '2644e580-61f1-4575-b58d-c5e006429672', 'Ms. Alfonzo Klein', '2016-06-07', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(102, '2bc79d48-9f2c-4ecc-8520-78f4e68d0cb9', 'Zelda Christiansen', '2016-06-07', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(103, 'c2084211-dfd4-4992-bf39-161add006083', 'Carey Hermiston', '2016-06-07', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(104, '8225c697-33bd-48cd-b7b1-fbbcc19e500d', 'Calista Hodkiewicz IV', '2016-06-07', 'Уфа', NULL, 'М', '0', 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(105, 'fb4498ca-db2f-469e-bae6-a0bf6ef0723d', 'Eli Effertz PhD', '2016-06-09', 'Vivienneburgh', NULL, 'М', NULL, 11, 22, NULL, NULL, '0000-00-00 00:00:00', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(106, '0e44b21a-4e94-43f3-8d69-862f4045fd98', 'Petra Kuhic', '2003-03-04', 'Santiagostad', NULL, 'М', NULL, 11, 22, NULL, NULL, '2016-06-09 10:16:37', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(107, '5ed770b2-2ad3-4187-b701-38062d396c84', 'Veronica Trantow', '2005-05-20', 'Donaldfurt', NULL, 'М', NULL, 11, 22, NULL, NULL, '2016-06-09 11:06:18', NULL, 0);
INSERT INTO `people` (`id`, `guid`, `full_name`, `date_birth`, `birthplace`, `birthplace_fias`, `sex`, `snils`, `group_health`, `sports_group`, `need_adapted_training_program`, `need_long-term_treatment`, `create_date`, `update_date`, `deleted`) VALUES
	(108, '21988a23-e705-49d1-b13e-1e08387b6142', 'Газизовв Рамильв Ралифовичв', '2015-09-01', '', NULL, 'Мужской', '', 1, NULL, NULL, NULL, '2016-07-15 15:11:44', NULL, 0);
/*!40000 ALTER TABLE `people` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.people_countries
DROP TABLE IF EXISTS `people_countries`;
CREATE TABLE IF NOT EXISTS `people_countries` (
  `people_id` bigint(20) NOT NULL,
  `countries_id` int(11) NOT NULL,
  PRIMARY KEY (`people_id`,`countries_id`),
  KEY `FK_people_countries_countries` (`countries_id`),
  CONSTRAINT `FK_people_countries_countries` FOREIGN KEY (`countries_id`) REFERENCES `countries` (`id`),
  CONSTRAINT `FK_people_countries_people` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Гражданство';

-- Дамп данных таблицы contingent.people_countries: ~54 rows (приблизительно)
DELETE FROM `people_countries`;
/*!40000 ALTER TABLE `people_countries` DISABLE KEYS */;
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(1, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(2, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(3, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(4, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(5, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(6, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(7, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(8, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(9, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(10, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(60, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(61, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(62, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(63, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(64, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(65, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(66, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(67, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(68, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(69, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(70, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(71, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(72, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(73, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(74, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(75, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(76, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(78, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(79, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(84, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(85, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(86, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(87, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(88, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(89, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(90, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(91, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(92, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(93, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(94, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(95, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(96, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(97, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(98, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(99, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(100, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(101, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(102, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(103, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(104, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(105, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(106, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(107, 182);
INSERT INTO `people_countries` (`people_id`, `countries_id`) VALUES
	(108, 182);
/*!40000 ALTER TABLE `people_countries` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.people_ip
DROP TABLE IF EXISTS `people_ip`;
CREATE TABLE IF NOT EXISTS `people_ip` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `people_guid` varchar(36) NOT NULL,
  `ogrnip` varchar(15) NOT NULL COMMENT 'ОГРНИП',
  `okved` varchar(6) NOT NULL COMMENT 'ОКВЭД',
  `adress` text NOT NULL COMMENT 'Адрес',
  `address_fias` varchar(36) NOT NULL COMMENT 'Адрес по ФИАС',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Удален?',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Информация об индивидуальном предпринимателе, осуществляющем образовательную деятельность';

-- Дамп данных таблицы contingent.people_ip: ~0 rows (приблизительно)
DELETE FROM `people_ip`;
/*!40000 ALTER TABLE `people_ip` DISABLE KEYS */;
/*!40000 ALTER TABLE `people_ip` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.pupils_schools
DROP TABLE IF EXISTS `pupils_schools`;
CREATE TABLE IF NOT EXISTS `pupils_schools` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `educational_institutions_id` bigint(20) NOT NULL,
  `people_id` bigint(20) NOT NULL,
  `academic_year_id` int(11) NOT NULL,
  `educational_class` tinyint(4) NOT NULL DEFAULT '0',
  `transfer_date` date NOT NULL COMMENT 'Дата зачисления',
  `details_administrative_act_transfer` text COMMENT 'Реквизиты распорядительного акта о зачислении',
  `types_educational_program_id` int(11) NOT NULL COMMENT 'Уровень образовательной программы  ',
  `types_adaptedness_educational_program_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Адаптированность образовательной программы',
  `type_forms_education_id` int(11) NOT NULL DEFAULT '0',
  `shifts_educational_institution_id` int(11) DEFAULT NULL,
  `deducted_date` int(11) DEFAULT NULL COMMENT 'Когда отчислили',
  `bases_leaving_school` text NOT NULL COMMENT 'Основания для выбытия из школы  ',
  `type_document_education_id` int(11) NOT NULL COMMENT 'Тип документа об образовании',
  `requisites_document_leaving_school` text NOT NULL COMMENT 'Реквизиты документа об окончании школы',
  PRIMARY KEY (`id`),
  KEY `educational_institutions_id_people_id` (`educational_institutions_id`,`people_id`),
  KEY `FK_pupils_schools_people` (`people_id`),
  KEY `FK_pupils_schools_academic_year` (`academic_year_id`),
  KEY `FK_pupils_schools_types_educational_programs` (`types_educational_program_id`),
  KEY `FK_pupils_schools_types_adaptedness_educational_programs` (`types_adaptedness_educational_program_id`),
  KEY `FK_pupils_schools_type_forms_education` (`type_forms_education_id`),
  KEY `FK_pupils_schools_shifts_educational_institutions` (`shifts_educational_institution_id`),
  KEY `FK_pupils_schools_types_documents_education` (`type_document_education_id`),
  CONSTRAINT `FK_pupils_schools_academic_year` FOREIGN KEY (`academic_year_id`) REFERENCES `academic_year` (`id`),
  CONSTRAINT `FK_pupils_schools_educational_institutions` FOREIGN KEY (`educational_institutions_id`) REFERENCES `educational_institutions` (`id`),
  CONSTRAINT `FK_pupils_schools_people` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`),
  CONSTRAINT `FK_pupils_schools_shifts_educational_institutions` FOREIGN KEY (`shifts_educational_institution_id`) REFERENCES `shifts_educational_institutions` (`id`),
  CONSTRAINT `FK_pupils_schools_types_adaptedness_educational_programs` FOREIGN KEY (`types_adaptedness_educational_program_id`) REFERENCES `types_adaptedness_educational_programs` (`id`),
  CONSTRAINT `FK_pupils_schools_types_documents_education` FOREIGN KEY (`type_document_education_id`) REFERENCES `types_documents_education` (`id`),
  CONSTRAINT `FK_pupils_schools_types_educational_programs` FOREIGN KEY (`types_educational_program_id`) REFERENCES `types_educational_programs` (`id`),
  CONSTRAINT `FK_pupils_schools_type_forms_education` FOREIGN KEY (`type_forms_education_id`) REFERENCES `type_forms_education` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Уучащиеся школ';

-- Дамп данных таблицы contingent.pupils_schools: ~0 rows (приблизительно)
DELETE FROM `pupils_schools`;
/*!40000 ALTER TABLE `pupils_schools` DISABLE KEYS */;
/*!40000 ALTER TABLE `pupils_schools` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.realized_educational_programs
DROP TABLE IF EXISTS `realized_educational_programs`;
CREATE TABLE IF NOT EXISTS `realized_educational_programs` (
  `education_institution_id` bigint(20) NOT NULL,
  `education_type_pogram` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted` tinyint(4) NOT NULL COMMENT 'Удален?',
  PRIMARY KEY (`education_institution_id`,`education_type_pogram`),
  KEY `FK_realized_educational_programs_types_educational_programs` (`education_type_pogram`),
  CONSTRAINT `FK_realized_educational_programs_educational_institutions` FOREIGN KEY (`education_institution_id`) REFERENCES `educational_institutions` (`id`),
  CONSTRAINT `FK_realized_educational_programs_types_educational_programs` FOREIGN KEY (`education_type_pogram`) REFERENCES `types_educational_programs` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Реализуемые образовательные программы  ';

-- Дамп данных таблицы contingent.realized_educational_programs: ~0 rows (приблизительно)
DELETE FROM `realized_educational_programs`;
/*!40000 ALTER TABLE `realized_educational_programs` DISABLE KEYS */;
/*!40000 ALTER TABLE `realized_educational_programs` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.relationship_people
DROP TABLE IF EXISTS `relationship_people`;
CREATE TABLE IF NOT EXISTS `relationship_people` (
  `child_id` bigint(20) NOT NULL,
  `legal_representative_id` bigint(20) NOT NULL,
  `type_representative` int(11) NOT NULL,
  PRIMARY KEY (`child_id`,`legal_representative_id`),
  KEY `FK_relationship_people_people_2` (`legal_representative_id`),
  KEY `FK_relationship_people_types_representatives_child` (`type_representative`),
  CONSTRAINT `FK_relationship_people_people` FOREIGN KEY (`child_id`) REFERENCES `people` (`id`),
  CONSTRAINT `FK_relationship_people_people_2` FOREIGN KEY (`legal_representative_id`) REFERENCES `people` (`id`),
  CONSTRAINT `FK_relationship_people_types_representatives_child` FOREIGN KEY (`type_representative`) REFERENCES `types_representatives_child` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Родство между людьми';

-- Дамп данных таблицы contingent.relationship_people: ~0 rows (приблизительно)
DELETE FROM `relationship_people`;
/*!40000 ALTER TABLE `relationship_people` DISABLE KEYS */;
/*!40000 ALTER TABLE `relationship_people` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.school_estimates_pupil
DROP TABLE IF EXISTS `school_estimates_pupil`;
CREATE TABLE IF NOT EXISTS `school_estimates_pupil` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `people_id` bigint(20) NOT NULL DEFAULT '0',
  `school_id` bigint(20) NOT NULL DEFAULT '0',
  `type_system_estimation_id` int(11) NOT NULL DEFAULT '0',
  `assessment` varchar(50) NOT NULL DEFAULT '0',
  `type_estimate_id` int(11) NOT NULL DEFAULT '0',
  `academic_yaer_id` int(11) NOT NULL DEFAULT '0',
  `type_educational_period_id` int(11) NOT NULL DEFAULT '0',
  `subject` text NOT NULL,
  `create_date_assessment` date DEFAULT NULL,
  `actual_date_assessment` date DEFAULT NULL COMMENT 'фактическая дата выставления оценки',
  `assessment_score` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_school_estimates_pupil_people` (`people_id`),
  KEY `FK_school_estimates_pupil_educational_institutions` (`school_id`),
  KEY `FK_school_estimates_pupil_types_systems_estimation` (`type_system_estimation_id`),
  KEY `FK_school_estimates_pupil_types_estimates` (`type_estimate_id`),
  KEY `FK_school_estimates_pupil_academic_year` (`academic_yaer_id`),
  KEY `FK_school_estimates_pupil_types_educational_periods` (`type_educational_period_id`),
  CONSTRAINT `FK_school_estimates_pupil_academic_year` FOREIGN KEY (`academic_yaer_id`) REFERENCES `academic_year` (`id`),
  CONSTRAINT `FK_school_estimates_pupil_educational_institutions` FOREIGN KEY (`school_id`) REFERENCES `educational_institutions` (`id`),
  CONSTRAINT `FK_school_estimates_pupil_people` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`),
  CONSTRAINT `FK_school_estimates_pupil_types_educational_periods` FOREIGN KEY (`type_educational_period_id`) REFERENCES `types_educational_periods` (`id`),
  CONSTRAINT `FK_school_estimates_pupil_types_estimates` FOREIGN KEY (`type_estimate_id`) REFERENCES `types_estimates` (`id`),
  CONSTRAINT `FK_school_estimates_pupil_types_systems_estimation` FOREIGN KEY (`type_system_estimation_id`) REFERENCES `types_systems_estimation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Школьная успеваемость';

-- Дамп данных таблицы contingent.school_estimates_pupil: ~0 rows (приблизительно)
DELETE FROM `school_estimates_pupil`;
/*!40000 ALTER TABLE `school_estimates_pupil` DISABLE KEYS */;
/*!40000 ALTER TABLE `school_estimates_pupil` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.shifts_educational_institutions
DROP TABLE IF EXISTS `shifts_educational_institutions`;
CREATE TABLE IF NOT EXISTS `shifts_educational_institutions` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник смен';

-- Дамп данных таблицы contingent.shifts_educational_institutions: ~4 rows (приблизительно)
DELETE FROM `shifts_educational_institutions`;
/*!40000 ALTER TABLE `shifts_educational_institutions` DISABLE KEYS */;
INSERT INTO `shifts_educational_institutions` (`id`, `name`) VALUES
	(1, 'Первая смена');
INSERT INTO `shifts_educational_institutions` (`id`, `name`) VALUES
	(2, 'Вторая смена');
INSERT INTO `shifts_educational_institutions` (`id`, `name`) VALUES
	(3, 'Третья смена');
INSERT INTO `shifts_educational_institutions` (`id`, `name`) VALUES
	(4, 'Нет сменности\r\n');
/*!40000 ALTER TABLE `shifts_educational_institutions` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.statuses_educational_organizations
DROP TABLE IF EXISTS `statuses_educational_organizations`;
CREATE TABLE IF NOT EXISTS `statuses_educational_organizations` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник статусов образовательных организаций';

-- Дамп данных таблицы contingent.statuses_educational_organizations: ~12 rows (приблизительно)
DELETE FROM `statuses_educational_organizations`;
/*!40000 ALTER TABLE `statuses_educational_organizations` DISABLE KEYS */;
INSERT INTO `statuses_educational_organizations` (`id`, `name`) VALUES
	(1, 'Функционирует');
INSERT INTO `statuses_educational_organizations` (`id`, `name`) VALUES
	(2, 'Капитальный ремонт');
INSERT INTO `statuses_educational_organizations` (`id`, `name`) VALUES
	(3, 'Деятельность приостановлена');
INSERT INTO `statuses_educational_organizations` (`id`, `name`) VALUES
	(4, 'Действует, но нуждается в капитальном ремонте');
INSERT INTO `statuses_educational_organizations` (`id`, `name`) VALUES
	(5, 'Не ведет деятельность в ЭЖД');
INSERT INTO `statuses_educational_organizations` (`id`, `name`) VALUES
	(6, 'Без контингента\r\n');
INSERT INTO `statuses_educational_organizations` (`id`, `name`) VALUES
	(7, 'Реконструкция');
INSERT INTO `statuses_educational_organizations` (`id`, `name`) VALUES
	(8, 'Контингент отсуствует');
INSERT INTO `statuses_educational_organizations` (`id`, `name`) VALUES
	(9, 'Ожидает открытия');
INSERT INTO `statuses_educational_organizations` (`id`, `name`) VALUES
	(10, 'Ликвидирована');
INSERT INTO `statuses_educational_organizations` (`id`, `name`) VALUES
	(11, 'Закрыта');
INSERT INTO `statuses_educational_organizations` (`id`, `name`) VALUES
	(12, 'Присоединена к другой организации');
/*!40000 ALTER TABLE `statuses_educational_organizations` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.types_adaptedness_educational_programs
DROP TABLE IF EXISTS `types_adaptedness_educational_programs`;
CREATE TABLE IF NOT EXISTS `types_adaptedness_educational_programs` (
  `id` int(11) NOT NULL,
  `name` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Типы адаптированности образовательных программ  ';

-- Дамп данных таблицы contingent.types_adaptedness_educational_programs: ~13 rows (приблизительно)
DELETE FROM `types_adaptedness_educational_programs`;
/*!40000 ALTER TABLE `types_adaptedness_educational_programs` DISABLE KEYS */;
INSERT INTO `types_adaptedness_educational_programs` (`id`, `name`) VALUES
	(1, 'Не адаптирована');
INSERT INTO `types_adaptedness_educational_programs` (`id`, `name`) VALUES
	(2, 'Адаптирована');
INSERT INTO `types_adaptedness_educational_programs` (`id`, `name`) VALUES
	(201, 'для слабовидящих обучающихся ');
INSERT INTO `types_adaptedness_educational_programs` (`id`, `name`) VALUES
	(202, 'для слепых обучающихся');
INSERT INTO `types_adaptedness_educational_programs` (`id`, `name`) VALUES
	(203, 'для слабослышащих ');
INSERT INTO `types_adaptedness_educational_programs` (`id`, `name`) VALUES
	(204, 'для глухих ');
INSERT INTO `types_adaptedness_educational_programs` (`id`, `name`) VALUES
	(205, 'для слабослышащих обучающихся, имеющих сложную структуру дефекта (нарушение слуха и задержка психического развития)');
INSERT INTO `types_adaptedness_educational_programs` (`id`, `name`) VALUES
	(206, 'для обучающихся, имеющих нарушения опорно-двигательного аппарата');
INSERT INTO `types_adaptedness_educational_programs` (`id`, `name`) VALUES
	(207, 'для обучающихся, имеющих тяжелые нарушения речи');
INSERT INTO `types_adaptedness_educational_programs` (`id`, `name`) VALUES
	(208, 'для обучающихся с задержкой психического развития');
INSERT INTO `types_adaptedness_educational_programs` (`id`, `name`) VALUES
	(209, 'для обучающихся с умственной отсталостью');
INSERT INTO `types_adaptedness_educational_programs` (`id`, `name`) VALUES
	(210, 'для обучающихся с умственной отсталостью, имеющих сложную структуру дефекта');
INSERT INTO `types_adaptedness_educational_programs` (`id`, `name`) VALUES
	(211, 'для обучающихся с иными ограничениями здоровья\r\n');
/*!40000 ALTER TABLE `types_adaptedness_educational_programs` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.types_categories_disability
DROP TABLE IF EXISTS `types_categories_disability`;
CREATE TABLE IF NOT EXISTS `types_categories_disability` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник видов и категорий инвалидности ';

-- Дамп данных таблицы contingent.types_categories_disability: ~8 rows (приблизительно)
DELETE FROM `types_categories_disability`;
/*!40000 ALTER TABLE `types_categories_disability` DISABLE KEYS */;
INSERT INTO `types_categories_disability` (`id`, `name`) VALUES
	(1, 'Инвалидность ');
INSERT INTO `types_categories_disability` (`id`, `name`) VALUES
	(2, 'Категория инвалидности ');
INSERT INTO `types_categories_disability` (`id`, `name`) VALUES
	(11, 'Первая группа ');
INSERT INTO `types_categories_disability` (`id`, `name`) VALUES
	(12, 'Вторая группа ');
INSERT INTO `types_categories_disability` (`id`, `name`) VALUES
	(13, 'Третья группа ');
INSERT INTO `types_categories_disability` (`id`, `name`) VALUES
	(14, 'Ребенок-инвалид (для лиц до 18 лет) ');
INSERT INTO `types_categories_disability` (`id`, `name`) VALUES
	(21, 'Инвалид с детства ');
INSERT INTO `types_categories_disability` (`id`, `name`) VALUES
	(22, 'Инвалид вследствие военной травмы или заболевания, полученного в период прохождения военной службы ');
/*!40000 ALTER TABLE `types_categories_disability` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.types_documents_education
DROP TABLE IF EXISTS `types_documents_education`;
CREATE TABLE IF NOT EXISTS `types_documents_education` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Типы документов об образовании';

-- Дамп данных таблицы contingent.types_documents_education: ~20 rows (приблизительно)
DELETE FROM `types_documents_education`;
/*!40000 ALTER TABLE `types_documents_education` DISABLE KEYS */;
INSERT INTO `types_documents_education` (`id`, `name`) VALUES
	(1, 'Документы об образовании');
INSERT INTO `types_documents_education` (`id`, `name`) VALUES
	(2, 'Документ об образовании и о квалификации');
INSERT INTO `types_documents_education` (`id`, `name`) VALUES
	(3, 'Документ о квалификации');
INSERT INTO `types_documents_education` (`id`, `name`) VALUES
	(4, 'Справка об обучении');
INSERT INTO `types_documents_education` (`id`, `name`) VALUES
	(5, 'Свидетельство об освоении предпрофессиональной программы в области искусств');
INSERT INTO `types_documents_education` (`id`, `name`) VALUES
	(6, 'Другой документ об образовании');
INSERT INTO `types_documents_education` (`id`, `name`) VALUES
	(7, 'Другой документ об обучении\r\n');
INSERT INTO `types_documents_education` (`id`, `name`) VALUES
	(11, 'Аттестат о среднем общем образовании');
INSERT INTO `types_documents_education` (`id`, `name`) VALUES
	(12, 'Аттестат об основном общем образовании');
INSERT INTO `types_documents_education` (`id`, `name`) VALUES
	(21, 'Диплом о среднем профессиональном образовании ');
INSERT INTO `types_documents_education` (`id`, `name`) VALUES
	(22, 'Диплом бакалавра');
INSERT INTO `types_documents_education` (`id`, `name`) VALUES
	(23, 'Диплом специалиста');
INSERT INTO `types_documents_education` (`id`, `name`) VALUES
	(24, 'Диплом магистра');
INSERT INTO `types_documents_education` (`id`, `name`) VALUES
	(25, 'Диплом об окончании аспирантуры (адъюнктуры)');
INSERT INTO `types_documents_education` (`id`, `name`) VALUES
	(26, 'Диплом об окончании ординатуры');
INSERT INTO `types_documents_education` (`id`, `name`) VALUES
	(27, 'Диплом об окончании  ассистентуры-стажировки');
INSERT INTO `types_documents_education` (`id`, `name`) VALUES
	(31, 'Удостоверение о повышении квалификации');
INSERT INTO `types_documents_education` (`id`, `name`) VALUES
	(32, 'Диплом о профессиональной переподготовке');
INSERT INTO `types_documents_education` (`id`, `name`) VALUES
	(33, 'Свидетельство о профессии рабочего, должности служащего');
INSERT INTO `types_documents_education` (`id`, `name`) VALUES
	(41, 'Свидетельство об обучении для лиц с ограниченными возможностями здоровья (с различными формами умственной отсталости) ');
/*!40000 ALTER TABLE `types_documents_education` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.types_educational_periods
DROP TABLE IF EXISTS `types_educational_periods`;
CREATE TABLE IF NOT EXISTS `types_educational_periods` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник учебных периодов';

-- Дамп данных таблицы contingent.types_educational_periods: ~21 rows (приблизительно)
DELETE FROM `types_educational_periods`;
/*!40000 ALTER TABLE `types_educational_periods` DISABLE KEYS */;
INSERT INTO `types_educational_periods` (`id`, `name`) VALUES
	(1, 'I четверть');
INSERT INTO `types_educational_periods` (`id`, `name`) VALUES
	(2, 'II четверть');
INSERT INTO `types_educational_periods` (`id`, `name`) VALUES
	(3, 'III четверть');
INSERT INTO `types_educational_periods` (`id`, `name`) VALUES
	(4, 'IV четверть');
INSERT INTO `types_educational_periods` (`id`, `name`) VALUES
	(5, 'I триместр');
INSERT INTO `types_educational_periods` (`id`, `name`) VALUES
	(6, 'II триместр');
INSERT INTO `types_educational_periods` (`id`, `name`) VALUES
	(7, 'III триместр');
INSERT INTO `types_educational_periods` (`id`, `name`) VALUES
	(8, 'I полугодие  ');
INSERT INTO `types_educational_periods` (`id`, `name`) VALUES
	(9, 'II полугодие');
INSERT INTO `types_educational_periods` (`id`, `name`) VALUES
	(10, 'I модуль');
INSERT INTO `types_educational_periods` (`id`, `name`) VALUES
	(11, 'II модуль');
INSERT INTO `types_educational_periods` (`id`, `name`) VALUES
	(12, 'III модуль');
INSERT INTO `types_educational_periods` (`id`, `name`) VALUES
	(13, 'IV модуль');
INSERT INTO `types_educational_periods` (`id`, `name`) VALUES
	(14, 'V модуль');
INSERT INTO `types_educational_periods` (`id`, `name`) VALUES
	(15, 'VI модуль');
INSERT INTO `types_educational_periods` (`id`, `name`) VALUES
	(16, 'VII модуль');
INSERT INTO `types_educational_periods` (`id`, `name`) VALUES
	(17, 'VIII модуль');
INSERT INTO `types_educational_periods` (`id`, `name`) VALUES
	(18, 'I семестр');
INSERT INTO `types_educational_periods` (`id`, `name`) VALUES
	(19, 'II семестр');
INSERT INTO `types_educational_periods` (`id`, `name`) VALUES
	(20, 'Год');
INSERT INTO `types_educational_periods` (`id`, `name`) VALUES
	(99, 'другое (не определено)  \r\n');
/*!40000 ALTER TABLE `types_educational_periods` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.types_educational_programs
DROP TABLE IF EXISTS `types_educational_programs`;
CREATE TABLE IF NOT EXISTS `types_educational_programs` (
  `id` int(11) NOT NULL COMMENT 'Номер',
  `name` text COMMENT 'Значение',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник типов образовательных программ';

-- Дамп данных таблицы contingent.types_educational_programs: ~23 rows (приблизительно)
DELETE FROM `types_educational_programs`;
/*!40000 ALTER TABLE `types_educational_programs` DISABLE KEYS */;
INSERT INTO `types_educational_programs` (`id`, `name`) VALUES
	(1, 'Основная образовательная программа');
INSERT INTO `types_educational_programs` (`id`, `name`) VALUES
	(2, 'Основная профессиональная образовательная программа');
INSERT INTO `types_educational_programs` (`id`, `name`) VALUES
	(3, 'Основная программа профессионального обучения');
INSERT INTO `types_educational_programs` (`id`, `name`) VALUES
	(4, 'Дополнительная общеобразовательная программа');
INSERT INTO `types_educational_programs` (`id`, `name`) VALUES
	(21, 'Образовательная программа среднего профессионального образования');
INSERT INTO `types_educational_programs` (`id`, `name`) VALUES
	(22, 'Образовательная программа высшего образования');
INSERT INTO `types_educational_programs` (`id`, `name`) VALUES
	(31, 'Программа профессиональной подготовки по профессиям рабочих, должностям служащих');
INSERT INTO `types_educational_programs` (`id`, `name`) VALUES
	(32, 'Программа переподготовки рабочих, служащих, ');
INSERT INTO `types_educational_programs` (`id`, `name`) VALUES
	(33, 'Программа повышения квалификации рабочих, служащих.');
INSERT INTO `types_educational_programs` (`id`, `name`) VALUES
	(41, 'Дополнительная общеразвивающая программа ');
INSERT INTO `types_educational_programs` (`id`, `name`) VALUES
	(42, 'Дополнительная предпрофессиональная программа\r\n');
INSERT INTO `types_educational_programs` (`id`, `name`) VALUES
	(101, 'Образовательная программа дошкольного образования');
INSERT INTO `types_educational_programs` (`id`, `name`) VALUES
	(102, 'Образовательная программа начального общего образования');
INSERT INTO `types_educational_programs` (`id`, `name`) VALUES
	(103, 'Образовательная программа основного общего образования');
INSERT INTO `types_educational_programs` (`id`, `name`) VALUES
	(104, 'Образовательная программа среднего общего образования');
INSERT INTO `types_educational_programs` (`id`, `name`) VALUES
	(211, 'Программа подготовки квалифицированных рабочих, служащих');
INSERT INTO `types_educational_programs` (`id`, `name`) VALUES
	(212, 'Программа подготовки специалистов среднего звена');
INSERT INTO `types_educational_programs` (`id`, `name`) VALUES
	(221, 'Программа бакалавриата ');
INSERT INTO `types_educational_programs` (`id`, `name`) VALUES
	(222, 'Программа специалитета ');
INSERT INTO `types_educational_programs` (`id`, `name`) VALUES
	(223, 'Программа магистратуры');
INSERT INTO `types_educational_programs` (`id`, `name`) VALUES
	(224, 'Программа подготовки научно-педагогических кадров в аспирантуре (адъюнктуре)');
INSERT INTO `types_educational_programs` (`id`, `name`) VALUES
	(225, 'Программа ординатуры');
INSERT INTO `types_educational_programs` (`id`, `name`) VALUES
	(226, 'Программа ассистентуры-стажировки');
/*!40000 ALTER TABLE `types_educational_programs` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.types_estimates
DROP TABLE IF EXISTS `types_estimates`;
CREATE TABLE IF NOT EXISTS `types_estimates` (
  `id` int(11) NOT NULL,
  `name` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник типы оценок';

-- Дамп данных таблицы contingent.types_estimates: ~14 rows (приблизительно)
DELETE FROM `types_estimates`;
/*!40000 ALTER TABLE `types_estimates` DISABLE KEYS */;
INSERT INTO `types_estimates` (`id`, `name`) VALUES
	(1, 'Текущая');
INSERT INTO `types_estimates` (`id`, `name`) VALUES
	(2, 'Итоговая за период');
INSERT INTO `types_estimates` (`id`, `name`) VALUES
	(3, 'Тематическая');
INSERT INTO `types_estimates` (`id`, `name`) VALUES
	(4, 'ЕГЭ');
INSERT INTO `types_estimates` (`id`, `name`) VALUES
	(5, 'ГИА');
INSERT INTO `types_estimates` (`id`, `name`) VALUES
	(6, 'Аттестат');
INSERT INTO `types_estimates` (`id`, `name`) VALUES
	(7, 'Диплом');
INSERT INTO `types_estimates` (`id`, `name`) VALUES
	(9, 'Другое');
INSERT INTO `types_estimates` (`id`, `name`) VALUES
	(10, 'Итоговый экзамен');
INSERT INTO `types_estimates` (`id`, `name`) VALUES
	(11, 'Итоговая');
INSERT INTO `types_estimates` (`id`, `name`) VALUES
	(12, 'ГВЭ-11');
INSERT INTO `types_estimates` (`id`, `name`) VALUES
	(13, 'ОГЭ');
INSERT INTO `types_estimates` (`id`, `name`) VALUES
	(14, 'ГВЭ-9');
INSERT INTO `types_estimates` (`id`, `name`) VALUES
	(15, 'Вступительная\r\n');
/*!40000 ALTER TABLE `types_estimates` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.types_identity_documents
DROP TABLE IF EXISTS `types_identity_documents`;
CREATE TABLE IF NOT EXISTS `types_identity_documents` (
  `code` int(11) NOT NULL COMMENT 'Код',
  `name` varchar(255) NOT NULL COMMENT 'Наименование документа',
  `note` text NOT NULL COMMENT 'Примечание',
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Виды документов, удостоверяющих личность';

-- Дамп данных таблицы contingent.types_identity_documents: ~21 rows (приблизительно)
DELETE FROM `types_identity_documents`;
/*!40000 ALTER TABLE `types_identity_documents` DISABLE KEYS */;
INSERT INTO `types_identity_documents` (`code`, `name`, `note`) VALUES
	(1, 'Документы, удостоверяющие личность гражданина Российской Федерации ', '');
INSERT INTO `types_identity_documents` (`code`, `name`, `note`) VALUES
	(2, 'Для иностранных граждан и лиц без гражданства', '');
INSERT INTO `types_identity_documents` (`code`, `name`, `note`) VALUES
	(4, 'Другой документ, удостоверяющий личность', '');
INSERT INTO `types_identity_documents` (`code`, `name`, `note`) VALUES
	(11, 'Свидетельство о рождении', '');
INSERT INTO `types_identity_documents` (`code`, `name`, `note`) VALUES
	(12, 'Паспорт гражданина Российской Федерации', '');
INSERT INTO `types_identity_documents` (`code`, `name`, `note`) VALUES
	(13, 'Загранпаспорт гражданина Российской Федерации', '');
INSERT INTO `types_identity_documents` (`code`, `name`, `note`) VALUES
	(14, 'Удостоверение личности военнослужащего Российской Федерации', '');
INSERT INTO `types_identity_documents` (`code`, `name`, `note`) VALUES
	(15, 'Военный билет', '');
INSERT INTO `types_identity_documents` (`code`, `name`, `note`) VALUES
	(16, 'Временное удостоверение, выданное взамен военного билета', '');
INSERT INTO `types_identity_documents` (`code`, `name`, `note`) VALUES
	(17, 'Временное удостоверение личности гражданина Российской Федерации', '');
INSERT INTO `types_identity_documents` (`code`, `name`, `note`) VALUES
	(21, 'Паспорт иностранного гражданина', '');
INSERT INTO `types_identity_documents` (`code`, `name`, `note`) VALUES
	(22, 'Удостоверение личности лица без гражданства в Российской Федерации', '');
INSERT INTO `types_identity_documents` (`code`, `name`, `note`) VALUES
	(23, 'Удостоверение личности отдельных категорий лиц, находящихся на территории Российской Федерации, подавших заявление о признании гражданами Российской Федерации или о приеме в гражданство Российской Федерации', '');
INSERT INTO `types_identity_documents` (`code`, `name`, `note`) VALUES
	(24, 'Удостоверение личности лица, признанного беженцем', '');
INSERT INTO `types_identity_documents` (`code`, `name`, `note`) VALUES
	(25, 'Удостоверение личности лица, ходатайствующего о признании беженцем на территории Российской Федерации', '');
INSERT INTO `types_identity_documents` (`code`, `name`, `note`) VALUES
	(26, 'Удостоверение личности лица, получившего временное убежище на территории Российской Федерации', '');
INSERT INTO `types_identity_documents` (`code`, `name`, `note`) VALUES
	(27, 'Вид на жительство в Российской Федерации', '');
INSERT INTO `types_identity_documents` (`code`, `name`, `note`) VALUES
	(28, 'Разрешение на временное проживание в Российской Федерации', '');
INSERT INTO `types_identity_documents` (`code`, `name`, `note`) VALUES
	(29, 'Свидетельство о рассмотрении ходатайства о признании лица беженцем на территории Российской Федерации по существу', '');
INSERT INTO `types_identity_documents` (`code`, `name`, `note`) VALUES
	(30, 'Свидетельство о предоставлении временного убежища на территории Российской Федерации', '');
INSERT INTO `types_identity_documents` (`code`, `name`, `note`) VALUES
	(31, 'Свидетельство о рождении, выданное уполномоченным органом иностранного государства', '');
/*!40000 ALTER TABLE `types_identity_documents` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.types_medical_groups_health
DROP TABLE IF EXISTS `types_medical_groups_health`;
CREATE TABLE IF NOT EXISTS `types_medical_groups_health` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник видов медицинских групп здоровья ';

-- Дамп данных таблицы contingent.types_medical_groups_health: ~10 rows (приблизительно)
DELETE FROM `types_medical_groups_health`;
/*!40000 ALTER TABLE `types_medical_groups_health` DISABLE KEYS */;
INSERT INTO `types_medical_groups_health` (`id`, `name`) VALUES
	(1, 'Группы здоровья');
INSERT INTO `types_medical_groups_health` (`id`, `name`) VALUES
	(2, 'Физкультурная группа ');
INSERT INTO `types_medical_groups_health` (`id`, `name`) VALUES
	(11, 'Группа 1 - здоровые ');
INSERT INTO `types_medical_groups_health` (`id`, `name`) VALUES
	(12, 'Группа 2 - с незначительными отклонениями ');
INSERT INTO `types_medical_groups_health` (`id`, `name`) VALUES
	(13, 'Группа 3 - с хроническими заболеваниями и хорошим самочувствием, либо с временными отклонениями в состоянии здоровья ');
INSERT INTO `types_medical_groups_health` (`id`, `name`) VALUES
	(14, 'Группа 4 - с хроническими заболеваниями и плохим самочувствием ');
INSERT INTO `types_medical_groups_health` (`id`, `name`) VALUES
	(15, 'Группа 5 - с хроническими заболеваниями и наблюдаются в специальных лечебницах ');
INSERT INTO `types_medical_groups_health` (`id`, `name`) VALUES
	(21, 'Основная ');
INSERT INTO `types_medical_groups_health` (`id`, `name`) VALUES
	(22, 'Подготовительная ');
INSERT INTO `types_medical_groups_health` (`id`, `name`) VALUES
	(23, 'Специальная ');
/*!40000 ALTER TABLE `types_medical_groups_health` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.types_representatives_child
DROP TABLE IF EXISTS `types_representatives_child`;
CREATE TABLE IF NOT EXISTS `types_representatives_child` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник типов законных представителей ребенка';

-- Дамп данных таблицы contingent.types_representatives_child: ~6 rows (приблизительно)
DELETE FROM `types_representatives_child`;
/*!40000 ALTER TABLE `types_representatives_child` DISABLE KEYS */;
INSERT INTO `types_representatives_child` (`id`, `name`) VALUES
	(1, 'Родитель ');
INSERT INTO `types_representatives_child` (`id`, `name`) VALUES
	(2, 'Опекун ');
INSERT INTO `types_representatives_child` (`id`, `name`) VALUES
	(3, 'Попечитель ');
INSERT INTO `types_representatives_child` (`id`, `name`) VALUES
	(4, 'Орган опеки и попечительства ');
INSERT INTO `types_representatives_child` (`id`, `name`) VALUES
	(5, 'Приемный родитель ');
INSERT INTO `types_representatives_child` (`id`, `name`) VALUES
	(6, 'Руководитель воспитательного, лечебного и иного учреждения, в котором ребенок находится на полном государственном обеспечении  ');
/*!40000 ALTER TABLE `types_representatives_child` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.types_systems_estimation
DROP TABLE IF EXISTS `types_systems_estimation`;
CREATE TABLE IF NOT EXISTS `types_systems_estimation` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `possible_values` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник «Системы оценивания»';

-- Дамп данных таблицы contingent.types_systems_estimation: ~9 rows (приблизительно)
DELETE FROM `types_systems_estimation`;
/*!40000 ALTER TABLE `types_systems_estimation` DISABLE KEYS */;
INSERT INTO `types_systems_estimation` (`id`, `name`, `possible_values`) VALUES
	(1, '5-балльная', '5-балльная');
INSERT INTO `types_systems_estimation` (`id`, `name`, `possible_values`) VALUES
	(2, '6-балльная', '6-балльная');
INSERT INTO `types_systems_estimation` (`id`, `name`, `possible_values`) VALUES
	(3, '7-балльная', '7-балльная');
INSERT INTO `types_systems_estimation` (`id`, `name`, `possible_values`) VALUES
	(4, '10-балльная', '10-балльная');
INSERT INTO `types_systems_estimation` (`id`, `name`, `possible_values`) VALUES
	(5, '12-балльная', '12-балльная');
INSERT INTO `types_systems_estimation` (`id`, `name`, `possible_values`) VALUES
	(6, '100-балльная', '100-балльная');
INSERT INTO `types_systems_estimation` (`id`, `name`, `possible_values`) VALUES
	(7, 'Американская', 'Американская ');
INSERT INTO `types_systems_estimation` (`id`, `name`, `possible_values`) VALUES
	(8, 'Словесная', 'Словесная');
INSERT INTO `types_systems_estimation` (`id`, `name`, `possible_values`) VALUES
	(9, 'Безотметочная', 'Безотметочная\r\n');
/*!40000 ALTER TABLE `types_systems_estimation` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.type_educational_organizations
DROP TABLE IF EXISTS `type_educational_organizations`;
CREATE TABLE IF NOT EXISTS `type_educational_organizations` (
  `id` int(11) NOT NULL,
  `name` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник организаций образования субъекта РФ\r\n\r\nТип организации, осуществляющей образовательную деятельность   ';

-- Дамп данных таблицы contingent.type_educational_organizations: ~31 rows (приблизительно)
DELETE FROM `type_educational_organizations`;
/*!40000 ALTER TABLE `type_educational_organizations` DISABLE KEYS */;
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(1, 'Образовательные организации');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(2, 'Организации, осуществляющие обучение');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(3, 'Индивидуальный предприниматель, осуществляющий образовательную деятельность\r\n');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(11, 'Дошкольная образовательная организация.');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(12, 'Общеобразовательная организация ');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(13, 'Общеобразовательная организация, осуществляющая образовательную деятельность по адаптированным основным общеобразовательным программам ');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(14, 'Профессиональная образовательная организация.');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(15, 'Образовательная организация высшего образования');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(16, 'Организация дополнительного образования.');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(17, 'Организация дополнительного профессионального образования.');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(21, 'Научная организация');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(22, 'Центр психолого-педагогической, медицинской и социальной помощи');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(23, 'Организация для детей-сирот и детей, оставшихся без попечения родителей');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(24, 'Организация, осуществляющая лечение, оздоровление и (или) отдых');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(25, 'Организация, осуществляющая социальное обслуживание');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(26, 'Дипломатическое представительство Российской Федерации');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(27, 'Консульское учреждение Российской Федерации');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(28, 'Представительство Российской Федерации при международных (межгосударственных, межправительственных) организациях');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(29, 'Иные юридические лица');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(1301, 'для глухих');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(1302, 'для слабослышащих');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(1303, 'для позднооглохших');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(1304, 'для слепых');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(1305, 'для слабовидящих');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(1306, 'с тяжелыми нарушениями речи');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(1307, 'с нарушениями опорно-двигательного аппарата');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(1308, 'с задержкой психического развития');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(1309, 'с умственной отсталостью');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(1310, 'с расстройствами аутистического спектра');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(1311, 'со сложными дефектами');
INSERT INTO `type_educational_organizations` (`id`, `name`) VALUES
	(1312, 'для других обучающихся с ограниченными возможностями здоровья');
/*!40000 ALTER TABLE `type_educational_organizations` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.type_education_levels
DROP TABLE IF EXISTS `type_education_levels`;
CREATE TABLE IF NOT EXISTS `type_education_levels` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник уровней образования';

-- Дамп данных таблицы contingent.type_education_levels: ~13 rows (приблизительно)
DELETE FROM `type_education_levels`;
/*!40000 ALTER TABLE `type_education_levels` DISABLE KEYS */;
INSERT INTO `type_education_levels` (`id`, `name`) VALUES
	(1, 'Дошкольное образование');
INSERT INTO `type_education_levels` (`id`, `name`) VALUES
	(2, 'Начальное общее образование');
INSERT INTO `type_education_levels` (`id`, `name`) VALUES
	(3, 'Основное общее образование');
INSERT INTO `type_education_levels` (`id`, `name`) VALUES
	(4, 'Среднее общее образование');
INSERT INTO `type_education_levels` (`id`, `name`) VALUES
	(5, 'Среднее профессиональное образование');
INSERT INTO `type_education_levels` (`id`, `name`) VALUES
	(6, 'Высшее образование - бакалавриат');
INSERT INTO `type_education_levels` (`id`, `name`) VALUES
	(7, 'Высшее образование - специалитет ');
INSERT INTO `type_education_levels` (`id`, `name`) VALUES
	(8, 'Высшее образование - магистратура ');
INSERT INTO `type_education_levels` (`id`, `name`) VALUES
	(9, 'Высшее образование - подготовка кадров высшей квалификации - аспирантура (адъюнктура) ');
INSERT INTO `type_education_levels` (`id`, `name`) VALUES
	(10, 'Высшее образование - подготовка кадров высшей квалификации – ординатура');
INSERT INTO `type_education_levels` (`id`, `name`) VALUES
	(11, 'Высшее образование - подготовка кадров высшей квалификации - ассистентура-стажировка');
INSERT INTO `type_education_levels` (`id`, `name`) VALUES
	(12, 'Дополнительное образование детей и взрослых ');
INSERT INTO `type_education_levels` (`id`, `name`) VALUES
	(13, 'Профессиональное обучение\r\n');
/*!40000 ALTER TABLE `type_education_levels` ENABLE KEYS */;


-- Дамп структуры для таблица contingent.type_forms_education
DROP TABLE IF EXISTS `type_forms_education`;
CREATE TABLE IF NOT EXISTS `type_forms_education` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник форм получения образования';

-- Дамп данных таблицы contingent.type_forms_education: ~7 rows (приблизительно)
DELETE FROM `type_forms_education`;
/*!40000 ALTER TABLE `type_forms_education` DISABLE KEYS */;
INSERT INTO `type_forms_education` (`id`, `name`) VALUES
	(1, 'В организации, осуществляющей образовательную деятельность');
INSERT INTO `type_forms_education` (`id`, `name`) VALUES
	(2, 'Вне организации, осуществляющей образовательную деятельность');
INSERT INTO `type_forms_education` (`id`, `name`) VALUES
	(11, 'Очно');
INSERT INTO `type_forms_education` (`id`, `name`) VALUES
	(12, 'очно-заочно');
INSERT INTO `type_forms_education` (`id`, `name`) VALUES
	(13, 'Заочно');
INSERT INTO `type_forms_education` (`id`, `name`) VALUES
	(21, 'семейное образование');
INSERT INTO `type_forms_education` (`id`, `name`) VALUES
	(22, 'самообразование\r\n');
/*!40000 ALTER TABLE `type_forms_education` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
