CREATE DATABASE  IF NOT EXISTS `fias` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `fias`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: fias
-- ------------------------------------------------------
-- Server version	5.7.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `del_address_objects`
--

DROP TABLE IF EXISTS `del_address_objects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `del_address_objects` (
  `aoguid` varchar(36) NOT NULL COMMENT 'Глобальный уникальный идентификатор адресного объекта',
  `formalname` varchar(120) NOT NULL COMMENT 'Формализованное наименование',
  `regioncode` varchar(2) NOT NULL COMMENT 'Код региона',
  `autocode` varchar(1) NOT NULL COMMENT 'Код автономии',
  `areacode` varchar(3) NOT NULL COMMENT 'Код района',
  `citycode` varchar(3) NOT NULL COMMENT 'Код города',
  `ctarcode` varchar(3) NOT NULL COMMENT 'Код внутригородского района',
  `placecode` varchar(3) NOT NULL COMMENT 'Код населенного пункта',
  `streetcode` varchar(4) DEFAULT NULL COMMENT 'Код улицы',
  `extrcode` varchar(4) NOT NULL COMMENT 'Код дополнительного адресообразующего элемента',
  `sextcode` varchar(3) NOT NULL COMMENT 'Код подчиненного дополнительного адресообразующего элемента',
  `offname` varchar(120) DEFAULT NULL COMMENT 'Официальное наименование',
  `postalcode` varchar(6) DEFAULT NULL COMMENT 'Почтовый индекс',
  `ifnsfl` varchar(4) DEFAULT NULL COMMENT 'Код ИФНС ФЛ',
  `terrifnsfl` varchar(4) DEFAULT NULL COMMENT 'Код территориального участка ИФНС ФЛ',
  `ifnsul` varchar(4) DEFAULT NULL COMMENT 'Код ИФНС ЮЛ',
  `terrifnsul` varchar(4) DEFAULT NULL COMMENT 'Код территориального участка ИФНС ЮЛ',
  `okato` varchar(11) DEFAULT NULL COMMENT 'OKATO',
  `oktmo` varchar(11) DEFAULT NULL COMMENT 'OKTMO',
  `updatedate` date NOT NULL COMMENT 'Дата внесения записи',
  `shortname` varchar(10) NOT NULL COMMENT 'Краткое наименование типа Краткое наименование типа объекта',
  `aolevel` int(10) NOT NULL COMMENT 'Уровень адресного объекта',
  `parentguid` varchar(36) DEFAULT NULL COMMENT 'Идентификатор объекта родительского объекта',
  `aoid` varchar(36) NOT NULL COMMENT 'Уникальный идентификатор записи. Ключевое поле.',
  `previd` varchar(36) DEFAULT NULL COMMENT 'Идентификатор записи связывания с предыдушей исторической записью',
  `nextid` varchar(36) DEFAULT NULL COMMENT 'Идентификатор записи  связывания с последующей исторической записью',
  `code` varchar(17) DEFAULT NULL COMMENT 'Код адресного объекта одной строкой с признаком актуальности из КЛАДР 4.0.',
  `plaincode` varchar(15) DEFAULT NULL COMMENT 'Код адресного объекта из КЛАДР 4.0 одной строкой без признака актуальности (последних двух цифр)',
  `actstatus` int(10) NOT NULL COMMENT 'Статус актуальности адресного объекта ФИАС. Актуальный адрес на текущую дату. Обычно последняя запись об адресном объекте.',
  `centstatus` int(10) NOT NULL COMMENT 'Статус центра',
  `operstatus` int(10) NOT NULL COMMENT 'Статус действия над записью – причина появления записи (см. описание таблицы OperationStatus):',
  `currstatus` int(10) NOT NULL COMMENT 'Статус актуальности КЛАДР 4 (последние две цифры в коде)',
  `startdate` date NOT NULL COMMENT 'Начало действия записи',
  `enddate` date NOT NULL COMMENT 'Окончание действия записи',
  `normdoc` varchar(36) DEFAULT NULL COMMENT 'Внешний ключ на нормативный документ',
  `livestatus` binary(1) DEFAULT NULL COMMENT 'Признак действующего адресного объекта',
  PRIMARY KEY (`aoid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Классификатор адресообразующих элементов';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-04  7:55:28
