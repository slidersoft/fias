-- --------------------------------------------------------
-- Хост:                         10.3.27.7
-- Версия сервера:               5.5.49-0+deb8u1 - (Debian)
-- Операционная система:         debian-linux-gnu
-- HeidiSQL Версия:              9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица fias.actual_status
CREATE TABLE IF NOT EXISTS `actual_status` (
  `actstatid` int(11) NOT NULL COMMENT 'Идентификатор статуса (ключ)',
  `name` varchar(255) DEFAULT NULL COMMENT 'Наименование',
  PRIMARY KEY (`actstatid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Статус актуальности ФИАС';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица fias.address_objects
CREATE TABLE IF NOT EXISTS `address_objects` (
  `aoguid` varchar(36) NOT NULL COMMENT 'Глобальный уникальный идентификатор адресного объекта',
  `formalname` varchar(120) NOT NULL COMMENT 'Формализованное наименование',
  `regioncode` varchar(2) NOT NULL COMMENT 'Код региона',
  `autocode` varchar(1) NOT NULL COMMENT 'Код автономии',
  `areacode` varchar(3) NOT NULL COMMENT 'Код района',
  `citycode` varchar(3) NOT NULL COMMENT 'Код города',
  `ctarcode` varchar(3) NOT NULL COMMENT 'Код внутригородского района',
  `placecode` varchar(3) NOT NULL COMMENT 'Код населенного пункта',
  `streetcode` varchar(4) DEFAULT NULL COMMENT 'Код улицы',
  `extrcode` varchar(4) NOT NULL COMMENT 'Код дополнительного адресообразующего элемента',
  `sextcode` varchar(3) NOT NULL COMMENT 'Код подчиненного дополнительного адресообразующего элемента',
  `offname` varchar(120) DEFAULT NULL COMMENT 'Официальное наименование',
  `postalcode` varchar(6) DEFAULT NULL COMMENT 'Почтовый индекс',
  `ifnsfl` varchar(4) DEFAULT NULL COMMENT 'Код ИФНС ФЛ',
  `terrifnsfl` varchar(4) DEFAULT NULL COMMENT 'Код территориального участка ИФНС ФЛ',
  `ifnsul` varchar(4) DEFAULT NULL COMMENT 'Код ИФНС ЮЛ',
  `terrifnsul` varchar(4) DEFAULT NULL COMMENT 'Код территориального участка ИФНС ЮЛ',
  `okato` varchar(11) DEFAULT NULL COMMENT 'OKATO',
  `oktmo` varchar(11) DEFAULT NULL COMMENT 'OKTMO',
  `updatedate` datetime NOT NULL COMMENT 'Дата внесения записи',
  `shortname` varchar(10) NOT NULL COMMENT 'Краткое наименование типа Краткое наименование типа объекта',
  `aolevel` int(10) NOT NULL COMMENT 'Уровень адресного объекта',
  `parentguid` varchar(36) DEFAULT NULL COMMENT 'Идентификатор объекта родительского объекта',
  `aoid` varchar(36) NOT NULL COMMENT 'Уникальный идентификатор записи. Ключевое поле.',
  `previd` varchar(36) DEFAULT NULL COMMENT 'Идентификатор записи связывания с предыдушей исторической записью',
  `nextid` varchar(36) DEFAULT NULL COMMENT 'Идентификатор записи  связывания с последующей исторической записью',
  `code` varchar(17) DEFAULT NULL COMMENT 'Код адресного объекта одной строкой с признаком актуальности из КЛАДР 4.0.',
  `plaincode` varchar(15) DEFAULT NULL COMMENT 'Код адресного объекта из КЛАДР 4.0 одной строкой без признака актуальности (последних двух цифр)',
  `actstatus` int(10) NOT NULL COMMENT 'Статус актуальности адресного объекта ФИАС. Актуальный адрес на текущую дату. Обычно последняя запись об адресном объекте.',
  `centstatus` int(10) NOT NULL COMMENT 'Статус центра',
  `operstatus` int(10) NOT NULL COMMENT 'Статус действия над записью – причина появления записи (см. описание таблицы OperationStatus):',
  `currstatus` int(10) NOT NULL COMMENT 'Статус актуальности КЛАДР 4 (последние две цифры в коде)',
  `startdate` datetime NOT NULL COMMENT 'Начало действия записи',
  `enddate` datetime NOT NULL COMMENT 'Окончание действия записи',
  `normdoc` varchar(36) DEFAULT NULL COMMENT 'Внешний ключ на нормативный документ',
  `livestatus` tinyint(4) NOT NULL,
  PRIMARY KEY (`aoid`),
  KEY `aoguid` (`aoguid`),
  KEY `livestatus` (`livestatus`),
  KEY `formalname` (`formalname`),
  KEY `aolevel` (`aolevel`),
  KEY `shortname` (`shortname`),
  KEY `get_object` (`parentguid`,`formalname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Классификатор адресообразующих элементов';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица fias.address_object_type
CREATE TABLE IF NOT EXISTS `address_object_type` (
  `level` int(10) NOT NULL COMMENT 'Уровень адресного объекта',
  `scname` varchar(10) DEFAULT NULL COMMENT 'Краткое наименование типа объекта',
  `socrname` varchar(50) NOT NULL COMMENT 'Полное наименование типа объекта',
  `kod_t_st` varchar(4) NOT NULL COMMENT 'Ключевое поле'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Тип адресного объекта';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица fias.center_status
CREATE TABLE IF NOT EXISTS `center_status` (
  `centerstid` int(10) NOT NULL COMMENT 'Идентификатор статуса',
  `name` varchar(100) NOT NULL COMMENT 'Наименование'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Статус центра';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица fias.current_status
CREATE TABLE IF NOT EXISTS `current_status` (
  `curentstid` int(10) NOT NULL COMMENT 'Идентификатор статуса (ключ)',
  `name` varchar(100) NOT NULL COMMENT 'Наименование (0 - актуальный, 1-50, 2-98 – исторический (кроме 51), 51 - переподчиненный, 99 - несуществующий)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Статус актуальности КЛАДР 4.0';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица fias.del_address_objects
CREATE TABLE IF NOT EXISTS `del_address_objects` (
  `aoguid` varchar(36) NOT NULL COMMENT 'Глобальный уникальный идентификатор адресного объекта',
  `formalname` varchar(120) NOT NULL COMMENT 'Формализованное наименование',
  `regioncode` varchar(2) NOT NULL COMMENT 'Код региона',
  `autocode` varchar(1) NOT NULL COMMENT 'Код автономии',
  `areacode` varchar(3) NOT NULL COMMENT 'Код района',
  `citycode` varchar(3) NOT NULL COMMENT 'Код города',
  `ctarcode` varchar(3) NOT NULL COMMENT 'Код внутригородского района',
  `placecode` varchar(3) NOT NULL COMMENT 'Код населенного пункта',
  `streetcode` varchar(4) DEFAULT NULL COMMENT 'Код улицы',
  `extrcode` varchar(4) NOT NULL COMMENT 'Код дополнительного адресообразующего элемента',
  `sextcode` varchar(3) NOT NULL COMMENT 'Код подчиненного дополнительного адресообразующего элемента',
  `offname` varchar(120) DEFAULT NULL COMMENT 'Официальное наименование',
  `postalcode` varchar(6) DEFAULT NULL COMMENT 'Почтовый индекс',
  `ifnsfl` varchar(4) DEFAULT NULL COMMENT 'Код ИФНС ФЛ',
  `terrifnsfl` varchar(4) DEFAULT NULL COMMENT 'Код территориального участка ИФНС ФЛ',
  `ifnsul` varchar(4) DEFAULT NULL COMMENT 'Код ИФНС ЮЛ',
  `terrifnsul` varchar(4) DEFAULT NULL COMMENT 'Код территориального участка ИФНС ЮЛ',
  `okato` varchar(11) DEFAULT NULL COMMENT 'OKATO',
  `oktmo` varchar(11) DEFAULT NULL COMMENT 'OKTMO',
  `updatedate` datetime NOT NULL COMMENT 'Дата внесения записи',
  `shortname` varchar(10) NOT NULL COMMENT 'Краткое наименование типа Краткое наименование типа объекта',
  `aolevel` int(10) NOT NULL COMMENT 'Уровень адресного объекта',
  `parentguid` varchar(36) DEFAULT NULL COMMENT 'Идентификатор объекта родительского объекта',
  `aoid` varchar(36) NOT NULL COMMENT 'Уникальный идентификатор записи. Ключевое поле.',
  `previd` varchar(36) DEFAULT NULL COMMENT 'Идентификатор записи связывания с предыдушей исторической записью',
  `nextid` varchar(36) DEFAULT NULL COMMENT 'Идентификатор записи  связывания с последующей исторической записью',
  `code` varchar(17) DEFAULT NULL COMMENT 'Код адресного объекта одной строкой с признаком актуальности из КЛАДР 4.0.',
  `plaincode` varchar(15) DEFAULT NULL COMMENT 'Код адресного объекта из КЛАДР 4.0 одной строкой без признака актуальности (последних двух цифр)',
  `actstatus` int(10) NOT NULL COMMENT 'Статус актуальности адресного объекта ФИАС. Актуальный адрес на текущую дату. Обычно последняя запись об адресном объекте.',
  `centstatus` int(10) NOT NULL COMMENT 'Статус центра',
  `operstatus` int(10) NOT NULL COMMENT 'Статус действия над записью – причина появления записи (см. описание таблицы OperationStatus):',
  `currstatus` int(10) NOT NULL COMMENT 'Статус актуальности КЛАДР 4 (последние две цифры в коде)',
  `startdate` datetime NOT NULL COMMENT 'Начало действия записи',
  `enddate` datetime NOT NULL COMMENT 'Окончание действия записи',
  `normdoc` varchar(36) DEFAULT NULL COMMENT 'Внешний ключ на нормативный документ',
  `livestatus` bit(1) NOT NULL COMMENT 'Признак действующего адресного объекта',
  PRIMARY KEY (`aoid`),
  KEY `aoguid` (`aoguid`),
  KEY `livestatus` (`livestatus`),
  KEY `formalname` (`formalname`),
  KEY `aolevel` (`aolevel`),
  KEY `shortname` (`shortname`),
  KEY `get_object` (`parentguid`,`formalname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Классификатор адресообразующих элементов';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица fias.del_house
CREATE TABLE IF NOT EXISTS `del_house` (
  `postalcode` varchar(6) DEFAULT NULL COMMENT 'Почтовый индекс',
  `ifnsfl` varchar(4) DEFAULT NULL COMMENT 'Код ИФНС ФЛ',
  `terrifnsfl` varchar(4) DEFAULT NULL COMMENT 'Код территориального участка ИФНС ФЛ',
  `ifnsul` varchar(4) DEFAULT NULL COMMENT 'Код ИФНС ЮЛ',
  `terrifnsul` varchar(4) DEFAULT NULL COMMENT 'Код территориального участка ИФНС ЮЛ',
  `okato` varchar(11) DEFAULT NULL COMMENT 'OKATO',
  `oktmo` varchar(11) DEFAULT NULL COMMENT 'OKTMO',
  `updatedate` date NOT NULL COMMENT 'Дата время внесения записи',
  `housenum` varchar(20) NOT NULL COMMENT 'Номер дома',
  `eststatus` int(1) DEFAULT NULL COMMENT 'Признак владения',
  `buildnum` varchar(10) NOT NULL COMMENT 'Номер корпуса',
  `strucnum` varchar(10) NOT NULL COMMENT 'Номер строения',
  `strstatus` varchar(10) NOT NULL COMMENT 'Признак строения',
  `houseid` varchar(36) NOT NULL COMMENT 'Уникальный идентификатор записи дома',
  `houseguid` varchar(36) NOT NULL COMMENT 'Глобальный уникальный идентификатор дома',
  `aoguid` varchar(36) DEFAULT NULL COMMENT 'Guid записи родительского объекта (улицы, города, населенного пункта и т.п.)',
  `startdate` date DEFAULT NULL COMMENT 'Начало действия записи',
  `enddate` date DEFAULT NULL COMMENT 'Окончание действия записи',
  `statstatus` int(10) DEFAULT NULL COMMENT 'Состояние дома',
  `normdoc` varchar(36) NOT NULL COMMENT 'Внешний ключ на нормативный документ',
  `counter` int(10) DEFAULT NULL COMMENT 'Счетчик записей домов для КЛАДР 4',
  PRIMARY KEY (`houseid`),
  KEY `aoguid` (`aoguid`),
  KEY `houseguid` (`houseguid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Сведения по номерам домов улиц городов и населенных пунктов, номера земельных участков и т.п';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица fias.del_house_interval
CREATE TABLE IF NOT EXISTS `del_house_interval` (
  `postalcode` varchar(6) DEFAULT NULL COMMENT 'Почтовый индекс',
  `ifnsfl` varchar(4) DEFAULT NULL COMMENT 'Код ИФНС ФЛ',
  `terrifnsfl` varchar(4) DEFAULT NULL COMMENT 'Код территориального участка ИФНС ФЛ',
  `ifnsul` varchar(4) DEFAULT NULL COMMENT 'Код ИФНС ЮЛ',
  `terrifnsul` varchar(4) DEFAULT NULL COMMENT 'Код территориального участка ИФНС ЮЛ',
  `okato` varchar(11) DEFAULT NULL COMMENT 'OKATO',
  `oktmo` varchar(11) DEFAULT NULL COMMENT 'OKTMO',
  `updatedate` date NOT NULL COMMENT 'Дата  внесения записи',
  `intstart` int(10) NOT NULL COMMENT 'Значение начала интервала',
  `intend` int(10) NOT NULL COMMENT 'Значение окончания интервала',
  `houseintid` varchar(36) NOT NULL COMMENT 'Идентификатор записи интервала домов',
  `intguid` varchar(36) NOT NULL COMMENT 'Глобальный уникальный идентификатор интервала домов',
  `aoguid` varchar(36) NOT NULL COMMENT 'Идентификатор объекта родительского объекта (улицы, города, населенного пункта и т.п.)',
  `startdate` date NOT NULL COMMENT 'Начало действия записи',
  `enddate` date NOT NULL COMMENT 'Окончание действия записи',
  `intstatus` int(10) NOT NULL COMMENT 'Статус интервала (обычный, четный, нечетный)',
  `normdoc` varchar(36) DEFAULT NULL COMMENT 'Внешний ключ на нормативный документ',
  `counter` int(10) DEFAULT NULL COMMENT 'Счетчик записей домов для КЛАДР 4'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Интервалы домов';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица fias.del_normative_document
CREATE TABLE IF NOT EXISTS `del_normative_document` (
  `normdocid` varchar(36) NOT NULL COMMENT 'Идентификатор нормативного документа',
  `docname` text COMMENT 'Наименование документа',
  `docdate` date NOT NULL COMMENT 'Дата документа',
  `docnum` varchar(20) NOT NULL COMMENT 'Номер документа',
  `doctype` int(10) NOT NULL COMMENT 'Тип документа',
  `docimgid` int(10) NOT NULL COMMENT 'Идентификатор образа (внешний ключ)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Сведения по нормативному документу, являющемуся основанием присвоения адресному элементу наименования';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица fias.estate_status
CREATE TABLE IF NOT EXISTS `estate_status` (
  `eststatid` int(10) NOT NULL COMMENT 'Признак владения',
  `name` varchar(20) NOT NULL COMMENT 'Наименование',
  `shortname` varchar(20) DEFAULT NULL COMMENT 'Краткое наименование'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Признак владения';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица fias.house
CREATE TABLE IF NOT EXISTS `house` (
  `postalcode` varchar(6) DEFAULT NULL COMMENT 'Почтовый индекс',
  `ifnsfl` varchar(4) DEFAULT NULL COMMENT 'Код ИФНС ФЛ',
  `terrifnsfl` varchar(4) DEFAULT NULL COMMENT 'Код территориального участка ИФНС ФЛ',
  `ifnsul` varchar(4) DEFAULT NULL COMMENT 'Код ИФНС ЮЛ',
  `terrifnsul` varchar(4) DEFAULT NULL COMMENT 'Код территориального участка ИФНС ЮЛ',
  `okato` varchar(11) DEFAULT NULL COMMENT 'OKATO',
  `oktmo` varchar(11) DEFAULT NULL COMMENT 'OKTMO',
  `updatedate` date NOT NULL COMMENT 'Дата время внесения записи',
  `housenum` varchar(20) DEFAULT NULL COMMENT 'Номер дома',
  `eststatus` int(1) NOT NULL COMMENT 'Признак владения',
  `buildnum` varchar(10) DEFAULT NULL COMMENT 'Номер корпуса',
  `strucnum` varchar(10) DEFAULT NULL COMMENT 'Номер строения',
  `strstatus` varchar(10) DEFAULT NULL COMMENT 'Признак строения',
  `houseid` varchar(36) NOT NULL COMMENT 'Уникальный идентификатор записи дома',
  `houseguid` varchar(36) NOT NULL COMMENT 'Глобальный уникальный идентификатор дома',
  `aoguid` varchar(36) NOT NULL COMMENT 'Guid записи родительского объекта (улицы, города, населенного пункта и т.п.)',
  `startdate` date NOT NULL COMMENT 'Начало действия записи',
  `enddate` date NOT NULL COMMENT 'Окончание действия записи',
  `statstatus` int(10) NOT NULL COMMENT 'Состояние дома',
  `normdoc` varchar(36) DEFAULT NULL COMMENT 'Внешний ключ на нормативный документ',
  `counter` int(10) NOT NULL COMMENT 'Счетчик записей домов для КЛАДР 4',
  PRIMARY KEY (`houseid`),
  KEY `aoguid` (`aoguid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Сведения по номерам домов улиц городов и населенных пунктов, номера земельных участков и т.п';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица fias.house_interval
CREATE TABLE IF NOT EXISTS `house_interval` (
  `postalcode` varchar(6) DEFAULT NULL COMMENT 'Почтовый индекс',
  `ifnsfl` varchar(4) DEFAULT NULL COMMENT 'Код ИФНС ФЛ',
  `terrifnsfl` varchar(4) DEFAULT NULL COMMENT 'Код территориального участка ИФНС ФЛ',
  `ifnsul` varchar(4) DEFAULT NULL COMMENT 'Код ИФНС ЮЛ',
  `terrifnsul` varchar(4) DEFAULT NULL COMMENT 'Код территориального участка ИФНС ЮЛ',
  `okato` varchar(11) DEFAULT NULL COMMENT 'OKATO',
  `oktmo` varchar(11) DEFAULT NULL COMMENT 'OKTMO',
  `updatedate` date NOT NULL COMMENT 'Дата  внесения записи',
  `intstart` int(10) NOT NULL COMMENT 'Значение начала интервала',
  `intend` int(10) NOT NULL COMMENT 'Значение окончания интервала',
  `houseintid` varchar(36) NOT NULL COMMENT 'Идентификатор записи интервала домов',
  `intguid` varchar(36) NOT NULL COMMENT 'Глобальный уникальный идентификатор интервала домов',
  `aoguid` varchar(36) NOT NULL COMMENT 'Идентификатор объекта родительского объекта (улицы, города, населенного пункта и т.п.)',
  `startdate` date NOT NULL COMMENT 'Начало действия записи',
  `enddate` date NOT NULL COMMENT 'Окончание действия записи',
  `intstatus` int(10) NOT NULL COMMENT 'Статус интервала (обычный, четный, нечетный)',
  `normdoc` varchar(36) DEFAULT NULL COMMENT 'Внешний ключ на нормативный документ',
  `counter` int(10) NOT NULL COMMENT 'Счетчик записей домов для КЛАДР 4'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Интервалы домов';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица fias.house_state_status
CREATE TABLE IF NOT EXISTS `house_state_status` (
  `housestid` int(10) NOT NULL COMMENT 'Идентификатор статуса',
  `name` varchar(60) NOT NULL COMMENT 'Наименование'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Статус состояния домов';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица fias.interval_status
CREATE TABLE IF NOT EXISTS `interval_status` (
  `intvstatid` int(10) NOT NULL COMMENT 'Идентификатор статуса (обычный, четный, нечетный)',
  `name` varchar(60) NOT NULL COMMENT 'Наименование'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Статус интервала домов';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица fias.landmark
CREATE TABLE IF NOT EXISTS `landmark` (
  `location` varchar(500) NOT NULL COMMENT 'Месторасположение ориентира',
  `postalcoe` varchar(6) DEFAULT NULL COMMENT 'Почтовый индекс',
  `ifnsfl` varchar(4) DEFAULT NULL COMMENT 'Код ИФНС ФЛ',
  `terrifnsfl` varchar(4) DEFAULT NULL COMMENT 'Код территориального участка ИФНС ФЛ',
  `ifnsul` varchar(4) DEFAULT NULL COMMENT 'Код ИФНС ЮЛ',
  `terrifnsul` varchar(4) DEFAULT NULL COMMENT 'Код территориального участка ИФНС ЮЛ',
  `okato` varchar(11) DEFAULT NULL COMMENT 'OKATO',
  `oktmo` varchar(11) DEFAULT NULL COMMENT 'OKTMO',
  `updatedate` date NOT NULL COMMENT 'Дата внесения записи',
  `landid` varchar(36) NOT NULL COMMENT 'Уникальный идентификатор записи ориентира',
  `landguid` varchar(36) NOT NULL COMMENT 'Глобальный уникальный идентификатор ориентира',
  `aoguid` varchar(36) NOT NULL COMMENT 'Уникальный идентификатор родительского объекта (улицы, города, населенного пункта и т.п.)',
  `startdate` date NOT NULL COMMENT 'Начало действия записи',
  `enddate` date NOT NULL COMMENT 'Окончание действия записи',
  `normdoc` varchar(36) DEFAULT NULL COMMENT 'Внешний ключ на нормативный документ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Описание мест расположения имущественных объектов';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица fias.normative_document
CREATE TABLE IF NOT EXISTS `normative_document` (
  `normdocid` varchar(36) NOT NULL COMMENT 'Идентификатор нормативного документа',
  `docname` text COMMENT 'Наименование документа',
  `docdate` date NOT NULL COMMENT 'Дата документа',
  `docnum` varchar(20) NOT NULL COMMENT 'Номер документа',
  `doctype` int(10) NOT NULL COMMENT 'Тип документа',
  `docimgid` int(10) NOT NULL COMMENT 'Идентификатор образа (внешний ключ)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Сведения по нормативному документу, являющемуся основанием присвоения адресному элементу наименования';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица fias.normative_document_type
CREATE TABLE IF NOT EXISTS `normative_document_type` (
  `ndtypeid` int(10) NOT NULL COMMENT 'Идентификатор записи (ключ)',
  `name` varchar(250) NOT NULL COMMENT 'Наименование типа нормативного документа'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Тип нормативного документа';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица fias.operation_status
CREATE TABLE IF NOT EXISTS `operation_status` (
  `operstatid` int(10) NOT NULL COMMENT 'Идентификатор статуса (ключ)',
  `name` varchar(100) NOT NULL COMMENT 'Наименование',
  PRIMARY KEY (`operstatid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Статус действия';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица fias.rooms
CREATE TABLE IF NOT EXISTS `rooms` (
  `roomguid` varchar(36) NOT NULL COMMENT 'Глобальный уникальный идентификатор адресного объекта (помещения)',
  `flatnumber` varchar(50) NOT NULL COMMENT 'Номер помещения или офиса',
  `flattype` int(11) NOT NULL COMMENT 'Тип помещения',
  `roomnumber` varchar(50) DEFAULT NULL COMMENT 'Номер комнаты',
  `roomtype` int(11) DEFAULT NULL COMMENT 'Тип комнаты',
  `regioncode` varchar(2) NOT NULL COMMENT 'Код региона',
  `postalcode` varchar(6) DEFAULT NULL COMMENT 'Почтовый индекс',
  `updatedate` date NOT NULL COMMENT 'Дата внесения записи',
  `houseguid` varchar(36) NOT NULL COMMENT 'Идентификатор родительского объекта (дома)',
  `roomid` varchar(36) NOT NULL COMMENT 'Уникальный идентификатор записи. Ключевое поле.',
  `previd` varchar(36) DEFAULT NULL COMMENT 'Идентификатор записи связывания с предыдушей исторической записью',
  `nextid` varchar(36) DEFAULT NULL COMMENT 'Идентификатор записи связывания с последующей исторической записью',
  `startdate` date NOT NULL COMMENT 'Начало действия записи',
  `enddate` date NOT NULL COMMENT 'Окончание действия записи',
  `livestatus` bit(1) NOT NULL COMMENT 'Признак действующего адресного объекта',
  `normdoc` varchar(36) DEFAULT NULL COMMENT 'Внешний ключ на нормативный документ',
  `operstatus` int(11) NOT NULL COMMENT 'Статус действия над записью – причина появления записи (см. описание таблицы OperationStatus)',
  `cadnum` varchar(100) DEFAULT NULL COMMENT 'Кадастровый номер помещения',
  `roomcadnum` varchar(100) DEFAULT NULL COMMENT 'Кадастровый номер комнаты в помещении',
  PRIMARY KEY (`roomid`),
  KEY `roomid` (`roomid`),
  KEY `houseguid` (`houseguid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Классификатор помещениях';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица fias.steads
CREATE TABLE IF NOT EXISTS `steads` (
  `steadguid` varchar(36) NOT NULL COMMENT 'Глобальный уникальный идентификатор адресного объекта (земельного участка)',
  `number` varchar(120) DEFAULT NULL COMMENT 'Номер земельного участка',
  `regioncode` varchar(2) NOT NULL COMMENT 'Код региона',
  `postalcode` varchar(6) DEFAULT NULL COMMENT 'Почтовый индекс',
  `ifnsfl` varchar(4) DEFAULT NULL COMMENT 'Код ИФНС ФЛ',
  `terrifnsfl` varchar(4) DEFAULT NULL COMMENT 'Код территориального участка ИФНС ФЛ',
  `ifnsul` varchar(4) DEFAULT NULL COMMENT 'Код ИФНС ЮЛ',
  `terrifnsul` varchar(4) DEFAULT NULL COMMENT 'Код территориального участка ИФНС ЮЛ',
  `okato` varchar(11) DEFAULT NULL COMMENT 'OKATO',
  `oktmo` varchar(11) DEFAULT NULL COMMENT 'OKTMO',
  `updatedate` date NOT NULL COMMENT 'Дата внесения записи',
  `parentguid` varchar(36) DEFAULT NULL COMMENT 'Идентификатор объекта родительского объекта',
  `steadid` varchar(36) NOT NULL COMMENT 'Уникальный идентификатор записи. Ключевое поле.',
  `previd` varchar(36) DEFAULT NULL COMMENT 'Идентификатор записи связывания с предыдушей исторической записью',
  `nextid` varchar(36) DEFAULT NULL COMMENT 'Идентификатор записи  связывания с последующей исторической записью',
  `operstatus` int(11) NOT NULL COMMENT 'Статус действия над записью – причина появления записи (см. описание таблицы OperationStatus)',
  `startdate` date NOT NULL COMMENT 'Начало действия записи',
  `enddate` date NOT NULL COMMENT 'Окончание действия записи',
  `normdoc` varchar(36) DEFAULT NULL COMMENT 'Внешний ключ на нормативный документ',
  `livestatus` bit(1) NOT NULL COMMENT 'Признак действующего адресного объекта',
  `cadnum` varchar(100) DEFAULT NULL COMMENT 'Кадастровый номер',
  `divtype` int(11) NOT NULL COMMENT 'Тип адресации',
  PRIMARY KEY (`steadid`),
  KEY `steadid` (`steadid`),
  KEY `parentguid` (`parentguid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Классификатор земельных участков';

-- Экспортируемые данные не выделены.
-- Дамп структуры для таблица fias.structure_status
CREATE TABLE IF NOT EXISTS `structure_status` (
  `strstatid` int(10) NOT NULL COMMENT 'Признак строения',
  `name` varchar(20) NOT NULL COMMENT 'Наименование',
  `shortname` varchar(20) DEFAULT NULL COMMENT 'Краткое наименование'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Признак строения';

-- Экспортируемые данные не выделены.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
