CREATE DATABASE  IF NOT EXISTS `fias` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `fias`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: fias
-- ------------------------------------------------------
-- Server version	5.7.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `del_house_interval`
--

DROP TABLE IF EXISTS `del_house_interval`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `del_house_interval` (
  `postalcode` varchar(6) DEFAULT NULL COMMENT 'Почтовый индекс',
  `ifnsfl` varchar(4) DEFAULT NULL COMMENT 'Код ИФНС ФЛ',
  `terrifnsfl` varchar(4) DEFAULT NULL COMMENT 'Код территориального участка ИФНС ФЛ',
  `ifnsul` varchar(4) DEFAULT NULL COMMENT 'Код ИФНС ЮЛ',
  `terrifnsul` varchar(4) DEFAULT NULL COMMENT 'Код территориального участка ИФНС ЮЛ',
  `okato` varchar(11) DEFAULT NULL COMMENT 'OKATO',
  `oktmo` varchar(11) DEFAULT NULL COMMENT 'OKTMO',
  `updatedate` date NOT NULL COMMENT 'Дата  внесения записи',
  `intstart` int(10) NOT NULL COMMENT 'Значение начала интервала',
  `intend` int(10) NOT NULL COMMENT 'Значение окончания интервала',
  `houseintid` varchar(36) NOT NULL COMMENT 'Идентификатор записи интервала домов',
  `intguid` varchar(36) NOT NULL COMMENT 'Глобальный уникальный идентификатор интервала домов',
  `aoguid` varchar(36) NOT NULL COMMENT 'Идентификатор объекта родительского объекта (улицы, города, населенного пункта и т.п.)',
  `startdate` date NOT NULL COMMENT 'Начало действия записи',
  `enddate` date NOT NULL COMMENT 'Окончание действия записи',
  `intstatus` int(10) NOT NULL COMMENT 'Статус интервала (обычный, четный, нечетный)',
  `normdoc` varchar(36) DEFAULT NULL COMMENT 'Внешний ключ на нормативный документ',
  `counter` int(10) DEFAULT NULL COMMENT 'Счетчик записей домов для КЛАДР 4'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Интервалы домов';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-04  7:55:42
