﻿using FIAS.Domain.Entity;
using FIAS.Domain.Repository;
using System;

namespace FIAS.Domain
{
    public class UnitOfWork : IDisposable
    {
        private fiasEntities db = new fiasEntities();
        private ActualStatusRepository actual_status_repository;
        private AddressObjectsRepository address_object_repository;
        private HouseRepository house_repository;

        public ActualStatusRepository ActualStatus
        {
            get
            {
                if (actual_status_repository == null)
                    actual_status_repository = new ActualStatusRepository(db);
                return actual_status_repository;
            }
        }

        public AddressObjectsRepository AddressObjects
        {
            get
            {
                if (address_object_repository == null)
                    address_object_repository = new AddressObjectsRepository(db);
                return address_object_repository;
            }
        }

        public HouseRepository House
        {
            get
            {
                if (house_repository == null)
                    house_repository = new HouseRepository(db);
                return house_repository;
            }
        }

        public void Save()
        {
            db.SaveChanges();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
