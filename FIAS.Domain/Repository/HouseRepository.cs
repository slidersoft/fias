﻿using FIAS.Domain.Entity;
using FIAS.Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIAS.Domain.Repository
{
    public class HouseRepository : IRepository<house>
    {
        private fiasEntities db;

        public HouseRepository(fiasEntities contex)
        {
            this.db = contex;
        }

        public void Create(house item)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public house Get(string id)
        {
            return db.house.Find(id);
        }

        public IEnumerable<house> GetHousesSteet(string street_giud)
        {
            return db.house.Where(h => h.aoguid == street_giud);
        }

        public IEnumerable<house> GetAll()
        {
            return db.house.Take(1000);
        }

        public void Update(house item)
        {
            throw new NotImplementedException();
        }
    }
}
