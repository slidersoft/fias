﻿using FIAS.Domain.Interface;
using FIAS.Domain.Entity;
using System;
using System.Linq;
using System.Collections.Generic;

namespace FIAS.Domain.Repository
{
    public class AddressObjectsRepository : IRepository<address_objects>
    {
        private fiasEntities db;

        public AddressObjectsRepository(fiasEntities contex)
        {
            this.db = contex;
        }

        public void Create(address_objects item)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public address_objects Get(string id)
        {
            return db.address_objects.Find(id);
        }

        /// <summary>
        /// Получить республику, край, область
        /// </summary>
        /// <param name = "name" ></ param >
        /// < returns ></ returns >
        public IEnumerable<address_objects> GetRepublic(string name)
        {
            var socrname = new string[] { "Автономная область", "Край", "Область" };
            var result = (from ao in db.address_objects
                          join aot in db.address_object_type on ao.shortname equals aot.scname
                          where
                          (
                            socrname.Contains(aot.socrname) &&
                            ao.offname.Contains(name)
                          )
                          select ao);
            return result;
        }

        /// <summary>
        /// Получить дочернии элементы адреса
        /// </summary>
        /// <param name="name">Имя дочернего элемента</param>
        /// <param name="parent_guid">ГУИД родителя</param>
        /// <returns></returns>
        public IEnumerable<address_objects> GetObject(string name, string parent_guid = "")
        {
            var result = db.address_objects
                .Where(
                ao => ao.livestatus == new byte[] { 1 } &&
                ao.parentguid == parent_guid && 
                ao.formalname.Contains(name))                
                .AsEnumerable();

            return result;
        }

        public IEnumerable<address_objects> GetAll()
        {
            int limit = 1000;
            var result = db.address_objects.Take(limit);
            return result;
        }

        public void Update(address_objects item)
        {
            throw new NotImplementedException();
        }
    }
}
