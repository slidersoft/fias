﻿using FIAS.Domain.Interface;
using FIAS.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIAS.Domain.Repository
{
    public class ActualStatusRepository : IRepository<actual_status>
    {
        private fiasEntities db;

        public ActualStatusRepository(fiasEntities contex)
        {
            this.db = contex;
        }

        public void Create(actual_status item)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public actual_status Get(string id)
        {
            return db.actual_status.Find(id);
        }

        public IEnumerable<actual_status> GetAll()
        {
            int limit = 1000;
            return db.actual_status.Take(limit);
        }

        public void Update(actual_status item)
        {
            throw new NotImplementedException();
        }
    }
}
