﻿using FIAS.Domain.Entity;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIAS.Domain
{
    public class DBService
    {
        private string _connection_string;

        public DBService ()
        {
            this._connection_string = ConfigurationManager.ConnectionStrings["MySQL"].ConnectionString;
        }
        
        public MySqlDataReader ExecuteQuery(string sql)
        {            
            using (MySqlConnection msc = new MySqlConnection(_connection_string))
            {
                using (MySqlCommand cmd = new MySqlCommand(sql, msc))
                {
                    msc.Open();
                    return cmd.ExecuteReader();                    
                }
            }            
        }

        public IEnumerable<address_objects> GetRepublic(string name)
        {
            List<address_objects> result = new List<address_objects>();

            string sql = "SELECT aoguid, formalname, regioncode, autocode, areacode, citycode, ctarcode, placecode, streetcode, extrcode, sextcode, offname, postalcode, ifnsfl, terrifnsfl, ifnsul, terrifnsul, okato, oktmo, updatedate, shortname, aolevel, parentguid, aoid, previd, nextid, code, plaincode, actstatus, centstatus, operstatus, currstatus, startdate, enddate, normdoc, livestatus "+
                        "FROM address_objects AS ao "+
                        "JOIN address_object_type AS aot ON ao.shortname = aot.scname "+
                        "WHERE aot.socrname in (\"Автономная область\", \"Край\", \"Область\",\"Республика\") "+
                        "AND ao.offname like '%"+ name +"%' "+
                        "AND ao.livestatus = 1 "+
                        "LIMIT 1000; ";

            using (MySqlConnection msc = new MySqlConnection(_connection_string))
            {
                using (MySqlCommand cmd = new MySqlCommand(sql, msc))
                {
                    msc.Open();
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result.Add(new address_objects
                                {
                                    aoguid = reader.GetString("aoguid"),
                                    formalname = reader.GetString("formalname"),
                                    actstatus = reader.GetUInt16("actstatus"),
                                    aoid = reader.GetString("aoid"),
                                    aolevel = reader.GetUInt16("aolevel"),
                                    areacode = reader.GetString("areacode"),
                                    autocode = reader.GetString("autocode"),
                                    centstatus = reader.GetUInt16("centstatus"),
                                    citycode = reader.GetString("citycode"),
                                    code = reader.GetString("code"),
                                    ctarcode = reader.GetString("ctarcode"),
                                    currstatus = reader.GetUInt16("currstatus"),                                    
                                    extrcode = reader.GetString("extrcode")
                                });
                            }
                            
                        }
                    }
                }
            }

            return result;
        }
    }
}
