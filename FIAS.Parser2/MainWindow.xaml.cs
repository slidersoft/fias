﻿using FIASParser;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;

namespace FIAS.Parser2
{
    public static class Extensions
    {
        public static string EmptyIfNull(this object value)
        {
            if (value == null)
                return "";
            return value.ToString();
        }
    }
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const string ACTSTAT_PATTERN = "^.+(AS_ACTSTAT_)[0-9]{8}_.+";
        const string ADDROBJ_PATTERN = "^.+(AS_ADDROBJ_)[0-9]{8}_.+";
        const string CENTERST_PATTERN = "^.+(AS_CENTERST_)[0-9]{8}_.+";
        const string CURENTST_PATTERN = "^.+(AS_CURENTST_)[0-9]{8}_.+";
        const string DEL_ADDROBJ_PATTERN = "^.+(AS_DEL_ADDROBJ_)[0-9]{8}_.+";
        const string DEL_HOUSE_PATTERN = "^.+(AS_DEL_HOUSE_)[0-9]{8}_.+";
        const string DEL_HOUSEINT_PATTERN = "^.+(AS_DEL_HOUSEINT_)[0-9]{8}_.+";
        const string DEL_NORMDOC_PATTERN = "^.+(AS_DEL_NORMDOC_)[0-9]{8}_.+";
        const string ESTSTAT_PATTERN = "^.+(AS_ESTSTAT_)[0-9]{8}_.+";
        const string HOUSE_PATTERN = "^.+(AS_HOUSE_)[0-9]{8}_.+";
        const string HOUSEINT_PATTERN = "^.+(AS_HOUSEINT_)[0-9]{8}_.+";
        const string HSTSTAT_PATTERN = "^.+(AS_HSTSTAT_)[0-9]{8}_.+";
        const string INTVSTAT_PATTERN = "^.+(AS_INTVSTAT_)[0-9]{8}_.+";
        const string LANDMARK_PATTERN = "^.+(AS_LANDMARK_)[0-9]{8}_.+";
        const string NDOCTYPE_PATTERN = "^.+(AS_NDOCTYPE_)[0-9]{8}_.+";
        const string NORMDOC_PATTERN = "^.+(AS_NORMDOC_)[0-9]{8}_.+";
        const string OPERSTAT_PATTERN = "^.+(AS_OPERSTAT_)[0-9]{8}_.+";
        const string SOCRBASE_PATTERN = "^.+(AS_SOCRBASE_)[0-9]{8}_.+";
        const string STRSTAT_PATTERN = "^.+(AS_STRSTAT_)[0-9]{8}_.+";
        const string STEAD_PATTERN = "^.+(AS_STEAD_)[0-9]{8}_.+";
        const string ROOM_PATTERN = "^.+(AS_ROOM_)[0-9]{8}_.+";        

        string connection_string;
        int max_count_insert;
        string path_file_version = @"file_version";
        Action<int> download_bar_action;

        public MainWindow()
        {
            InitializeComponent();
            max_count_insert = int.Parse(ConfigurationManager.AppSettings["max_count_insert"]);
            download_bar_action = (x) =>
            {
                Download_bar.Value = x;
            };
        }

        private int GetProcent(int all, int current)
        {
            return Convert.ToInt32(current * 100 / all);
        }

        private void checkBox_Parser_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                // устанавливаем метод обратного вызова
                TimerCallback tm = new TimerCallback(Parser);
                // создаем таймер                
                Timer timer = new Timer(tm, checkBox_Parser.IsChecked, 0, 604800000);
            }
            catch(Exception ex)
            {
                LogView.Items.Add("Ошибочка" + ex.Message);
            }
        }

        private async void Parser(object obj)
        {
            var Checked = (bool?)obj;
            if (Checked != null && (bool)Checked)
            {
                try
                {
                    await LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() => {
                        LogView.Items.Clear();
                        LogView.Items.Add("Проверяем обнавления БД");
                    }));
                    WebClient wc = new WebClient();
                    wc.DownloadProgressChanged += new DownloadProgressChangedEventHandler(DownloadProgressChanged);
                    FIASWebServices.DownloadServiceSoapClient client = new FIASWebServices.DownloadServiceSoapClient();
                    var file = client.GetLastDownloadFileInfo();
                    if (file.VersionId > GetFiasVersion())
                    {
                        string download_url = file.FiasCompleteXmlUrl;
                        var uri = new Uri(download_url);
                        await LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>{
                            LogView.Items.Add("Начало загрузки");
                        }));                        
                        var folder = ConfigurationManager.AppSettings["folder"];
                        var filename_extension = ConfigurationManager.AppSettings["filename_extension"];
                        var file_name = folder + file.TextVersion + filename_extension;
                        foreach (var f in Directory.GetFiles(folder))
                        {
                            File.Delete(f);
                        }
                        await wc.DownloadFileTaskAsync(uri, file_name);
                        
                        await LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() => {
                            LogView.Items.Add("Загрузка завершена");
                            LogView.Items.Add("Начало распоковки");
                        }));

                        var extract_result = 0;
                        extract_result = ExtractFromArchive(@"C:\Program Files\7-Zip\7z.exe", file_name, folder);

                        switch (extract_result)
                        {
                            case 0: // Без ошибок и предупреждений
                                await LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() => {
                                    LogView.Items.Add("Конец распоковки, Без ошибок и предупреждений");
                                }));                                
                                break;
                            case 1: // Есть некритичные предупреждения
                                await LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() => {
                                    LogView.Items.Add("Конец распоковки, Есть некритичные предупреждения");
                                }));                                
                                break;
                            case 2:
                                await LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() => {
                                    LogView.Items.Add("Конец распоковки, Фатальная ошибка");
                                }));                                
                                break;
                            case 7:
                                await LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() => {
                                    LogView.Items.Add("Конец распоковки, Ошибка в командной строке");
                                }));                                
                                break;
                            case 8:
                                await LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() => {
                                    LogView.Items.Add("Конец распоковки, Недостаточно памяти для выполнения операции");
                                }));                                
                                break;
                            case 225:
                                await LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() => {
                                    LogView.Items.Add("Конец распоковки, Пользователь отменил выполнение операции");
                                }));                                
                                break;
                            default:
                                await LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() => {
                                    LogView.Items.Add("Конец распоковки, Архиватор 7z вернул недокументированный код ошибки: " + extract_result.ToString());
                                }));
                                break;
                        }

                        if (extract_result == 0 || extract_result == 1)
                        {
                            connection_string = ConfigurationManager.ConnectionStrings["MySQL"].ToString();
                            var files = Directory.GetFiles(folder);
                            foreach (var f in files)
                            {
                                await Task.Factory.StartNew(() =>
                                {
                                    Parsing(f);
                                });
                            }
                            SetFiasVersion(file.VersionId);
                        }
                    }
                    else
                    {
                        await LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() => {
                            LogView.Items.Add("Нет новых версий базы");
                        }));
                    }
                }
                catch (Exception ex)
                {
                    await LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() => {
                        LogView.Items.Add("Непредвиденная ошибка " + ex.Message);
                    }));                    
                }
            }            
        }

        private void BeginParsing()
        {

        }

        private void DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            Download_bar.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() => {
                Download_bar.Value = e.ProgressPercentage;
            }));
                      
        }


        /// <summary>
        /// Распаковывает архив archiveName в каталог outputFolder
        /// </summary>
        /// <param name="archiver">файл архиватора вместе с полным путем</param>
        /// <param name="archiveName">Имя архива с полным путем</param>
        /// <param name="outputFolder">Каталог для распаковки</param>
        public int ExtractFromArchive(string archiver, string archiveName,
        string outputFolder)
        {
            try
            {
                // Предварительные проверки
                if (!File.Exists(archiver))
                    throw new Exception("Архиватор 7z по пути \"" + archiver +
                    "\" не найден");
                if (!File.Exists(archiveName))
                    throw new Exception("Файл архива \"" + archiveName +
                    "\" не найден");
                if (!Directory.Exists(outputFolder))
                    Directory.CreateDirectory(outputFolder);

                // Формируем параметры вызова 7z
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = archiver;
                // Распаковать (для полных путей - x)
                startInfo.Arguments = " e";
                // На все отвечать yes
                startInfo.Arguments += " -y";
                // Файл, который нужно распаковать
                startInfo.Arguments += " " + "\"" + archiveName + "\"";
                // Папка распаковки
                startInfo.Arguments += " -o" + "\"" + outputFolder + "\"";
                startInfo.WindowStyle = ProcessWindowStyle.Normal;
                int sevenZipExitCode = 0;

                using (Process sevenZip = Process.Start(startInfo))
                {   
                    sevenZip.WaitForExit();
                    sevenZipExitCode = sevenZip.ExitCode;
                }
                                
                return sevenZipExitCode;
            }
            catch (Exception e)
            {
                throw new Exception("SevenZip.ExtractFromArchive: " + e.Message);
            }
        }

        private void checkBox_Parser_Unchecked(object sender, RoutedEventArgs e)
        {
            var p = Process.GetProcesses();
            foreach(var i in p)
            {

            }
        }
        public void Parsing(string file_name)
        {
            if (Regex.IsMatch(file_name, ACTSTAT_PATTERN))
            {
                ReadACTSTAT(file_name);
            }
            else if (Regex.IsMatch(file_name, ADDROBJ_PATTERN))
            {
                ReadADDROBJ(file_name);
            }
            else if (Regex.IsMatch(file_name, CENTERST_PATTERN))
            {
                ReadCENTERST(file_name);
            }
            else if (Regex.IsMatch(file_name, CURENTST_PATTERN))
            {
                ReadCURENTST(file_name);
            }
            else if (Regex.IsMatch(file_name, DEL_ADDROBJ_PATTERN))
            {
                ReadADDROBJ(file_name);
            }
            else if (Regex.IsMatch(file_name, DEL_HOUSE_PATTERN))
            {
                ReadHOUSE(file_name);
            }
            else if (Regex.IsMatch(file_name, DEL_HOUSEINT_PATTERN))
            {
                ReadHOUSEINT(file_name);
            }
            else if (Regex.IsMatch(file_name, DEL_NORMDOC_PATTERN))
            {
                ReadNORMDOC(file_name);
            }
            else if (Regex.IsMatch(file_name, ESTSTAT_PATTERN))
            {
                ReadESTSTAT(file_name);
            }
            else if (Regex.IsMatch(file_name, HOUSE_PATTERN))
            {
                ReadHOUSE(file_name);
            }
            else if (Regex.IsMatch(file_name, HOUSEINT_PATTERN))
            {
                ReadHOUSEINT(file_name);
            }
            else if (Regex.IsMatch(file_name, HSTSTAT_PATTERN))
            {
                ReadHSTSTAT(file_name);
            }
            else if (Regex.IsMatch(file_name, INTVSTAT_PATTERN))
            {
                ReadINTVSTAT(file_name);
            }
            else if (Regex.IsMatch(file_name, LANDMARK_PATTERN))
            {
                ReadLANDMARK(file_name);
            }
            else if (Regex.IsMatch(file_name, NDOCTYPE_PATTERN))
            {
                ReadNDOCTYPE(file_name);
            }
            else if (Regex.IsMatch(file_name, NORMDOC_PATTERN))
            {
                ReadNORMDOC(file_name);
            }
            else if (Regex.IsMatch(file_name, OPERSTAT_PATTERN))
            {
                ReadOPERSTAT(file_name);
            }
            else if (Regex.IsMatch(file_name, SOCRBASE_PATTERN))
            {
                ReadSOCRBASE(file_name);
            }
            else if (Regex.IsMatch(file_name, STRSTAT_PATTERN))
            {
                ReadSTRSTAT(file_name);
            }
            else if (Regex.IsMatch(file_name, STEAD_PATTERN))
            {
                ReadSTEAD(file_name);
            }
            else if (Regex.IsMatch(file_name, ROOM_PATTERN))
            {
                ReadROOM(file_name);
            }
        }

        private void ReadROOM(string file_name)
        {
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Добавляем: " + file_name);
                })
                );
            int counter = 0;
            StringBuilder sCommand = new StringBuilder("TRUNCATE `fias`.`rooms`; INSERT INTO `fias`.`rooms` (`roomguid`, `flatnumber`, `flattype`, `roomnumber`, `roomtype`, `regioncode`,`postalcode`,`updatedate`,`houseguid`,`roomid`,`previd`,`nextid`,`startdate`,`enddate`,`livestatus`,`normdoc`,`operstatus`,`cadnum`,`roomcadnum`) VALUES");
            XmlTextReader reader = new XmlTextReader(file_name);
            List<string> Rows = new List<string>();
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element: // Узел является элементом.                        
                        if (reader.Name == "Room")
                        {
                            counter++;
                            RoomsRoom rr = new RoomsRoom();
                            while (reader.MoveToNextAttribute()) // Чтение атрибутов.
                            {
                                switch (reader.Name)
                                {
                                    case "ROOMGUID":
                                        rr.ROOMGUID = reader.Value;
                                        break;
                                    case "FLATNUMBER":
                                        rr.FLATNUMBER = reader.Value;
                                        break;
                                    case "FLATTYPE":
                                        rr.FLATTYPE = int.Parse(reader.Value);
                                        break;
                                    case "ROOMNUMBER":
                                        rr.ROOMNUMBER = reader.Value;
                                        break;
                                    case "ROOMTYPE":
                                        rr.ROOMTYPE = int.Parse(reader.Value);
                                        break;
                                    case "REGIONCODE":
                                        rr.REGIONCODE = reader.Value;
                                        break;
                                    case "POSTALCODE":
                                        rr.REGIONCODE = reader.Value;
                                        break;
                                    case "UPDATEDATE":
                                        rr.UPDATEDATE = DateTime.Parse(reader.Value);
                                        break;
                                    case "HOUSEGUID":
                                        rr.HOUSEGUID = reader.Value;
                                        break;
                                    case "ROOMID":
                                        rr.ROOMID = reader.Value;
                                        break;
                                    case "PREVID":
                                        rr.PREVID = reader.Value;
                                        break;
                                    case "NEXTID":
                                        rr.NEXTID = reader.Value;
                                        break;
                                    case "STARTDATE":
                                        rr.STARTDATE = DateTime.Parse(reader.Value);
                                        break;
                                    case "ENDDATE":
                                        rr.ENDDATE = DateTime.Parse(reader.Value);
                                        break;
                                    case "LIVESTATUS":
                                        rr.LIVESTATUS = sbyte.Parse(reader.Value);
                                        break;
                                    case "NORMDOC":
                                        rr.NORMDOC = reader.Value;
                                        break;
                                    case "OPERSTATUS":
                                        rr.OPERSTATUS = reader.Value;
                                        break;
                                    case "CADNUM":
                                        rr.CADNUM = reader.Value;
                                        break;
                                    case "ROOMCADNUM":
                                        rr.ROOMCADNUM = reader.Value;
                                        break;                                        
                                }
                            }
                            Rows.Add(string.Format("('{0}', '{1}', '{2}', '{3}', '{4}', '{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}')", 
                                MySqlHelper.EscapeString(rr.ROOMGUID), 
                                MySqlHelper.EscapeString(rr.FLATNUMBER),
                                rr.FLATTYPE,
                                rr.ROOMNUMBER,
                                rr.ROOMTYPE,
                                rr.REGIONCODE,
                                rr.POSTALCODE,
                                rr.UPDATEDATE.ToString("yyyyMMdd"),
                                rr.HOUSEGUID,
                                rr.ROOMID,
                                rr.PREVID,
                                rr.NEXTID,
                                rr.STARTDATE.ToString("yyyyMMdd"),
                                rr.ENDDATE.ToString("yyyyMMdd"),
                                rr.LIVESTATUS,
                                rr.NORMDOC,
                                rr.OPERSTATUS,
                                MySqlHelper.EscapeString(rr.CADNUM.EmptyIfNull()),
                                rr.ROOMCADNUM
                                ));

                            if (Rows.Count == max_count_insert)
                            {
                                sCommand.Append(string.Join(",", Rows));
                                sCommand.Append(";");
                                Rows.Clear();

                                MySQLEXEQUTE(sCommand);

                                sCommand.Clear();
                                sCommand = new StringBuilder("INSERT INTO `fias`.`rooms` (`roomguid`, `flatnumber`, `flattype`, `roomnumber`, `roomtype`, `regioncode`,`postalcode`,`updatedate`,`houseguid`,`roomid`,`previd`,`nextid`,`startdate`,`enddate`,`livestatus`,`normdoc`,`operstatus`,`cadnum`,`roomcadnum`) VALUES");
                            }
                        }
                        break;
                }
            }

            if (Rows.Count > 0)
            {
                sCommand.Append(string.Join(",", Rows));
                sCommand.Append(";");

                MySQLEXEQUTE(sCommand);
            }
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Готово: " + file_name);
                })
                );
            
        }

        private void ReadSTEAD(string file_name)
        {
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Добовляем: " + file_name);
                })
                );
            int counter = 0;
            StringBuilder sCommand = new StringBuilder("TRUNCATE `fias`.`steads`;INSERT INTO `fias`.`steads` (`steadguid`, `number`, `regioncode`, `postalcode`, `ifnsfl`, `terrifnsfl`,`ifnsul`,`terrifnsul`,`okato`,`oktmo`,`updatedate`,`parentguid`,`steadid`,`previd`,`nextid`,`operstatus`,`startdate`,`enddate`,`normdoc`,`livestatus`,`cadnum`,`divtype`) VALUES");
            XmlTextReader reader = new XmlTextReader(file_name);
            List<string> Rows = new List<string>();
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element: // Узел является элементом.                        
                        if (reader.Name == "Stead")
                        {
                            counter++;
                            SteadsStead ss = new SteadsStead();
                            while (reader.MoveToNextAttribute()) // Чтение атрибутов.
                            {
                                switch (reader.Name)
                                {
                                    case "STEADGUID":
                                        ss.STEADGUID = reader.Value;
                                        break;
                                    case "NUMBER":
                                        ss.NUMBER = reader.Value;
                                        break;
                                    case "REGIONCODE":
                                        ss.REGIONCODE = reader.Value;
                                        break;
                                    case "POSTALCODE":
                                        ss.POSTALCODE = reader.Value;
                                        break;
                                    case "IFNSFL":
                                        ss.IFNSFL = reader.Value;
                                        break;
                                    case "TERRIFNSFL":
                                        ss.TERRIFNSFL = reader.Value;
                                        break;
                                    case "IFNSUL":
                                        ss.IFNSUL = reader.Value;
                                        break;
                                    case "TERRIFNSUL":
                                        ss.TERRIFNSUL = reader.Value;
                                        break;
                                    case "OKATO":
                                        ss.OKATO = reader.Value;
                                        break;
                                    case "OKTMO":
                                        ss.OKTMO = reader.Value;
                                        break;
                                    case "UPDATEDATE":
                                        ss.UPDATEDATE = DateTime.Parse(reader.Value);
                                        break;
                                    case "PARENTGUID":
                                        ss.PARENTGUID = reader.Value;
                                        break;
                                    case "STEADID":
                                        ss.STEADID = reader.Value;
                                        break;
                                    case "PREVID":
                                        ss.PREVID = reader.Value;
                                        break;
                                    case "NEXTID":
                                        ss.NEXTID = reader.Value;
                                        break;
                                    case "OPERSTATUS":
                                        ss.OPERSTATUS = reader.Value;
                                        break;
                                    case "STARTDATE":
                                        ss.STARTDATE = DateTime.Parse(reader.Value);
                                        break;
                                    case "ENDDATE":
                                        ss.ENDDATE = DateTime.Parse(reader.Value);
                                        break;
                                    case "NORMDOC":
                                        ss.NORMDOC = reader.Value;
                                        break;
                                    case "LIVESTATUS":
                                        ss.LIVESTATUS = sbyte.Parse(reader.Value);
                                        break;
                                    case "CADNUM":
                                        ss.CADNUM = reader.Value;
                                        break;
                                    case "DIVTYPE":
                                        ss.DIVTYPE = int.Parse(reader.Value);
                                        break;
                                }
                            }
                            Rows.Add(string.Format("('{0}', '{1}', '{2}', '{3}', '{4}', '{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}','{19}','{20}','{21}')",
                                MySqlHelper.EscapeString(ss.STEADGUID),
                                MySqlHelper.EscapeString(ss.NUMBER.EmptyIfNull()),
                                ss.REGIONCODE,
                                ss.POSTALCODE,
                                ss.IFNSFL,
                                ss.TERRIFNSFL,
                                ss.IFNSUL,
                                ss.TERRIFNSUL,
                                ss.OKATO,
                                ss.OKTMO,
                                ss.UPDATEDATE.ToString("yyyyMMdd"),
                                ss.PARENTGUID,
                                ss.STEADID,
                                ss.PREVID,
                                ss.NEXTID,
                                ss.OPERSTATUS,
                                ss.STARTDATE.ToString("yyyyMMdd"),
                                ss.ENDDATE.ToString("yyyyMMdd"),
                                ss.NORMDOC,
                                ss.LIVESTATUS,
                                ss.CADNUM,
                                ss.DIVTYPE
                                ));

                            if (Rows.Count == max_count_insert)
                            {
                                sCommand.Append(string.Join(",", Rows));
                                sCommand.Append(";");
                                Rows.Clear();

                                MySQLEXEQUTE(sCommand);

                                sCommand.Clear();
                                sCommand = new StringBuilder("INSERT INTO `fias`.`steads` (`steadguid`, `number`, `regioncode`, `postalcode`, `ifnsfl`, `terrifnsfl`,`ifnsul`,`terrifnsul`,`okato`,`oktmo`,`updatedate`,`parentguid`,`steadid`,`previd`,`nextid`,`operstatus`,`startdate`,`enddate`,`normdoc`,`livestatus`,`cadnum`,`divtype`) VALUES");
                            }
                        }
                        break;
                }
            }

            if (Rows.Count > 0)
            {
                sCommand.Append(string.Join(",", Rows));
                sCommand.Append(";");

                MySQLEXEQUTE(sCommand);
            }
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Готово: " + file_name);
                })
                );
        }

        public void MySQLEXEQUTE(StringBuilder SQL)
        {            
            try
            {
                using (MySqlConnection mConnection = new MySqlConnection(connection_string))
                {
                    Stopwatch stopWatch = new Stopwatch();
                    mConnection.Open();

                    using (MySqlCommand myCmd = new MySqlCommand(SQL.ToString(), mConnection))
                    {
                        myCmd.CommandTimeout = 180000;
                        myCmd.CommandType = CommandType.Text;
                        stopWatch.Start();
                        int counter = myCmd.ExecuteNonQuery();
                        stopWatch.Stop();
                        TimeSpan ts = stopWatch.Elapsed;
                        string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                            ts.Hours, ts.Minutes, ts.Seconds,
                            ts.Milliseconds / 10);
                        if (ts.Seconds > 60)
                        {
                            System.Threading.Thread.Sleep(60000);
                        }
                    }
                }
            }
            catch(Exception e)
            {
                LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Ошибка: " + e.Message);
                })
                );               
            }
        }
        public void ReadACTSTAT(string file_name)
        {
            int items_count = 0;
            var items_insert = 0;
            List<string> Rows = new List<string>();

            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Добовляем: " + file_name);
                })
                );
            

            XmlTextReader reader = new XmlTextReader(file_name);
            StringBuilder sCommand = new StringBuilder("TRUNCATE `fias`.`actual_status`;INSERT INTO `fias`.`actual_status` (`actstatid`, `name`) VALUES");
            
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element: // Узел является элементом.                        
                        if (reader.Name == "ActualStatus")
                        {
                            items_count++;
                            items_insert++;
                            ActualStatus act = new ActualStatus();
                            while (reader.MoveToNextAttribute()) // Чтение атрибутов.
                            {
                                switch (reader.Name)
                                {
                                    case "ACTSTATID":
                                        act.actstatid = int.Parse(reader.Value);
                                        break;
                                    case "NAME":
                                        act.name = reader.Value.ToString();
                                        break;
                                }
                            }
                            Rows.Add(string.Format("('{0}','{1}')", MySqlHelper.EscapeString(act.actstatid.ToString()), MySqlHelper.EscapeString(act.name)));
                        }
                        break;
                }
            }

            sCommand.Append(string.Join(",", Rows));
            sCommand.Append(";");

            MySQLEXEQUTE(sCommand);

            Download_bar.Dispatcher.BeginInvoke(download_bar_action, GetProcent(items_count, items_insert));

            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Готово: " + file_name);
                })
                );
        }
        public void ReadADDROBJ(string file_name)
        {
            string sql = "";
            var items_count = 0;
            var items_insert = 0;
            List<string> Rows = new List<string>();
            

            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Добовляем: " + file_name);
                })
                );
            
            if (Regex.IsMatch(file_name, ADDROBJ_PATTERN))
            {
                sql = "TRUNCATE `fias`.`address_objects`;INSERT INTO `fias`.`address_objects` ( " +
                "`aoguid`, `formalname`, `regioncode`, `autocode`, `areacode`, `citycode`, `ctarcode`, `placecode`, `streetcode`, `extrcode`, `sextcode`, `offname`, `postalcode`, `ifnsfl`, `terrifnsfl`, `ifnsul`, `terrifnsul`, `okato`, `oktmo`, `updatedate`, `shortname`, `aolevel`, `parentguid`, `aoid`, `previd`, `nextid`, `code`, `plaincode`, `actstatus`, `centstatus`, `operstatus`, `currstatus`, `startdate`, `enddate`, `normdoc`, `livestatus`) VALUES";
            }
            else if (Regex.IsMatch(file_name, DEL_ADDROBJ_PATTERN))
            {
                sql = "TRUNCATE `fias`.`del_address_objects`;INSERT INTO `fias`.`del_address_objects` ( " +
                "`aoguid`, `formalname`, `regioncode`, `autocode`, `areacode`, `citycode`, `ctarcode`, `placecode`, `streetcode`, `extrcode`, `sextcode`, `offname`, `postalcode`, `ifnsfl`, `terrifnsfl`, `ifnsul`, `terrifnsul`, `okato`, `oktmo`, `updatedate`, `shortname`, `aolevel`, `parentguid`, `aoid`, `previd`, `nextid`, `code`, `plaincode`, `actstatus`, `centstatus`, `operstatus`, `currstatus`, `startdate`, `enddate`, `normdoc`, `livestatus`) VALUES";
            }

            using (XmlTextReader reader = new XmlTextReader(file_name))
            {
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element: // Узел является элементом.                                                
                            if (reader.Name == "Object")
                            {
                                items_count++;                                
                            }
                            break;
                    }
                }
            }
                        
            XmlTextReader reader1 = new XmlTextReader(file_name);
            StringBuilder sCommand = new StringBuilder(sql);
            
            while (reader1.Read())
            {
                switch (reader1.NodeType)
                {
                    case XmlNodeType.Element: // Узел является элементом.                                                
                        if (reader1.Name == "Object")
                        {                            
                            AddressObjects ao = new AddressObjects();
                            while (reader1.MoveToNextAttribute()) // Чтение атрибутов.
                            {
                                switch (reader1.Name)
                                {
                                    case "AOGUID":
                                        ao.AOGUID = reader1.Value;
                                        break;
                                    case "FORMALNAME":
                                        ao.FORMALNAME = reader1.Value.ToString();
                                        break;
                                    case "REGIONCODE":
                                        ao.REGIONCODE = reader1.Value.ToString();
                                        break;
                                    case "AUTOCODE":
                                        ao.AUTOCODE = reader1.Value.ToString();
                                        break;
                                    case "AREACODE":
                                        ao.AREACODE = reader1.Value.ToString();
                                        break;
                                    case "CITYCODE":
                                        ao.CITYCODE = reader1.Value.ToString();
                                        break;
                                    case "CTARCODE":
                                        ao.CTARCODE = reader1.Value.ToString();
                                        break;
                                    case "PLACECODE":
                                        ao.PLACECODE = reader1.Value.ToString();
                                        break;
                                    case "STREETCODE":
                                        ao.STREETCODE = reader1.Value.ToString();
                                        break;
                                    case "EXTRCODE":
                                        ao.EXTRCODE = reader1.Value.ToString();
                                        break;
                                    case "SEXTCODE":
                                        ao.SEXTCODE = reader1.Value.ToString();
                                        break;
                                    case "OFFNAME":
                                        ao.OFFNAME = reader1.Value.ToString();
                                        break;
                                    case "POSTALCODE":
                                        ao.POSTALCODE = reader1.Value.ToString();
                                        break;
                                    case "IFNSFL":
                                        ao.IFNSFL = reader1.Value.ToString();
                                        break;
                                    case "TERRIFNSFL":
                                        ao.TERRIFNSFL = reader1.Value.ToString();
                                        break;
                                    case "IFNSUL":
                                        ao.IFNSUL = reader1.Value.ToString();
                                        break;
                                    case "TERRIFNSUL":
                                        ao.TERRIFNSUL = reader1.Value.ToString();
                                        break;
                                    case "OKATO":
                                        ao.OKATO = reader1.Value.ToString();
                                        break;
                                    case "OKTMO":
                                        ao.OKTMO = reader1.Value.ToString();
                                        break;
                                    case "UPDATEDATE":
                                        ao.UPDATEDATE = Convert.ToDateTime(reader1.Value.ToString()).Date;
                                        break;
                                    case "SHORTNAME":
                                        ao.SHORTNAME = reader1.Value.ToString();
                                        break;
                                    case "AOLEVEL":
                                        ao.AOLEVEL = Convert.ToInt16(reader1.Value.ToString());
                                        break;
                                    case "PARENTGUID":
                                        ao.PARENTGUID = reader1.Value.ToString();
                                        break;
                                    case "AOID":
                                        ao.AOID = reader1.Value.ToString();
                                        break;
                                    case "PREVID":
                                        ao.PREVID = reader1.Value.ToString();
                                        break;
                                    case "NEXTID":
                                        ao.NEXTID = reader1.Value.ToString();
                                        break;
                                    case "CODE":
                                        ao.CODE = reader1.Value.ToString();
                                        break;
                                    case "PLAINCODE":
                                        ao.PLAINCODE = reader1.Value.ToString();
                                        break;
                                    case "ACTSTATUS":
                                        ao.ACTSTATUS = Convert.ToInt16(reader1.Value.ToString());
                                        break;
                                    case "CENTSTATUS":
                                        ao.CENTSTATUS = Convert.ToInt16(reader1.Value.ToString());
                                        break;
                                    case "OPERSTATUS":
                                        ao.OPERSTATUS = Convert.ToInt16(reader1.Value.ToString());
                                        break;
                                    case "CURRSTATUS":
                                        ao.CURRSTATUS = Convert.ToInt16(reader1.Value.ToString());
                                        break;
                                    case "STARTDATE":
                                        ao.STARTDATE = Convert.ToDateTime(reader1.Value.ToString()).Date;
                                        break;
                                    case "ENDDATE":
                                        ao.ENDDATE = Convert.ToDateTime(reader1.Value.ToString()).Date;
                                        break;
                                    case "NORMDOC":
                                        ao.NORMDOC = reader1.Value.ToString();
                                        break;
                                    case "LIVESTATUS":
                                        ao.LIVESTATUS = Convert.ToByte(reader1.Value.ToString());
                                        break;
                                }
                            }
                            Rows.Add(String.Format(
                                "('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}', '{19}', '{20}', '{21}', '{22}', '{23}', '{24}', '{25}', '{26}', '{27}', '{28}', '{29}', '{30}', '{31}', '{32}', '{33}', '{34}', '{35}')",
                                ao.AOGUID, //1 
                                ao.FORMALNAME,//2 
                                ao.REGIONCODE, //3
                                ao.AUTOCODE, //4
                                ao.AREACODE, //5
                                ao.CITYCODE, //6
                                ao.CTARCODE, //7
                                ao.PLACECODE, //8
                                ao.STREETCODE.ToString(), //9
                                ao.EXTRCODE, //10
                                ao.SEXTCODE, //11
                                MySqlHelper.EscapeString(ao.OFFNAME.EmptyIfNull()), //12
                                ao.POSTALCODE,
                                ao.IFNSFL,
                                ao.TERRIFNSFL,
                                ao.IFNSUL,
                                ao.TERRIFNSUL,
                                ao.OKATO,
                                ao.OKTMO,
                                ao.UPDATEDATE.ToString("yyyymmdd"),
                                ao.SHORTNAME,
                                ao.AOLEVEL,
                                ao.PARENTGUID,
                                ao.AOID,
                                ao.PREVID,
                                ao.NEXTID,
                                ao.CODE,
                                ao.PLAINCODE,
                                ao.ACTSTATUS,
                                ao.CENTSTATUS,
                                ao.OPERSTATUS,
                                ao.CURRSTATUS,
                                ao.STARTDATE.ToString("yyyymmdd"),
                                ao.ENDDATE.ToString("yyyymmdd"),
                                ao.NORMDOC,
                                ao.LIVESTATUS.ToString()));

                            if (Rows.Count == max_count_insert)
                            {
                                items_insert += Rows.Count;                                
                                sCommand.Append(string.Join(",", Rows));
                                sCommand.Append(";");
                                Rows.Clear();

                                MySQLEXEQUTE(sCommand);

                                Download_bar.Dispatcher.BeginInvoke(download_bar_action, GetProcent(items_count,items_insert));
                                                                                                
                                sCommand.Clear();
                                sCommand = new StringBuilder("INSERT INTO `fias`.`address_objects` ( " +
                                    "`aoguid`, `formalname`, `regioncode`, `autocode`, `areacode`, `citycode`, `ctarcode`, `placecode`, `streetcode`, `extrcode`, `sextcode`, `offname`, `postalcode`, `ifnsfl`, `terrifnsfl`, `ifnsul`, `terrifnsul`, `okato`, `oktmo`, `updatedate`, `shortname`, `aolevel`, `parentguid`, `aoid`, `previd`, `nextid`, `code`, `plaincode`, `actstatus`, `centstatus`, `operstatus`, `currstatus`, `startdate`, `enddate`, `normdoc`, `livestatus`) VALUES");
                            }
                        }
                        break;
                }
            }
            if (Rows.Count > 0)
            {
                sCommand.Append(string.Join(",", Rows));
                sCommand.Append(";");
                MySQLEXEQUTE(sCommand);
                Download_bar.Dispatcher.BeginInvoke(download_bar_action, GetProcent(items_count, items_insert));
            }
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Готово: " + file_name);
                })
                );
        }
        public void ReadCENTERST(string file_name)
        {
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Добовляем: " + file_name);
                })
                );
            using (XmlTextReader local_reader = new XmlTextReader(file_name))
            {
                Stopwatch stopWatch = new Stopwatch();
                StringBuilder st = new StringBuilder("TRUNCATE `fias`.`center_status`;INSERT INTO `fias`.`center_status` (`centerstid`, `name`) VALUES");
                List<string> r = new List<string>();

                while (local_reader.Read())
                {
                    if (local_reader.NodeType == XmlNodeType.Element)
                    {
                        if (local_reader.Name == "CenterStatus")
                        {
                            CenterStatus ct = new CenterStatus();
                            while (local_reader.MoveToNextAttribute())
                            {
                                switch (local_reader.Name)
                                {
                                    case "CENTERSTID":
                                        ct.CENTERSTID = Convert.ToInt16(local_reader.Value);
                                        break;
                                    case "NAME":
                                        ct.NAME = local_reader.Value;
                                        break;
                                }
                            }
                            r.Add(String.Format("({0},'{1}')",
                                ct.CENTERSTID,
                                ct.NAME.EmptyIfNull()));
                        }
                    }
                }
                st.Append(string.Join(",", r));
                st.Append(";");

                MySQLEXEQUTE(st);
            }
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Готово: " + file_name);
                })
                );
        }
        public void ReadCURENTST(string file_name)
        {
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Добовляем: " + file_name);
                })
                );
            string sql = "TRUNCATE `fias`.`current_status`;INSERT INTO `fias`.`current_status` (`curentstid`, `name`) VALUES";

            using (XmlTextReader local_reader = new XmlTextReader(file_name))
            {
                StringBuilder st = new StringBuilder(sql);
                List<string> r = new List<string>();

                while (local_reader.Read())
                {
                    if (local_reader.NodeType == XmlNodeType.Element)
                    {
                        if (local_reader.Name == "CurrentStatus")
                        {
                            CurrentStatus cs = new CurrentStatus();
                            while (local_reader.MoveToNextAttribute())
                            {
                                switch (local_reader.Name)
                                {
                                    case "CURENTSTID":
                                        cs.CURENTSTID = Convert.ToInt16(local_reader.Value);
                                        break;
                                    case "NAME":
                                        cs.NAME = local_reader.Value;
                                        break;
                                }
                            }
                            r.Add(String.Format("({0},'{1}')",
                                cs.CURENTSTID,
                                cs.NAME));

                            if (r.Count == max_count_insert)
                            {
                                st.Append(string.Join(",", r));
                                st.Append(";");
                                r.Clear();

                                MySQLEXEQUTE(st);

                                st.Clear();
                                st.Append(sql);
                            }
                        }
                    }
                }
                if (r.Count > 0)
                {
                    st.Append(string.Join(",", r));
                    st.Append(";");

                    MySQLEXEQUTE(st);
                }
            }
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Готово: " + file_name);
                })
                );
        }
        private void ReadHOUSE(string file_name)
        {
            string sql = "";
            int items_count = 0;
            var items_insert = 0;

            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Добовляем: " + file_name);
                })
                );
            
            if (Regex.IsMatch(file_name, HOUSE_PATTERN))
            {
                StringBuilder trunc = new StringBuilder().Append("TRUNCATE `fias`.`house`;");
                MySQLEXEQUTE(trunc);
                sql = "INSERT INTO `fias`.`house` " +
                "(postalcode, ifnsfl, terrifnsfl, ifnsul, terrifnsul, okato, oktmo, updatedate, housenum, eststatus, buildnum, strucnum, strstatus, houseid, houseguid, aoguid, startdate, enddate, statstatus, normdoc, counter) VALUES ";
            }
            else if (Regex.IsMatch(file_name, DEL_HOUSE_PATTERN))
            {
                StringBuilder trunc = new StringBuilder().Append("TRUNCATE `fias`.`del_house`;");
                MySQLEXEQUTE(trunc);
                sql = "INSERT INTO `fias`.`del_house` " +
                "(postalcode, ifnsfl, terrifnsfl, ifnsul, terrifnsul, okato, oktmo, updatedate, housenum, eststatus, buildnum, strucnum, strstatus, houseid, houseguid, aoguid, startdate, enddate, statstatus, normdoc, counter) VALUES ";
                //('', '', '', '', '', '', '', NOW(), '', 0, '', '', '', '', '', '', NOW(), NOW(), 0, '', 0)";
            }

            using (XmlTextReader local_reader = new XmlTextReader(file_name))
            {
                while (local_reader.Read())
                {
                    if (local_reader.NodeType == XmlNodeType.Element)
                    {
                        if (local_reader.Name == "House") {
                            items_count++;
                        }
                    }
                }
            }

            using (XmlTextReader local_reader = new XmlTextReader(file_name))
            {
                StringBuilder st = new StringBuilder(sql);
                List<string> r = new List<string>();

                while (local_reader.Read())
                {
                    if (local_reader.NodeType == XmlNodeType.Element)
                    {
                        if (local_reader.Name == "House")
                        {
                            House cs = new House();
                            while (local_reader.MoveToNextAttribute())
                            {
                                switch (local_reader.Name)
                                {
                                    case "POSTALCODE":
                                        cs.POSTALCODE = local_reader.Value;
                                        break;
                                    case "IFNSFL":
                                        cs.IFNSFL = local_reader.Value;
                                        break;
                                    case "TERRIFNSFL":
                                        cs.TERRIFNSFL = local_reader.Value;
                                        break;
                                    case "IFNSUL":
                                        cs.IFNSUL = local_reader.Value;
                                        break;
                                    case "TERRIFNSUL":
                                        cs.TERRIFNSUL = local_reader.Value;
                                        break;
                                    case "OKATO":
                                        cs.OKATO = local_reader.Value;
                                        break;
                                    case "OKTMO":
                                        cs.OKTMO = local_reader.Value;
                                        break;
                                    case "UPDATEDATE":
                                        cs.UPDATEDATE = Convert.ToDateTime(local_reader.Value.ToString()).Date;
                                        break;
                                    case "HOUSENUM":
                                        cs.HOUSENUM = local_reader.Value;
                                        break;
                                    case "ESTSTATUS":
                                        cs.ESTSTATUS = int.Parse(local_reader.Value);
                                        break;
                                    case "BUILDNUM":
                                        cs.BUILDNUM = local_reader.Value;
                                        break;
                                    case "STRUCNUM":
                                        cs.STRUCNUM = local_reader.Value;
                                        break;
                                    case "STRSTATUS":
                                        cs.STRUCNUM = local_reader.Value;
                                        break;
                                    case "HOUSEID":
                                        cs.HOUSEID = local_reader.Value;
                                        break;
                                    case "HOUSEGUID":
                                        cs.HOUSEGUID = local_reader.Value;
                                        break;
                                    case "AOGUID":
                                        cs.AOGUID = local_reader.Value;
                                        break;
                                    case "STARTDATE":
                                        cs.STARTDATE = Convert.ToDateTime(local_reader.Value.ToString()).Date;
                                        break;
                                    case "ENDDATE":
                                        cs.ENDDATE = Convert.ToDateTime(local_reader.Value.ToString()).Date;
                                        break;
                                    case "STATSTATUS":
                                        cs.STATSTATUS = int.Parse(local_reader.Value);
                                        break;
                                    case "NORMDOC":
                                        cs.NORMDOC = local_reader.Value;
                                        break;
                                    case "COUNTER":
                                        cs.COUNTER = int.Parse(local_reader.Value);
                                        break;
                                }
                            }
                            r.Add(String.Format("('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', {9}, '{10}', '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', {18}, '{19}', {20})",
                                cs.POSTALCODE.EmptyIfNull(),
                                cs.IFNSFL.EmptyIfNull(),
                                cs.TERRIFNSFL.EmptyIfNull(),
                                cs.IFNSUL.EmptyIfNull(),
                                cs.TERRIFNSUL.EmptyIfNull(),
                                cs.OKATO.EmptyIfNull(),
                                cs.OKTMO.EmptyIfNull(),
                                cs.UPDATEDATE.ToString("yyyyMMdd"),
                                cs.HOUSENUM.EmptyIfNull(),
                                cs.ESTSTATUS,
                                cs.BUILDNUM.EmptyIfNull(),
                                cs.STRUCNUM.EmptyIfNull(),
                                cs.STRSTATUS.EmptyIfNull(),
                                cs.HOUSEID,
                                cs.HOUSEGUID,
                                cs.AOGUID,
                                cs.STARTDATE.ToString("yyyyMMdd"),
                                cs.ENDDATE.ToString("yyyyMMdd"),
                                cs.STATSTATUS,
                                cs.NORMDOC.EmptyIfNull(),
                                cs.COUNTER));

                            if (r.Count == max_count_insert)
                            {
                                items_insert += r.Count;
                                st.Append(string.Join(",", r));
                                st.Append(";");
                                r.Clear();

                                MySQLEXEQUTE(st);

                                Download_bar.Dispatcher.BeginInvoke(download_bar_action, GetProcent(items_count, items_insert));

                                st.Clear();
                                st.Append(sql);
                            }
                        }
                    }
                }
                if (r.Count > 0)
                {
                    items_insert += r.Count;
                    st.Append(string.Join(",", r));
                    st.Append(";");

                    MySQLEXEQUTE(st);

                    Download_bar.Dispatcher.BeginInvoke(download_bar_action, GetProcent(items_count, items_insert));
                }
            }
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Готово: " + file_name);
                })
                );
        }
        private void ReadHOUSEINT(string file_name)
        {
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Добовляем: " + file_name);
                })
                );
            string sql = "";
            if (Regex.IsMatch(file_name, HOUSEINT_PATTERN))
            {
                sql = "TRUNCATE `fias`.`house_interval`;INSERT INTO `fias`.`house_interval` " +
                "(postalcode, ifnsfl, terrifnsfl, ifnsul, terrifnsul, okato, oktmo, updatedate, intstart, intend, houseintid, intguid, aoguid, startdate, enddate, intstatus, normdoc, counter) VALUES ";
                //('', '', '', '', '', '', '', NOW(), 0, 0, '', '', '', NOW(), NOW(), 0, '', 0)
            }
            else if (Regex.IsMatch(file_name, DEL_HOUSEINT_PATTERN))
            {
                sql = "TRUNCATE `fias`.`del_house_interval`;INSERT INTO `fias`.`del_house_interval` " +
                "(postalcode, ifnsfl, terrifnsfl, ifnsul, terrifnsul, okato, oktmo, updatedate, intstart, intend, houseintid, intguid, aoguid, startdate, enddate, intstatus, normdoc, counter) VALUES ";
            }
            using (XmlTextReader local_reader = new XmlTextReader(file_name))
            {
                StringBuilder st = new StringBuilder(sql);
                List<string> r = new List<string>();

                while (local_reader.Read())
                {
                    if (local_reader.NodeType == XmlNodeType.Element)
                    {
                        if (local_reader.Name == "HouseInterval")
                        {
                            HouseInterval cs = new HouseInterval();
                            while (local_reader.MoveToNextAttribute())
                            {
                                switch (local_reader.Name)
                                {
                                    case "POSTALCODE":
                                        cs.POSTALCODE = local_reader.Value;
                                        break;
                                    case "IFNSFL":
                                        cs.IFNSFL = local_reader.Value;
                                        break;
                                    case "TERRIFNSFL":
                                        cs.TERRIFNSFL = local_reader.Value;
                                        break;
                                    case "IFNSUL":
                                        cs.IFNSUL = local_reader.Value;
                                        break;
                                    case "TERRIFNSUL":
                                        cs.TERRIFNSUL = local_reader.Value;
                                        break;
                                    case "OKATO":
                                        cs.OKATO = local_reader.Value;
                                        break;
                                    case "OKTMO":
                                        cs.OKTMO = local_reader.Value;
                                        break;
                                    case "UPDATEDATE":
                                        cs.UPDATEDATE = DateTime.Parse(local_reader.Value).Date;
                                        break;
                                    case "INTSTART":
                                        cs.INTSTART = int.Parse(local_reader.Value);
                                        break;
                                    case "INTEND":
                                        cs.INTEND = int.Parse(local_reader.Value);
                                        break;
                                    case "HOUSEINTID":
                                        cs.HOUSEINTID = local_reader.Value;
                                        break;
                                    case "INTGUID":
                                        cs.INTGUID = local_reader.Value;
                                        break;
                                    case "AOGUID":
                                        cs.AOGUID = local_reader.Value;
                                        break;
                                    case "STARTDATE":
                                        cs.STARTDATE = DateTime.Parse(local_reader.Value).Date;
                                        break;
                                    case "ENDDATE":
                                        cs.ENDDATE = DateTime.Parse(local_reader.Value).Date;
                                        break;
                                    case "INTSTATUS":
                                        cs.INTSTATUS = int.Parse(local_reader.Value);
                                        break;
                                    case "NORMDOC":
                                        cs.NORMDOC = local_reader.Value;
                                        break;
                                    case "COUNTER":
                                        cs.COUNTER = int.Parse(local_reader.Value);
                                        break;
                                }
                            }
                            r.Add(String.Format("('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', {8}, {9}, '{10}', '{11}', '{12}', '{13}', '{14}', {15}, '{16}', {17})",
                                cs.POSTALCODE.EmptyIfNull(),
                                cs.IFNSFL.EmptyIfNull(),
                                cs.TERRIFNSFL.EmptyIfNull(),
                                cs.IFNSUL.EmptyIfNull(),
                                cs.TERRIFNSUL.EmptyIfNull(),
                                cs.OKATO.EmptyIfNull(),
                                cs.OKTMO.EmptyIfNull(),
                                cs.UPDATEDATE.ToString("yyyyMMdd"),
                                cs.INTSTART,
                                cs.INTEND,
                                cs.HOUSEINTID,
                                cs.INTGUID,
                                cs.AOGUID,
                                cs.STARTDATE.ToString("yyyyMMdd"),
                                cs.ENDDATE.ToString("yyyyMMdd"),
                                cs.INTSTATUS,
                                cs.NORMDOC.EmptyIfNull(),
                                cs.COUNTER));

                            if (r.Count == max_count_insert)
                            {
                                st.Append(string.Join(",", r));
                                st.Append(";");
                                r.Clear();

                                MySQLEXEQUTE(st);

                                st.Clear();
                                st.Append(sql);
                            }
                        }
                    }
                }
                if (r.Count > 0)
                {
                    st.Append(string.Join(",", r));
                    st.Append(";");

                    MySQLEXEQUTE(st);
                }
            }
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Готово: " + file_name);
                })
                );
        }
        private void ReadNORMDOC(string file_name)
        {
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Добовляем: " + file_name);
                })
                );
            string sql = "";
            if (Regex.IsMatch(file_name, NORMDOC_PATTERN))
            {
                sql = "TRUNCATE `fias`.`normative_document`;INSERT INTO `fias`.`normative_document` " +
                "	(normdocid, docname, docdate, docnum, doctype, docimgid) VALUES ";
                //('', '', NOW(), '', 0, 0)
            }
            else if (Regex.IsMatch(file_name, DEL_NORMDOC_PATTERN))
            {
                sql = "TRUNCATE `fias`.`del_normative_document`;INSERT INTO `fias`.`del_normative_document` " +
                "	(normdocid, docname, docdate, docnum, doctype, docimgid) VALUES ";
            }
            using (XmlTextReader local_reader = new XmlTextReader(file_name))
            {
                StringBuilder st = new StringBuilder(sql);
                List<string> r = new List<string>();

                while (local_reader.Read())
                {
                    if (local_reader.NodeType == XmlNodeType.Element)
                    {
                        if (local_reader.Name == "NormativeDocument")
                        {
                            NormativeDocument cs = new NormativeDocument();
                            while (local_reader.MoveToNextAttribute())
                            {
                                switch (local_reader.Name)
                                {
                                    case "NORMDOCID":
                                        cs.NORMDOCID = local_reader.Value;
                                        break;
                                    case "DOCNAME":
                                        cs.DOCNAME = local_reader.Value;
                                        break;
                                    case "DOCDATE":
                                        cs.DOCDATE = DateTime.Parse(local_reader.Value).Date;
                                        break;
                                    case "DOCNUM":
                                        cs.DOCNUM = local_reader.Value;
                                        break;
                                    case "DOCTYPE":
                                        cs.DOCTYPE = int.Parse(local_reader.Value);
                                        break;
                                    case "DOCIMGID":
                                        cs.DOCIMGID = int.Parse(local_reader.Value);
                                        break;
                                }
                            }
                            r.Add(string.Format("('{0}', '{1}', '{2}', '{3}', {4}, {5})",
                                cs.NORMDOCID,
                                MySqlHelper.EscapeString(cs.DOCNAME.EmptyIfNull()),
                                cs.DOCDATE.ToString("yyyyMMdd"),
                                MySqlHelper.EscapeString(cs.DOCNUM.EmptyIfNull()),
                                cs.DOCTYPE,
                                cs.DOCIMGID));

                            if (r.Count == max_count_insert)
                            {
                                st.Append(string.Join(",", r));
                                st.Append(";");
                                r.Clear();

                                MySQLEXEQUTE(st);

                                st.Clear();
                                st.Append(sql);
                            }
                        }
                    }
                }
                if (r.Count > 0)
                {
                    st.Append(string.Join(",", r));
                    st.Append(";");

                    MySQLEXEQUTE(st);
                }
            }
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Готово: " + file_name);
                })
                );
        }
        private void ReadESTSTAT(string file_name)
        {
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Добовляем: " + file_name);
                })
                );
            string sql = "TRUNCATE `fias`.`estate_status`;INSERT INTO `fias`.`estate_status` " +
                 "(eststatid, name, shortname) VALUES ";

            using (XmlTextReader local_reader = new XmlTextReader(file_name))
            {
                StringBuilder st = new StringBuilder(sql);
                List<string> r = new List<string>();

                while (local_reader.Read())
                {
                    if (local_reader.NodeType == XmlNodeType.Element)
                    {
                        if (local_reader.Name == "EstateStatus")
                        {
                            EstateStatus cs = new EstateStatus();
                            while (local_reader.MoveToNextAttribute())
                            {
                                switch (local_reader.Name)
                                {
                                    case "ESTSTATID":
                                        cs.ESTSTATID = int.Parse(local_reader.Value);
                                        break;
                                    case "NAME":
                                        cs.NAME = local_reader.Value;
                                        break;
                                    case "SHORTNAME":
                                        cs.SHORTNAME = local_reader.Value;
                                        break;
                                }
                            }
                            r.Add(String.Format("({0}, '{1}', '{2}')",
                                cs.ESTSTATID,
                                cs.NAME,
                                cs.SHORTNAME.EmptyIfNull()));

                            if (r.Count == max_count_insert)
                            {
                                st.Append(string.Join(",", r));
                                st.Append(";");
                                r.Clear();

                                MySQLEXEQUTE(st);

                                st.Clear();
                                st.Append(sql);
                            }
                        }
                    }
                }
                if (r.Count > 0)
                {
                    st.Append(string.Join(",", r));
                    st.Append(";");

                    MySQLEXEQUTE(st);
                }
            }
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Готово: " + file_name);
                })
                );
        }
        private void ReadHSTSTAT(string file_name)
        {
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Добовляем: " + file_name);
                })
                );
            string sql = "TRUNCATE `fias`.`house_state_status`;INSERT INTO `fias`.`house_state_status` " +
                "(housestid, name) VALUES ";

            using (XmlTextReader local_reader = new XmlTextReader(file_name))
            {
                StringBuilder st = new StringBuilder(sql);
                List<string> r = new List<string>();

                while (local_reader.Read())
                {
                    if (local_reader.NodeType == XmlNodeType.Element)
                    {
                        if (local_reader.Name == "HouseStateStatus")
                        {
                            HouseStateStatus cs = new HouseStateStatus();
                            while (local_reader.MoveToNextAttribute())
                            {
                                switch (local_reader.Name)
                                {
                                    case "HOUSESTID":
                                        cs.HOUSESTID = int.Parse(local_reader.Value);
                                        break;
                                    case "NAME":
                                        cs.NAME = local_reader.Value;
                                        break;
                                }
                            }
                            r.Add(String.Format("({0}, '{1}')",
                                cs.HOUSESTID,
                                cs.NAME));

                            if (r.Count == max_count_insert)
                            {
                                st.Append(string.Join(",", r));
                                st.Append(";");
                                r.Clear();

                                MySQLEXEQUTE(st);

                                st.Clear();
                                st.Append(sql);
                            }
                        }
                    }
                }
                if (r.Count > 0)
                {
                    st.Append(string.Join(",", r));
                    st.Append(";");

                    MySQLEXEQUTE(st);
                }
            }
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Готово: " + file_name);
                })
                );
        }
        private void ReadINTVSTAT(string file_name)
        {
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Добовляем: " + file_name);
                })
                );
            string sql = "TRUNCATE `fias`.`interval_status`;INSERT INTO `fias`.`interval_status` " +
                "(intvstatid, name) VALUES ";

            using (XmlTextReader local_reader = new XmlTextReader(file_name))
            {
                StringBuilder st = new StringBuilder(sql);
                List<string> r = new List<string>();

                while (local_reader.Read())
                {
                    if (local_reader.NodeType == XmlNodeType.Element)
                    {
                        if (local_reader.Name == "IntervalStatus")
                        {
                            IntervalStatus cs = new IntervalStatus();
                            while (local_reader.MoveToNextAttribute())
                            {
                                switch (local_reader.Name)
                                {
                                    case "INTVSTATID":
                                        cs.INTVSTATID = int.Parse(local_reader.Value);
                                        break;
                                    case "NAME":
                                        cs.NAME = local_reader.Value;
                                        break;
                                }
                            }
                            r.Add(String.Format("({0}, '{1}')",
                                cs.INTVSTATID,
                                cs.NAME));

                            if (r.Count == max_count_insert)
                            {
                                st.Append(string.Join(",", r));
                                st.Append(";");
                                r.Clear();

                                MySQLEXEQUTE(st);

                                st.Clear();
                                st.Append(sql);
                            }
                        }
                    }
                }
                if (r.Count > 0)
                {
                    st.Append(string.Join(",", r));
                    st.Append(";");

                    MySQLEXEQUTE(st);
                }
            }
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Готово: " + file_name);
                })
                );
        }
        private void ReadLANDMARK(string file_name)
        {
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Добовляем: " + file_name);
                })
                );
            string sql = "TRUNCATE `fias`.`landmark`;INSERT INTO `fias`.`landmark` " +
                "(location, postalcoe, ifnsfl, terrifnsfl, ifnsul, terrifnsul, okato, oktmo, updatedate, landid, landguid, aoguid, startdate, enddate, normdoc) VALUES ";

            using (XmlTextReader local_reader = new XmlTextReader(file_name))
            {
                StringBuilder st = new StringBuilder(sql);
                List<string> r = new List<string>();

                while (local_reader.Read())
                {
                    if (local_reader.NodeType == XmlNodeType.Element)
                    {
                        if (local_reader.Name == "Landmark")
                        {
                            Landmark cs = new Landmark();
                            while (local_reader.MoveToNextAttribute())
                            {
                                switch (local_reader.Name)
                                {
                                    case "LOCATION":
                                        cs.LOCATION = local_reader.Value;
                                        break;
                                    case "POSTALCODE":
                                        cs.POSTALCODE = local_reader.Value;
                                        break;
                                    case "IFNSFL":
                                        cs.IFNSFL = local_reader.Value;
                                        break;
                                    case "TERRIFNSFL":
                                        cs.TERRIFNSFL = local_reader.Value;
                                        break;
                                    case "IFNSUL":
                                        cs.IFNSUL = local_reader.Value;
                                        break;
                                    case "TERRIFNSUL":
                                        cs.TERRIFNSUL = local_reader.Value;
                                        break;
                                    case "OKATO":
                                        cs.OKATO = local_reader.Value;
                                        break;
                                    case "OKTMO":
                                        cs.OKTMO = local_reader.Value;
                                        break;
                                    case "UPDATEDATE":
                                        cs.UPDATEDATE = DateTime.Parse(local_reader.Value).Date;
                                        break;
                                    case "LANDID":
                                        cs.LANDID = local_reader.Value;
                                        break;
                                    case "LANDGUID":
                                        cs.LANDGUID = local_reader.Value;
                                        break;
                                    case "AOGUID":
                                        cs.AOGUID = local_reader.Value;
                                        break;
                                    case "STARTDATE":
                                        cs.STARTDATE = DateTime.Parse(local_reader.Value).Date;
                                        break;
                                    case "ENDDATE":
                                        cs.ENDDATE = DateTime.Parse(local_reader.Value).Date;
                                        break;
                                    case "NORMDOC":
                                        cs.NORMDOC = local_reader.Value;
                                        break;
                                }
                            }
                            r.Add(String.Format("('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}')",
                                cs.LOCATION,
                                cs.POSTALCODE.EmptyIfNull(),
                                cs.IFNSFL.EmptyIfNull(),
                                cs.TERRIFNSFL.EmptyIfNull(),
                                cs.IFNSUL.EmptyIfNull(),
                                cs.TERRIFNSUL.EmptyIfNull(),
                                cs.OKATO.EmptyIfNull(),
                                cs.OKTMO.EmptyIfNull(),
                                cs.UPDATEDATE.ToString("yyyyMMdd"),
                                cs.LANDID.EmptyIfNull(),
                                cs.LANDGUID.EmptyIfNull(),
                                cs.AOGUID.EmptyIfNull(),
                                cs.STARTDATE.ToString("yyyyMMdd"),
                                cs.ENDDATE.ToString("yyyyMMdd"),
                                cs.NORMDOC.EmptyIfNull()));

                            if (r.Count == max_count_insert)
                            {
                                st.Append(string.Join(",", r));
                                st.Append(";");
                                r.Clear();

                                MySQLEXEQUTE(st);

                                st.Clear();
                                st.Append(sql);
                            }
                        }
                    }
                }
                if (r.Count > 0)
                {
                    st.Append(string.Join(",", r));
                    st.Append(";");

                    MySQLEXEQUTE(st);
                }
            }
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Готово: " + file_name);
                })
                );
        }
        private void ReadNDOCTYPE(string file_name)
        {
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Добовляем: " + file_name);
                })
                );
            string sql = "TRUNCATE `fias`.`normative_document_type`;INSERT INTO `fias`.`normative_document_type` " +
                "(ndtypeid, name) VALUES ";

            using (XmlTextReader local_reader = new XmlTextReader(file_name))
            {
                StringBuilder st = new StringBuilder(sql);
                List<string> r = new List<string>();

                while (local_reader.Read())
                {
                    if (local_reader.NodeType == XmlNodeType.Element)
                    {
                        if (local_reader.Name == "NormativeDocumentType")
                        {
                            NormativeDocumentType cs = new NormativeDocumentType();
                            while (local_reader.MoveToNextAttribute())
                            {
                                switch (local_reader.Name)
                                {
                                    case "NDTYPEID":
                                        cs.NDTYPEID = int.Parse(local_reader.Value);
                                        break;
                                    case "NAME":
                                        cs.NAME = local_reader.Value;
                                        break;
                                }
                            }
                            r.Add(String.Format("({0}, '{1}')",
                                cs.NDTYPEID,
                                cs.NAME));

                            if (r.Count == max_count_insert)
                            {
                                st.Append(string.Join(",", r));
                                st.Append(";");
                                r.Clear();

                                MySQLEXEQUTE(st);

                                st.Clear();
                                st.Append(sql);
                            }
                        }
                    }
                }
                if (r.Count > 0)
                {
                    st.Append(string.Join(",", r));
                    st.Append(";");

                    MySQLEXEQUTE(st);
                }
            }
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Готово: " + file_name);
                })
                );
        }
        private void ReadOPERSTAT(string file_name)
        {
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Добовляем: " + file_name);
                })
                );
            string sql = "TRUNCATE `fias`.`operation_status`;INSERT INTO `fias`.`operation_status` " +
                "(operstatid, name) VALUES ";

            using (XmlTextReader local_reader = new XmlTextReader(file_name))
            {
                StringBuilder st = new StringBuilder(sql);
                List<string> r = new List<string>();

                while (local_reader.Read())
                {
                    if (local_reader.NodeType == XmlNodeType.Element)
                    {
                        if (local_reader.Name == "OperationStatus")
                        {
                            OperationStatus cs = new OperationStatus();
                            while (local_reader.MoveToNextAttribute())
                            {
                                switch (local_reader.Name)
                                {
                                    case "OPERSTATID":
                                        cs.OPERSTATID = int.Parse(local_reader.Value);
                                        break;
                                    case "NAME":
                                        cs.NAME = local_reader.Value;
                                        break;
                                }
                            }
                            r.Add(String.Format("({0}, '{1}')",
                                cs.OPERSTATID,
                                cs.NAME));

                            if (r.Count == max_count_insert)
                            {
                                st.Append(string.Join(",", r));
                                st.Append(";");
                                r.Clear();

                                MySQLEXEQUTE(st);

                                st.Clear();
                                st.Append(sql);
                            }
                        }
                    }
                }
                if (r.Count > 0)
                {
                    st.Append(string.Join(",", r));
                    st.Append(";");

                    MySQLEXEQUTE(st);
                }
            }
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Готово: " + file_name);
                })
                );
        }
        private void ReadSOCRBASE(string file_name)
        {
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Добовляем: " + file_name);
                })
                );
            string sql = "TRUNCATE `fias`.`address_object_type`;INSERT INTO `fias`.`address_object_type` " +
                "(`level`, scname, socrname, kod_t_st) VALUES ";

            using (XmlTextReader local_reader = new XmlTextReader(file_name))
            {
                StringBuilder st = new StringBuilder(sql);
                List<string> r = new List<string>();

                while (local_reader.Read())
                {
                    if (local_reader.NodeType == XmlNodeType.Element)
                    {
                        if (local_reader.Name == "AddressObjectType")
                        {
                            AddressObjectType cs = new AddressObjectType();
                            while (local_reader.MoveToNextAttribute())
                            {
                                switch (local_reader.Name)
                                {
                                    case "LEVEL":
                                        cs.LEVEL = int.Parse(local_reader.Value);
                                        break;
                                    case "SCNAME":
                                        cs.SCNAME = local_reader.Value;
                                        break;
                                    case "SOCRNAME":
                                        cs.SOCRNAME = local_reader.Value;
                                        break;
                                    case "KOD_T_ST":
                                        cs.KOD_T_ST = local_reader.Value;
                                        break;
                                }
                            }
                            r.Add(String.Format("({0}, '{1}', '{2}', '{3}')",
                                cs.LEVEL,
                                cs.SCNAME.EmptyIfNull(),
                                cs.SOCRNAME,
                                cs.KOD_T_ST));

                            if (r.Count == max_count_insert)
                            {
                                st.Append(string.Join(",", r));
                                st.Append(";");
                                r.Clear();

                                MySQLEXEQUTE(st);

                                st.Clear();
                                st.Append(sql);
                            }
                        }
                    }
                }
                if (r.Count > 0)
                {
                    st.Append(string.Join(",", r));
                    st.Append(";");

                    MySQLEXEQUTE(st);
                }
            }
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Готово: " + file_name);
                })
                );
        }
        private void ReadSTRSTAT(string file_name)
        {
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Добовляем: " + file_name);
                })
                );
            string sql = "TRUNCATE `fias`.`structure_status`;INSERT INTO `fias`.`structure_status` " +
                "(strstatid, name, shortname) VALUES ";

            using (XmlTextReader local_reader = new XmlTextReader(file_name))
            {
                StringBuilder st = new StringBuilder(sql);
                List<string> r = new List<string>();

                while (local_reader.Read())
                {
                    if (local_reader.NodeType == XmlNodeType.Element)
                    {
                        if (local_reader.Name == "StructureStatus")
                        {
                            StructureStatus cs = new StructureStatus();
                            while (local_reader.MoveToNextAttribute())
                            {
                                switch (local_reader.Name)
                                {
                                    case "STRSTATID":
                                        cs.STRSTATID = int.Parse(local_reader.Value);
                                        break;
                                    case "NAME":
                                        cs.NAME = local_reader.Value;
                                        break;
                                    case "SHORTNAME":
                                        cs.SHORTNAME = local_reader.Value;
                                        break;
                                }
                            }
                            r.Add(String.Format("({0}, '{1}', '{2}')",
                                cs.STRSTATID,
                                cs.NAME,
                                cs.SHORTNAME.EmptyIfNull()));

                            if (r.Count == max_count_insert)
                            {
                                st.Append(string.Join(",", r));
                                st.Append(";");
                                r.Clear();

                                MySQLEXEQUTE(st);

                                st.Clear();
                                st.Append(sql);
                            }
                        }
                    }
                }
                if (r.Count > 0)
                {
                    st.Append(string.Join(",", r));
                    st.Append(";");

                    MySQLEXEQUTE(st);
                }
            }
            LogView.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                new Action(() =>
                {
                    LogView.Items.Add("Готово: " + file_name);
                })
                );
        }
        private int GetFiasVersion()
        {
            int result = 0;

            try
            {
                using (StreamReader sr = new StreamReader(path_file_version, System.Text.Encoding.Default))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        result = int.Parse(line);
                    }
                }
            }
            catch
            {

            }

            return result;
        }

        private void SetFiasVersion(int version)
        {
            using (StreamWriter sw = new StreamWriter(path_file_version, false, System.Text.Encoding.Default))
            {
                sw.WriteLine(version);
            }
        }
    }
}
