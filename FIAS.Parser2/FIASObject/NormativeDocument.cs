﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIASParser
{
    /// <summary>
    /// Сведения по нормативному документу, являющемуся основанием присвоения адресному элементу наименования
    /// </summary>
    public class NormativeDocument
    {
        /// <summary>
        /// Идентификатор нормативного документа
        /// </summary>
        public string NORMDOCID { get; set; }
        /// <summary>
        /// Наименование документа
        /// </summary>
        public string DOCNAME { get; set; }
        /// <summary>
        /// Дата документа
        /// </summary>
        public DateTime DOCDATE { get; set; }
        /// <summary>
        /// Номер документа
        /// </summary>
        public string DOCNUM { get; set; }
        /// <summary>
        /// Тип документа
        /// </summary>
        public int DOCTYPE { get; set; }
        /// <summary>
        /// Идентификатор образа (внешний ключ)
        /// </summary>
        public int DOCIMGID { get; set; }
    }
}
