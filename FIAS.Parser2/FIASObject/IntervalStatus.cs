﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIASParser
{
    /// <summary>
    /// Статус интервала домов
    /// </summary>
    class IntervalStatus
    {
        /// <summary>
        /// Идентификатор статуса (обычный, четный, нечетный)
        /// </summary>
        public int INTVSTATID { get; set; }
        /// <summary>
        /// Наименование
        /// </summary>
        public string NAME { get; set; }
    }
}
