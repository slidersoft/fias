﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIASParser
{
    /// <summary>
    /// Описание мест расположения  имущественных объектов
    /// </summary>
    public class Landmark
    {
        /// <summary>
        /// Месторасположение ориентира
        /// </summary>
        public string LOCATION { get; set; }
        /// <summary>
        /// Почтовый индекс
        /// </summary>
        public string POSTALCODE { get; set;}
        /// <summary>
        /// Код ИФНС ФЛ
        /// </summary>
        public string IFNSFL { get; set; }
        /// <summary>
        /// Код территориального участка ИФНС ФЛ
        /// </summary>
        public string TERRIFNSFL { get; set; }
        /// <summary>
        /// Код ИФНС ЮЛ
        /// </summary>
        public string IFNSUL { get; set; }
        /// <summary>
        /// Код территориального участка ИФНС ЮЛ
        /// </summary>
        public string TERRIFNSUL { get; set; }
        /// <summary>
        /// OKATO
        /// </summary>
        public string OKATO { get; set; }
        /// <summary>
        /// OKTMO
        /// </summary>
        public string OKTMO { get; set; }
        /// <summary>
        /// Дата внесения записи
        /// </summary>
        public DateTime UPDATEDATE { get; set; }
        /// <summary>
        /// Уникальный идентификатор записи ориентира
        /// </summary>
        public string LANDID { get; set; }
        /// <summary>
        /// Глобальный уникальный идентификатор ориентира
        /// </summary>
        public string LANDGUID { get; set; }
        /// <summary>
        /// Уникальный идентификатор родительского объекта (улицы, города, населенного пункта и т.п.)
        /// </summary>
        public string AOGUID { get; set; }
        /// <summary>
        /// Начало действия записи
        /// </summary>
        public DateTime STARTDATE { get; set; }
        /// <summary>
        /// Окончание действия записи
        /// </summary>
        public DateTime ENDDATE { get; set; }
        /// <summary>
        /// Внешний ключ на нормативный документ
        /// </summary>
        public string NORMDOC { get; set; }
    }
}
