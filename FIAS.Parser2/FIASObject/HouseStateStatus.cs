﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIASParser
{
    /// <summary>
    /// Статус состояния домов
    /// </summary>
    public class HouseStateStatus
    {
        /// <summary>
        /// Идентификатор статуса
        /// </summary>
        public int HOUSESTID { get; set; }
        /// <summary>
        /// Наименование
        /// </summary>
        public string NAME { get; set; }
    }
}
