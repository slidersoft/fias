﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIASParser
{
    /// <summary>
    /// Признак строения
    /// </summary>
    public class StructureStatus
    {
        /// <summary>
        /// Признак строения
        /// </summary>
        public int STRSTATID { get; set; }
        /// <summary>
        /// Наименование
        /// </summary>
        public string NAME { get; set;}
        /// <summary>
        /// Краткое наименование
        /// </summary>
        public string SHORTNAME { get; set; }
    }
}
