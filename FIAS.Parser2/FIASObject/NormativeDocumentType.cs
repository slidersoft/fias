﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIASParser
{
    /// <summary>
    /// Тип нормативного документа
    /// </summary>
    public class NormativeDocumentType
    {
        /// <summary>
        /// Идентификатор записи (ключ)
        /// </summary>
        public int NDTYPEID { get; set; }
        /// <summary>
        /// Наименование типа нормативного документа
        /// </summary>
        public string NAME { get; set; }
    }
}
