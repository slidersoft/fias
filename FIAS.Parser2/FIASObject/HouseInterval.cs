﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIASParser
{
    /// <summary>
    /// Интервалы домов
    /// </summary>
    public class HouseInterval
    {
        /// <summary>
        /// Почтовый индекс
        /// </summary>
        public string POSTALCODE { get; set; }
        /// <summary>
        /// Код ИФНС ФЛ
        /// </summary>
        public string IFNSFL { get; set; }
        /// <summary>
        /// Код территориального участка ИФНС ФЛ
        /// </summary>
        public string TERRIFNSFL { get; set; }
        /// <summary>
        /// Код ИФНС ЮЛ
        /// </summary>
        public string IFNSUL { get; set; }
        /// <summary>
        /// Код территориального участка ИФНС ЮЛ
        /// </summary>
        public string TERRIFNSUL { get; set; }
        /// <summary>
        /// OKATO
        /// </summary>
        public string OKATO { get; set; }
        /// <summary>
        /// OKTMO
        /// </summary>
        public string OKTMO { get; set; }
        /// <summary>
        /// Дата внесения записи
        /// </summary>
        public DateTime UPDATEDATE { get; set; }
        /// <summary>
        /// Значение начала интервала
        /// </summary>
        public int INTSTART { get; set; }
        /// <summary>
        /// Значение окончания интервала
        /// </summary>
        public int INTEND { get; set; }
        /// <summary>
        /// Идентификатор записи интервала домов
        /// </summary>
        public string HOUSEINTID { get; set; }
        /// <summary>
        /// Глобальный уникальный идентификатор интервала домов
        /// </summary>
        public string INTGUID { get; set; }
        /// <summary>
        /// Идентификатор объекта родительского объекта (улицы, города, населенного пункта и т.п.)
        /// </summary>
        public string AOGUID { get; set; }
        /// <summary>
        /// Начало действия записи
        /// </summary>
        public DateTime STARTDATE { get; set; }
        /// <summary>
        /// Окончание действия записи
        /// </summary>
        public DateTime ENDDATE { get; set; }
        /// <summary>
        /// Статус интервала (обычный, четный, нечетный)
        /// </summary>
        public int INTSTATUS { get; set; }
        /// <summary>
        /// Внешний ключ на нормативный документ
        /// </summary>
        public string NORMDOC { get; set; }
        /// <summary>
        /// Счетчик записей домов для КЛАДР 4
        /// </summary>
        public int COUNTER { get; set; }
    }
}
