﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIASParser
{
    /// <summary>
    /// Признак владения
    /// </summary>
    public class EstateStatus
    {
        /// <summary>
        /// Признак владения
        /// </summary>
        public int ESTSTATID { get; set; }
        /// <summary>
        /// Наименование
        /// </summary>
        public string NAME { get; set; }
        /// <summary>
        /// Краткое наименование
        /// </summary>
        public string SHORTNAME { get; set; }
    }
}
